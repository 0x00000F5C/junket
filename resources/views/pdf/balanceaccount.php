<html>
<head>
<title>End Day Report</title>
</head>
<style>
	body{
		font-size: 11px;
	}
	table{
		border-collapse: collapse;
	}
	
	table tr td{
		padding: none;
	}
	
	td.padding5{
		padding: 5px;
	}
	
	table.padding5 tr td{
		padding: 5px;
	}
	
	table.padding5 tr th{
		padding: 5px;
	}
	
	table.center tr td{
		text-align: center
	}
	
	table tr.bottom-line td{
		border-bottom: 1px solid black;
	}
	table tr td.bottom-line{
		border-bottom: 1px solid black;
	}
	.red{
		color: red;
	}
</style>
<body>
<table>
<tr>
	<td>
	<table class="padding5" width="50%" border="0" style="background-color:yellow">
		<tr>
			<td><b>END DAY REPORT</b></td>
		</tr>
		<?php $bal=0; foreach($data as $row): ?>
			<tr>
				<td><?php echo $row->name2; ?></td>
				<td <?php if($row->balance<0){echo 'class="color:red"';} ?>><?php echo $row->balance; $bal+=$row->balance; ?></td>
				<td><?php echo date('d-M',strtotime($row->date)); ?></td>
			</tr>
		<?php endforeach; ?>
		<tr>
			<td>Total</td>
			<td><?php echo $bal; ?></td>
		</tr>
	</table>
	</td>
	<td>&nbsp;</td>
	<td>
	<table class="padding5" width="50%" border="0">
		<?php $balance=0;$id=''; foreach($journal as $key=>$row): ?>
			<?php if($id!=$row->id){$balance=0;$id=$row->id;} ?>
			<tr>
				<td><?php echo $row->name2; ?></td>
				<td <?php if($row->balance<0){echo 'class="color:red"';} ?>><?php echo $row->balance; $balance+=$row->balance; ?></td>
				<td><?php echo date('d-M',strtotime($row->date)); ?></td>
			</tr>
			<?php if($journal[$key+1]->id!=$row->id): ?>
			<tr>
				<td>Total</td>
				<td><?php echo $balance; ?></td>
			</tr>
			<tr>
				<td></td>
			</tr>
			<?php endif; ?>
		<?php endforeach; ?>
	</table>
	</td>
</tr>
</table>
</body>
</html>