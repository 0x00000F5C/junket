<html>
<head>
<title>Rolling Report</title>
</head>
<style>
	table{
		border-collapse: collapse;
	}
	
	table tr td{
		padding: none;
	}
	
	td.padding5{
		padding: 5px;
	}
	
	table.padding5 tr td{
		padding: 5px;
	}
	
	table.center tr td{
		text-align: center;
	}
</style>
<body>
	<table width="100%" class="padding5">
		<tr>
			<td width="20%">Date</td>
			<td width="20%">: <?php echo date('d-m-Y',strtotime(@$master->date)); ?></td>
			<td width="10%"></td>
			<td width="30%">Individual or Group / Agent</td>
			<td width="20%">: <?php echo @$master->customer->group_flag; ?></td>
		</tr>
		<tr>
			<td>Person In Charge</td>
			<td>: <?php echo @$master->user->name; ?></td>
			<td></td>
			<td>Agent Name</td>
			<td>: <?php echo @$master->customer->agent->name; ?></td>
		</tr>
		<tr>
			<td>Customer Name</td>
			<td>: <?php echo @$master->customer->first_name; ?></td>
			<td></td>
			<td>Scenario</td>
			<td>: <?php echo @$master->scenario; ?></td>
		</tr>
	</table>
	<br/>
	<br/>
	<?php $showagent = "style='display:none'"; 
	if($master->customer->aget){
		$showagent = "";
	} ?>
	<table width="100%" border="1" class="padding5">
		<tr>
			<th>No</th>
			<th>Bet</th>
			<th>Odds</th>
			<th>Amount</th>
			<th>Room</th>
			<th>GL</th>
			<th <?= $showagent; ?>>Agent</th>
			<th>PP</th>
			<th>Other</th>
		</tr>
		<?php $total_insurance=0; $total_gl=0; $total_as=0; $total_pp=0; 
		foreach($details as $key=>$data): ?>
		<tr>
			<td align="center"><?php echo $key+1; ?></td>
			<td align="right"><?php echo number_format($data->rolling,2); ?></td>
			<td align="right"><?php echo number_format($data->odds,2); ?></td>
			<td align="right"><?php echo number_format($data->insurance,2); $total_insurance+=$data->insurance; ?></td>
			<td><?php echo $data->room; ?></td>
			<?php if($data->scenario == 0){
				$status = "Lose";
				$kali = 1;
			}else{
				$status = "Win";
				$kali = 1;
			}
			$valueag = 0;
			$valuegl = 0;
			$valuepp = 0;
			if(!$master->customer->agent){
				if($data->room == "Room 38"){
					$valuegl = $data->insurance;
				} else if($data->room == "Maxims"){
					$valuegl = $data->insurance * 0.7;
					$valuepp = $data->insurance * 0.3;
				} else if($data->room == "Other Room"){
					$valuegl = $data->insurance * 0.8;
					$valuepp = $data->insurance * 0.2;
				}
			}else if($master->customer->agent->name != ""){
				if($data->status == "Win or Lose"){
					$shownego = true;
					if($data->room == "Room 38"){
						$persent = 100;
					} else if($data->room == "Maxims"){
						$persent = 70;
						$valuepp = $data->insurance * 0.3;
					} else if($data->room == "Other Room"){
						$persent = 80;
						$valuepp = $data->insurance * 0.2;
					}
					
					$hasil = $data->insurance * ($persent / 100);
					$persengl = 100 - $data->agent_shared;
					$valuegl = $hasil * ($persengl / 100);
					$valueag = $hasil * ($data->agent_shared / 100);
				} else if($data->status == "Win Only"){
					$shownego = false;
					if($data->room == "Room 38"){
						$valuegl = $data->insurance * (90 / 100);
						$valueag = $data->insurance * (10 / 100);
					} else if($data->room == "Maxims"){
						$showpp = true;
						$hasil = $data->insurance * 0.7;
						$valuegl = $hasil * 0.9;
						$valueag = $hasil * 0.1;
						$valuepp = $data->insurance * 0.3;
					} else if($data->room == "Other Room"){
						$hasil = $data->insurance * 0.8;
						$valuegl = $hasil * 0.9;
						$valueag = $hasil * 0.1;
						$valuepp = $data->insurance * 0.2;
					}
				}
			}
			
			$gl=$valuegl * $kali;
			$as=$valueag * $kali;
			$pp=($data->room=='Other Room'?0:($valuepp * $kali));
			$other=($data->room=='Other Room'?($valuepp * $kali):0);
			?>

			<td align="right"><?php echo number_format($gl,2); $total_gl+=$gl;  ?></td>
			<td align="right" <?php echo $showagent; ?>><?php echo number_format($as,2); $total_as+=$as; ?></td>
			<td align="right"><?php echo number_format($pp,2); $total_pp+=$pp; ?></td>
			<td align="right"><?php echo number_format($other,2); $total_other+=$other; ?></td>
		</tr>
		<?php endforeach; ?>
		<tr>
			<td colspan="2">Total</td>
			<td></td>
			<td align="right"><?php echo number_format($total_insurance,2); ?></td>
			<td></td>
			<td align="right"><?php echo number_format($total_gl,2); ?></td>
			<td align="right"><?php echo number_format($total_as,2); ?></td>
			<td align="right"><?php echo number_format($total_pp,2); ?></td>
			<td align="right"><?php echo number_format($total_other,2); ?></td>
		</tr>
	</table>
</body>
</html>