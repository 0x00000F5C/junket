<html>
<head>
<title>Manager Report</title>
</head>
<style>
	table{
		border-collapse: collapse;
	}
	
	table tr td{
		padding: none;
	}
	
	td.padding4{
		padding: 4px;
	}
	
	table.padding4 tr td{
		padding: 4px;
	}
	
	table.center tr td{
		text-align: center
	}
	
	.half{
		width: 48%;
		float: left;
		margin-right: 10px;
		margin-left: 10px;
	}
</style>
<body>
	Date: <?php echo $date; ?>
	<?php foreach($data as $valuesData): ?>
		<?php for($inde=0;$inde<$valuesData->ceil;$inde++): 
			$limitation=19*($inde+1)-19; 
			$i=19*($inde+1)-19-1; ?>
		<div class="half">
			<table width="100%" border="1" class="padding4">
				<tr>
					<td align="left" colspan="5">
						<?php echo $valuesData->name; ?>
					</td>
				</tr>
				<tr>
					<td align="center" width="20%">
						No
					</td>
					<td align="center" width="20%">
						Name
					</td>
					<td align="center" width="20%">
						In
					</td>
					<td align="center" width="20%">
						Out
					</td>
					<td align="center" width="20%">
						Remark
					</td>
				</tr>
				<?php $debit=0; $credit=0; for($inde2=0;$inde2<19;$inde2++): ?>
				<tr>
					<td align="center"><?php echo $inde; ?></td>
					<td><?php echo $valuesData->details[$inde2]->name; ?></td>
					<td align="right">
						<?php echo isset($valuesData->details[$inde2]->balancein)?number_format($valuesData->details[$inde2]->balancein,2):''; 
						$debit+=($inde2==0?0:$valuesData->details[$inde2]->balancein); ?>
					</td>
					<td align="right">
						<?php echo isset($valuesData->details[$inde2]->balanceout)?number_format($valuesData->details[$inde2]->balanceout,2):''; 
						$credit+=($inde2==0?0:$valuesData->details[$inde2]->balanceout); ?>
					</td>
					<td><?php echo $valuesData->details[$inde2]->remark; ?></td>
				</tr>
				<?php endfor; ?>
				<tr>
					<td align="center"></td>
					<td>Total</td>
					<td align="right"><?php echo number_format($debit,2); ?></td>
					<td align="right"><?php echo number_format($credit,2); ?></td>
					<td align="right"></td>
				</tr>
				<tr>
					<td align="center"></td>
					<td>Balance</td>
					<td align="right"><?php echo number_format($debit-$credit,2); ?></td>
					<td align="right"></td>
					<td align="right"></td>
				</tr>
			</table>
		</div>
		<?php endfor; ?>
	<?php endforeach; ?>
</body>
</html>