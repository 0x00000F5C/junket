main.registerCtrl("journalController",function($scope,$location,$http,DTOptionsBuilder, DTColumnDefBuilder,$routeParams,$filter){
	$scope.alert = "";
	$scope.success = "";
	$scope.fd=[];
	
	$scope.slug=($routeParams.genting && $routeParams.genting=="genting" || ($routeParams.id && $routeParams.id=="genting"))?"genting":"";
	$scope.company_id=$scope.slug=="genting"?2:1;

	$scope.add=function(){
		$location.path("master/journals/create/"+$scope.slug);
	}

	$scope.dateOptions = {
		changeYear: true,
		changeMonth: true,
		dateFormat: "dd/M/yy",
		yearRange: '1900:-0',
    };
	
	$scope.initData=function(){
		if($routeParams.id){
			return false;
		}
		$scope.fd.date=new Date();
		$scope.getNumber();
	}
	
	$scope.getNumber = function() {
		$http({
			method: "get",
			url: "api/journal/number",
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
		   }
		}).
		then(function(response){
			$scope.fd.je_num=response.data.data;
		}, function(response){
			$scope.alert=response.data.message;
		});
	}
	
	$scope.accountOption = [];
	$scope.getAccount = function() {
		$http({
			method: "get",
			url: "api/account",
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
		   }
		}).
		then(function(response){
			$scope.accountOption=response.data.data;
			// $scope.getEditDetail();
		}, function(response){
			$scope.alert=response.data.message;
		});
	}
	
	$scope.listDetail=[];
	$scope.inc=0;
	$scope.addDetail=function(){
		if(!$scope.fd.account_id){
			alert("Please select account!");
			return false;
		}

		account="";
		loop1:for(inde=0;inde<$scope.accountOption.length;inde++){
			if($scope.accountOption[inde].id==$scope.fd.account_id){
				account=$scope.accountOption[inde].name;
				break loop1;
			}
		}

		detail={
			inc: $scope.inc,
			id: 0,
			account_id: $scope.fd.account_id,
			account_name: account,
			debit: parseFloat($scope.fd.debit?$scope.fd.debit:0),
			credit: parseFloat($scope.fd.credit?$scope.fd.credit:0)
		};

		$scope.fd.debit=0;
		$scope.fd.credit=0;
		$scope.listDetail.push(detail);
		$scope.inc++;
		
		// $scope.calBalance();
	}
	
	$scope.balance=0;
	$scope.calBalance=function(){
		$scope.balance=0;
		for(inde=0;inde<$scope.listDetail.length;inde++){
			$scope.balance+=parseFloat($scope.listDetail[inde].debit?$scope.listDetail[inde].debit:0);
			$scope.balance-=parseFloat($scope.listDetail[inde].credit?$scope.listDetail[inde].credit:0);
		}
		if($scope.balance!=0){
			$scope.alert='Journal isn\'t balanced!';
		}else{
			$scope.alert='';
		}
	}
	
	$scope.editForm=function(row){
		$scope.selected = angular.copy(row);
		setTimeout(function(){$("#selected_debit").focus()},1);
	}

	$scope.editDetail=function(inde){
		$scope.listDetail[inde]=$scope.selected;
		$scope.selected={};
		setTimeout(function(){$("#debit").focus()},1);
		// $scope.calBalance();
	}

	$scope.deleteId = null;
	$scope.modalDelete = false;
	$scope.deleteModal=function(inde) {
		$scope.deleteId = inde;
		$scope.modalDelete = true;
	}

	$scope.deleteArray=[];
	$scope.deleteDetail=function(){
		if($scope.listDetail[$scope.deleteId].id){
			$scope.deleteArray.push($scope.listDetail[$scope.deleteId].id);
		}

		$scope.listDetail.splice($scope.deleteId,1);

		$scope.selected={};
		$scope.modalDelete = false;
		$scope.deleteId = null;
		// $scope.calBalance();
	}

	$scope.selected={};
	$scope.getTemplate = function (row) {
		if (row.inc === $scope.selected.inc){
			return 'edit';
		}else{
			return 'display';
		}
	}
	
	$scope.idJournal=0;
	$scope.save=function(){
		if($scope.balance!=0){
			$scope.alert='Journal isn\'t balanced!';
			return false;
		}
		for(inde = 0; inde < $scope.deleteArray.length; inde++) {
			$http({
				method: "delete", 
				url: 'api/journal_detail/'+$scope.deleteArray[inde],
				headers: {
					"Content-Type":"application/json",
					"X-Auth-Token":$scope.token
				}
			}).
			then(function(response){
			}, function(response){
				$scope.alert=response.data.message;
			});

			if (inde==($scope.deleteArray.length)-1){
				console.log('delete');
				$scope.saveDetail();
			}
		}
		// return false;
		if ($scope.deleteArray.length==0){
			$scope.saving();
		}
	}
	
	$scope.saving=function(){
		$scope.fd.date=$filter('date')($scope.fd.date, 'yyyy-MM-dd');
		$scope.fd.company_id=$scope.company_id;
 		if (!$scope.fd.id) {
 			$http({
				method: "post",
				url: 'api/journal',
				headers: {
					"Content-Type":"application/json",
					"X-Auth-Token":$scope.token
				},
				params: $scope.fd
			}).
			then(function(response){
				$scope.idJournal=response.data.id;
				$scope.saveDetail();
			}, function(response){
				$scope.alert=response.data.message;
			});
 		}

		if($scope.fd.id){
 			$http({
				method: "put",
				url: 'api/journal/'+$scope.fd.id,
				headers: {
					"Content-Type":"application/json",
					"X-Auth-Token":$scope.token
				},
				params: $scope.fd
			}).
			then(function(response){
				$scope.idJournal=response.data.id;
				$scope.saveDetail();
			}, function(response){
				$scope.alert=response.data.message;
			});
 		}
	}
	
	$scope.saveDetail=function(){
		$scope.success = '';
		for(inde2=0;inde2<$scope.listDetail.length;inde2++){

			params={
				account_id: $scope.listDetail[inde2].account_id,
				debit: $scope.listDetail[inde2].debit,
				credit: $scope.listDetail[inde2].credit
			};
			
			if($scope.listDetail[inde2].id==0){
				$http({
					method: "post",
					url: 'api/journal_detail/'+$scope.idJournal,
					headers: {
						"Content-Type":"application/json",
						"X-Auth-Token":$scope.token
					},
					params: params
				}).
				then(function(response){
					// console.log(response.data.message);
					$scope.success = response.data.message;
					if(inde2==($scope.listDetail.length)){
						$scope.cancel();
					}
				}, function(response){
					$scope.alert=response.data.message;
				});
			}

			if($scope.listDetail[inde2].id!=0){
				$http({
					method: "put",
					url: 'api/journal_detail/'+$scope.listDetail[inde2].id,
					headers: {
						"Content-Type":"application/json",
						"X-Auth-Token":$scope.token
					},
					params: params
				}).
				then(function(response){
					// console.log(response.data.message);
					$scope.success = response.data.message;
					if(inde2==($scope.listDetail.length)){
						$scope.cancel();
					}
				}, function(response){
					$scope.alert=response.data.message;
				});
			}
		}

	}

	$scope.cancel=function(){
		$location.path("master/journals/"+$scope.slug);
	}

    $scope.dtOptions = DTOptionsBuilder.newOptions();
    $scope.dtColumnDefs = [
        DTColumnDefBuilder.newColumnDef(0),
        DTColumnDefBuilder.newColumnDef(1),
        DTColumnDefBuilder.newColumnDef(2),
        DTColumnDefBuilder.newColumnDef(3),
        DTColumnDefBuilder.newColumnDef(4),
        DTColumnDefBuilder.newColumnDef(5),
        DTColumnDefBuilder.newColumnDef(6),
        DTColumnDefBuilder.newColumnDef(7),
        DTColumnDefBuilder.newColumnDef(8),
        DTColumnDefBuilder.newColumnDef(9).notSortable()
    ];
    $scope.dtInstance = {};

	$scope.search='';
	
    $scope.sortType     = 'je_num'; // set the default sort type
	$scope.sortReverse  = false;  // set the default sort order
	$scope.searchList   = '';     // set the default search/filter term
	$scope.page			= 1;

	$scope.listData = [];
	$scope.loadData=function() {

		if ($scope.sortReverse == true) {
			$scope.sort_method = 'desc';
		} else {
			$scope.sort_method = 'asc';
		}
		
		params="";
		if($scope.date){
			$scope.date=$filter('date')($scope.date, 'yyyy-MM-dd');
			params="&date="+$scope.date;
		}else{
			$scope.date='';
		}
		
		$http({
			method: "get",
			url: "api/journal?sort_by="+$scope.sortType+"&sort_type="+$scope.sort_method+"&page="+$scope.page+"&company_id="+$scope.company_id+params,
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
		   }
		}).
		then(function(response){
			$scope.listData = response.data;
		}, function(response){
			$scope.alert=response.data.message;
		});
	}
	
	$scope.sort = function(sortType) {
		$scope.sortType = sortType;
		$scope.sortReverse = !$scope.sortReverse;
		$scope.loadData();
	}

	$scope.showPage=function(en){
		return (en>=$scope.listData.current_page && en<=$scope.listData.current_page+2) || (en<=$scope.listData.current_page && (en>=$scope.listData.current_page-2 || en>=$scope.listData.last_page-2));
	}

	$scope.isLastPage=function(){
		return $scope.listData.current_page==$scope.listData.last_page;
	}

	$scope.paging=function(page){
		if ($scope.listData.last_page>0) {
			$scope.page=page;
			$scope.loadData();
		}
	}
	
    $scope.edit=function(id) {
		$location.path("master/journals/edit/"+id+"/"+$scope.slug);
    }

    $scope.show=function(id) {
		$location.path("master/journals/show/"+id+"/"+$scope.slug);
    }

	$scope.showStatus=false;
	$scope.getEdit=function() {
		if(!$routeParams.id){
			return false;
		}

		if($routeParams.show){
			if($routeParams.show!="show"){
				return false;
			}
			$scope.showStatus=true;
		}

		$http({
			method: "get",
			url: "api/journal/"+$routeParams.id,
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
		   }
		}).
		then(function(response){
			$scope.fd = response.data.data;
			$scope.getEditDetail();
		}, function(response){
			$scope.alert=response.data.message;
		});
	}

	$scope.getEditDetail=function(){
		if(!$routeParams.id){
			return false;
		}
		console.log("editDetail");

		$scope.listDetail={};
		$http({
			method: "get",
			url: "api/journal_detail_mst/"+$routeParams.id,
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
		   }
		}).
		then(function(response){
			$scope.listDetail=response.data.data;
			$scope.checkDataDetail(0);
		}, function(response){
			$scope.alert=response.data.message;
		});
	}

	$scope.checkDataDetail=function(indeks){
		if(indeks==$scope.listDetail.length){
			$scope.inc=indeks+1;
			return false;
		}
		statusNext=0;
		loop1:
		for(inde=0;inde<$scope.accountOption.length;inde++){
			// if($scope.accountOption[inde].id==$scope.listDetail[indeks].account_id){
				$scope.listDetail[indeks].inc=indeks;
				$scope.listDetail[indeks].account_name=$scope.listDetail[indeks].account.name;
				statusNext=1;
				break loop1;
			// }
		}
		
		$scope.checkDataDetail(indeks+1);
	}
	
	$scope.delete=function(id) {
		$http({
			method: "delete",
			url: 'api/journal/'+id,
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}
		}).
		then(function(response){
			$scope.success="Data deleted!";
			$scope.loadData();
		}, function(response){
			$scope.alert=response.data.message;
		});
	}
});
