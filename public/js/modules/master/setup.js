main.registerCtrl("SetUpController",function($scope,$filter,$location,$http,DTOptionsBuilder, DTColumnDefBuilder,$routeParams){

	$scope.dateOptions = {
		changeYear: true,
		changeMonth: true,
		dateFormat: "dd/M/yy",
		yearRange: '1900:-0',
  };

	$scope.fd = [];

	$scope.tab = 1;
	$scope.get_set_up = function() {
		if($routeParams.menu=="bank"){
			$scope.setTab(2);
			console.log($scope.tab);
		}
		$http({method: "get", url: "api/companies", headers: {
				    "Content-Type":"application/json",
				    "X-Auth-Token":$scope.token
				   }
		}).
		then(function(response){
			if(response.data.data==null){return false;}
			response.data.data.nn_chips = parseFloat(response.data.data.nn_chips).toFixed(2);
			response.data.data.junket_chips = parseFloat(response.data.data.junket_chips).toFixed(2);
			response.data.data.cash_chips = parseFloat(response.data.data.cash_chips).toFixed(2);
			response.data.data.cash_real = parseFloat(response.data.data.cash_real).toFixed(2);
			$scope.fd = response.data.data;
		}, function(response){
			$scope.alert=response.data.message;
		});
	}
	
	$scope.loadInfo=0;
	$scope.getInfo = function() {
		if($scope.loadInfo==0){
			$scope.getInformation();
		}
	}
	
	$scope.getInformation=function(){
		$scope.loadInfo=1;
		if($routeParams.menu=="bank"){
			$scope.setTab(2);
			console.log($scope.tab);
		}
		$http({method: "get", url: "api/companies/info", headers: {
				    "Content-Type":"application/json",
				    "X-Auth-Token":$scope.token
				   }
		}).
		then(function(response){
			if(response.data.data==null){return false;}
			// response.data.data.open_balance_asset = (parseFloat(response.data.data.nn_chips)+
				// parseFloat(response.data.data.junket_chips)+parseFloat(response.data.data.cash_chips)+
				// parseFloat(response.data.data.cash_real)).toFixed(2);
			response.data.data.nn_chips = parseFloat(response.data.data.nn_chips).toFixed(2);
			response.data.data.junket_chips = parseFloat(response.data.data.junket_chips).toFixed(2);
			response.data.data.cash_chips = parseFloat(response.data.data.cash_chips).toFixed(2);
			response.data.data.cash_real = parseFloat(response.data.data.cash_real).toFixed(2);
			response.data.data.bank_balance = parseFloat(response.data.data.bank_balance).toFixed(2);
			response.data.data.open_balance_asset = parseFloat(response.data.data.open_balance_asset).toFixed(2);
			$scope.fd = response.data.data;
			$scope.fd.capital=$scope.fd.nn_chips+$scope.fd.cash_chips+$scope.fd.cash_real+
				$scope.fd.bank_balance+$scope.fd.open_balance_asset-
				$scope.fd.agent_share-$scope.fd.boss_share;
			$scope.loadInfo=0;
		}, function(response){
			$scope.alert=response.data.message;
			$scope.loadInfo=0;
		});
	}

	$scope.formSetUp = function() {
		console.log('asd');

						$scope.fd.date = $filter('date')($scope.fd.date, 'yyyy-MM-dd');
						console.log($scope.fd.date);

			if ($scope.fd.id > 0) {

				$http({method: "put", url: 'api/companies/'+$scope.fd.id, headers: {
						"Content-Type":"application/json",
						"X-Auth-Token":$scope.token
					}, params: $scope.fd
				}).
				then(function(response){
					$scope.dataTable = response.data.data;
					$scope.get_set_up();
					$scope.success="Data updated!";
				}, function(response){
					$scope.alert=response.data.message;

				});

			} else {

				$http({method: "post", url: 'api/companies', headers: {
						"Content-Type":"application/json",
						"X-Auth-Token":$scope.token
					}, params: $scope.fd
				}).
				then(function(response){
					$scope.get_set_up();
				}, function(response){
					$scope.alert=response.data.message;

				});

			}
	};

	$scope.alert="";
	$scope.showAlert=function(){
			return $scope.alert!="" && $scope.alert!="success";
	}
	
	$scope.success="";
	$scope.showSuccess=function(){
			return $scope.success!="";
	}

  $scope.setTab = function(newTab){
    $scope.tab = newTab;
  };

  $scope.isSet = function(tabNum){
    return $scope.tab === tabNum;
  };

	$scope.accountOption=[];
	$scope.getAccountOption=function(){
		$http({
			method: "get",
			url: "api/account",
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}
		}).
		then(function(response){
			$scope.accountOption = response.data.data;
		}, function(response){
			$scope.alert=response.data.message;
		});
	}
  
	$scope.fmb = [];

	$scope.listDataManageBank = [];
	$scope.getManageBank = function() {
		$scope.getInfo();
		$http({method: "get", url: "api/banks", headers: {
				    "Content-Type":"application/json",
				    "X-Auth-Token":$scope.token
				   }
		}).
		then(function(response){
			$scope.alert = "";
			$scope.listDataManageBank = response.data.data;
		}, function(response){
			$scope.alert=response.data.message;
		});
	}

	$scope.formManageBank = function() {
		$http({method: "post", url: 'api/banks', headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}, params: $scope.fmb
		}).
		then(function(response){
			$scope.getManageBank();
			$scope.fmb =[];
			$scope.success="Data saved!";
		}, function(response){
			$scope.alert=response.data.message;

		});
	};
	
	$scope.selectedB=[];
	$scope.editFormB=function(row){
		$scope.selectedB = angular.copy(row);
		$scope.selectedB.account_id = parseInt($scope.selectedB.account_id);
		setTimeout(function(){$("#name_b").focus()},1);
	}
	
	$scope.getTemplateB=function(row) {
		if (row.id === $scope.selectedB.id){
			return 'edit';
		}else{
			return 'display';
		}
	}
	
	$scope.editBank = function(id) {
		$http({method: "put", url: 'api/banks/'+id, headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}, params: $scope.selectedB
		}).
		then(function(response){
			$scope.getManageBank();
			$scope.selectedB =[];
			$scope.success="Data updated!";
		}, function(response){
			$scope.alert=response.data.message;

		});
	};
	
	$scope.deleteBank = function(id) {
		$http({method: "delete", url: 'api/banks/'+id, headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}
		}).
		then(function(response){
			$scope.getManageBank();
			$scope.selectedB =[];
			$scope.success="Data deleted!";
		}, function(response){
			$scope.alert=response.data.message;

		});
	};
	
	//manager balance
	$scope.managerOption=[];
	$scope.getManagerOption = function() {
		$http({
			method: "get",
			url: "api/users/4/find_by_role",
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}
		}).
		then(function(response){
			$scope.managerOption = response.data.data;
		}, function(response){
			$scope.alert=response.data.message;
		});
	}

	$scope.fmbal = [];

	$scope.listDataManagerBalance = [];
	$scope.getManagerBalance = function() {
		$scope.getInfo();
		$http({method: "get", url: "api/open_balances", headers: {
				    "Content-Type":"application/json",
				    "X-Auth-Token":$scope.token
				   }
		}).
		then(function(response){
			$scope.alert = "";
			$scope.listDataManagerBalance = response.data.data;

		}, function(response){
			$scope.alert=response.data.message;
		});
	}

	$scope.formManagerBalance = function() {
		$http({method: "post", url: 'api/open_balances', headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}, params: $scope.fmbal
		}).
		then(function(response){
			$scope.getManagerBalance();
			$scope.success="Data saved!";
			$scope.fmbal=[];
		}, function(response){
			$scope.alert=response.data.message;

		});
	};

	$scope.selectedMB=[];
	$scope.editFormMB=function(row){
		$scope.selectedMB = angular.copy(row);
		setTimeout(function(){$("#nn_chips_mb").focus()},1);
	}
	
	$scope.getTemplateMB=function(row) {
		if (row.id === $scope.selectedMB.id){
			return 'editM';
		}else{
			return 'displayM';
		}
	}
	
	$scope.editManagerBalance = function(id) {
		$http({method: "put", url: 'api/open_balances/'+id, headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}, params: $scope.selectedMB
		}).
		then(function(response){
			$scope.getManagerBalance();
			$scope.selectedMB =[];
			$scope.success="Data updated!";
		}, function(response){
			$scope.alert=response.data.message;

		});
	};
	
	$scope.deleteManagerBalance = function(id) {
		$http({method: "delete", url: 'api/open_balances/'+id, headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}
		}).
		then(function(response){
			$scope.getManagerBalance();
			$scope.selectedMB =[];
			$scope.success="Data deleted!";
		}, function(response){
			$scope.alert=response.data.message;

		});
	};
	
	//agent share
	$scope.agentOption=[];
	$scope.getAgentOption = function() {
		$http({
			method: "get",
			url: "api/agent",
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}
		}).
		then(function(response){
			$scope.agentOption = response.data.data;
		}, function(response){
			$scope.alert=response.data.message;
		});
	}

	$scope.fas = [];

	$scope.listDataAgentShare = [];
	$scope.getAgentShare = function() {
		$scope.getInfo();

		$http({method: "get", url: "api/agent_share", headers: {
				    "Content-Type":"application/json",
				    "X-Auth-Token":$scope.token
				   }
		}).
		then(function(response){
			$scope.alert = "";
			$scope.listDataAgentShare = response.data.data;

		}, function(response){
			$scope.alert=response.data.message;
		});
	}

	$scope.formAgentShare = function() {
		$http({method: "post", url: 'api/agent_share', headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}, params: $scope.fas
		}).
		then(function(response){
			$scope.getAgentShare();
			$scope.success="Data saved!";
			$scope.fas=[];
		}, function(response){
			$scope.alert=response.data.message;

		});
	};

	$scope.selectedAS=[];
	$scope.editFormAS=function(row){
		$scope.selectedAS = angular.copy(row);
		setTimeout(function(){$("#amount_as").focus()},1);
	}
	
	$scope.getTemplateAS=function(row) {
		if (row.id === $scope.selectedAS.id){
			return 'editA';
		}else{
			return 'displayA';
		}
	}
	
	$scope.editAgentShare = function(id) {
		$http({method: "put", url: 'api/agent_share/'+id, headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}, params: $scope.selectedAS
		}).
		then(function(response){
			$scope.getAgentShare();
			$scope.selectedAS =[];
			$scope.success="Data updated!";
		}, function(response){
			$scope.alert=response.data.message;

		});
	};
	
	$scope.deleteAgentShare = function(id) {
		$http({method: "delete", url: 'api/agent_share/'+id, headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}
		}).
		then(function(response){
			$scope.getAgentShare();
			$scope.selectedAS =[];
			$scope.success="Data deleted!";
		}, function(response){
			$scope.alert=response.data.message;

		});
	};
	
	//boss share
	$scope.bossOption=[];
	$scope.getBossOption = function() {
		$http({
			method: "get",
			url: "api/users/2/find_by_role",
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}
		}).
		then(function(response){
			$scope.bossOption = response.data.data;
		}, function(response){
			$scope.alert=response.data.message;
		});
	}

	$scope.fbs = [];

	$scope.listDataBossShare = [];
	$scope.getBossShare = function() {
		$scope.getInfo();

		$http({method: "get", url: "api/boss_share", headers: {
				    "Content-Type":"application/json",
				    "X-Auth-Token":$scope.token
				   }
		}).
		then(function(response){
			$scope.alert = "";
			$scope.listDataBossShare = response.data.data;

		}, function(response){
			$scope.alert=response.data.message;
		});
	}

	$scope.formBossShare = function() {
		$http({method: "post", url: 'api/boss_share', headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}, params: $scope.fbs
		}).
		then(function(response){
			$scope.getBossShare();
			$scope.success="Data saved!";
			$scope.fbs=[];
		}, function(response){
			$scope.alert=response.data.message;

		});
	};

	$scope.selectedBS=[];
	$scope.editFormBS=function(row){
		$scope.selectedBS = angular.copy(row);
		setTimeout(function(){$("#amount_bs").focus()},1);
	}
	
	$scope.getTemplateBS=function(row) {
		if (row.id === $scope.selectedBS.id){
			return 'editBS';
		}else{
			return 'displayBS';
		}
	}
	
	$scope.editBossShare = function(id) {
		$http({method: "put", url: 'api/boss_share/'+id, headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}, params: $scope.selectedBS
		}).
		then(function(response){
			$scope.getBossShare();
			$scope.selectedBS =[];
			$scope.success="Data updated!";
		}, function(response){
			$scope.alert=response.data.message;

		});
	};
	
	$scope.deleteBossShare = function(id) {
		$http({method: "delete", url: 'api/boss_share/'+id, headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}
		}).
		then(function(response){
			$scope.getBossShare();
			$scope.selectedBS =[];
			$scope.success="Data deleted!";
		}, function(response){
			$scope.alert=response.data.message;

		});
	};
	
	//agent balance
	$scope.fabal = [];

	$scope.listDataAgentBalance = [];
	$scope.getAgentBalance = function() {
		$scope.getInfo();
		$http({method: "get", url: "api/open_balance_agent", headers: {
				    "Content-Type":"application/json",
				    "X-Auth-Token":$scope.token
				   }
		}).
		then(function(response){
			$scope.alert = "";
			$scope.listDataAgentBalance = response.data.data;

		}, function(response){
			$scope.alert=response.data.message;
		});
	}

	$scope.formAgentBalance = function() {
		$http({method: "post", url: 'api/open_balance_agent', headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}, params: $scope.fabal
		}).
		then(function(response){
			$scope.getAgentBalance();
			$scope.success="Data saved!";
			$scope.fabal=[];
		}, function(response){
			$scope.alert=response.data.message;

		});
	};

	$scope.selectedAB=[];
	$scope.editFormAB=function(row){
		$scope.selectedAB = angular.copy(row);
		setTimeout(function(){$("#junket_chips_ab").focus()},1);
	}
	
	$scope.getTemplateAB=function(row) {
		if (row.id === $scope.selectedAB.id){
			return 'editAB';
		}else{
			return 'displayAB';
		}
	}
	
	$scope.editAgentBalance = function(id) {
		$http({method: "put", url: 'api/open_balance_agent/'+id, headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}, params: $scope.selectedAB
		}).
		then(function(response){
			$scope.getAgentBalance();
			$scope.selectedAB =[];
			$scope.success="Data updated!";
		}, function(response){
			$scope.alert=response.data.message;

		});
	};
	
	$scope.deleteAgentBalance = function(id) {
		$http({method: "delete", url: 'api/open_balance_agent/'+id, headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}
		}).
		then(function(response){
			$scope.getAgentBalance();
			$scope.selectedAB =[];
			$scope.success="Data deleted!";
		}, function(response){
			$scope.alert=response.data.message;

		});
	};
	
	//boss balance
	$scope.fbbal = [];

	$scope.listDataBossBalance = [];
	$scope.getBossBalance = function() {
		$scope.getInfo();
		$http({method: "get", url: "api/open_balance_boss", headers: {
				    "Content-Type":"application/json",
				    "X-Auth-Token":$scope.token
				   }
		}).
		then(function(response){
			$scope.alert = "";
			$scope.listDataBossBalance = response.data.data;

		}, function(response){
			$scope.alert=response.data.message;
		});
	}

	$scope.formBossBalance = function() {
		$http({method: "post", url: 'api/open_balance_boss', headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}, params: $scope.fbbal
		}).
		then(function(response){
			$scope.getBossBalance();
			$scope.success="Data saved!";
			$scope.fbbal=[];
		}, function(response){
			$scope.alert=response.data.message;

		});
	};

	$scope.selectedBB=[];
	$scope.editFormBB=function(row){
		$scope.selectedBB = angular.copy(row);
		setTimeout(function(){$("#junket_chips_bb").focus()},1);
	}
	
	$scope.getTemplateBB=function(row) {
		if (row.id === $scope.selectedBB.id){
			return 'editBB';
		}else{
			return 'displayBB';
		}
	}
	
	$scope.editBossBalance = function(id) {
		$http({method: "put", url: 'api/open_balance_boss/'+id, headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}, params: $scope.selectedBB
		}).
		then(function(response){
			$scope.getBossBalance();
			$scope.selectedBB =[];
			$scope.success="Data updated!";
		}, function(response){
			$scope.alert=response.data.message;

		});
	};
	
	$scope.deleteBossBalance = function(id) {
		$http({method: "delete", url: 'api/open_balance_boss/'+id, headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}
		}).
		then(function(response){
			$scope.getBossBalance();
			$scope.selectedBB =[];
			$scope.success="Data deleted!";
		}, function(response){
			$scope.alert=response.data.message;

		});
	};
	
});
