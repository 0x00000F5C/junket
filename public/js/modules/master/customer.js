main.registerCtrl("customerController",function($scope,$location,$http,DTOptionsBuilder, DTColumnDefBuilder,$routeParams){
	idRol = $routeParams.id;
	$scope.alert = "";
	$scope.success = "";
	$scope.fd=[];

	$scope.add=function(){
		$location.path("master/customers/create");
	}

	$scope.queryOptions = {
        query: function (query) {
            var data = {
                results: [
                    { id: "1", text: "A" },
                    { id: "2", text: "B" }
                ]
            };

            query.callback(data);
        }
    };
	
	$scope.maxYear=new Date().getFullYear()-1;
	
	$scope.custTypesOption = [];
	$scope.getCustTypes = function() {
		$http({
			method: "get",
			url: "api/customer/get_customer_types",
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}
		}).
		then(function(response){
			$scope.custTypesOption = response.data.data;

		}, function(response){
			$scope.alert=response.data.message;
		});
	}

	$scope.initData=function(){
		$scope.fd.date=1;
		$scope.fd.month="01";
		$scope.fd.year=(new Date().getFullYear())-50;
		$scope.calculateAge();
	}

	$scope.calculateAge=function() {
		var start = new Date(($scope.fd.year?$scope.fd.year:0)+"-"+$scope.fd.month+"-"+$scope.fd.date);
		var end       = new Date();
		var age_year  = Math.floor((end - start)/31536000000);
		var age_month = Math.floor(((end - start)% 31536000000)/2628000000);
		var age_day   = Math.floor((((end - start)% 31536000000) % 2628000000)/86400000);
		$scope.fd.age = age_year;
	}
	
	$scope.fd.phone_code='+60';
	$scope.phoneCodeOption=[];
	$scope.getPhoneCode=function(){
		$http.get('public/data/phone.json', {}).then(function(response){$scope.phoneCodeOption=response.data.countries}, function(response){});
	}
	
	$scope.agentOption = [];
	$scope.getAgent = function() {
		$http({
			method: "get",
			url: "api/agent",
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}
		}).
		then(function(response){
			$scope.agentOption = response.data.data;

		}, function(response){
			$scope.alert=response.data.message;
		});
	}

	$scope.save=function() {
 		if (!$scope.fd.id) {
 			$http({
				method: "post",
				url: 'api/customer/save',
				headers: {
					"Content-Type":"application/json",
					"X-Auth-Token":$scope.token
				},
				params: $scope.fd
			}).
			then(function(response){
				$scope.cancel();
			}, function(response){
				$scope.alert=response.data.message;
			});
 		}

		if($scope.fd.id){
 			$http({
				method: "put",
				url: 'api/customer/update',
				headers: {
					"Content-Type":"application/json",
					"X-Auth-Token":$scope.token
				},
				params: $scope.fd
			}).
			then(function(response){
				$scope.cancel();
			}, function(response){
				$scope.alert=response.data.message;
			});
 		}
  }

	$scope.cancel=function(){
		$location.path("master/customers");
	}

    $scope.dtOptions = DTOptionsBuilder.newOptions();
    $scope.dtColumnDefs = [
        DTColumnDefBuilder.newColumnDef(0),
        DTColumnDefBuilder.newColumnDef(1),
        DTColumnDefBuilder.newColumnDef(2),
        DTColumnDefBuilder.newColumnDef(3),
        DTColumnDefBuilder.newColumnDef(4),
        DTColumnDefBuilder.newColumnDef(5),
        DTColumnDefBuilder.newColumnDef(6),
        DTColumnDefBuilder.newColumnDef(7),
        DTColumnDefBuilder.newColumnDef(8),
        DTColumnDefBuilder.newColumnDef(9).notSortable()
    ];
    $scope.dtInstance = {};

	$scope.search='';
	
    $scope.sortType     = 'first_name'; // set the default sort type
	$scope.sortReverse  = false;  // set the default sort order
	$scope.searchList   = '';     // set the default search/filter term
	$scope.page			= 1;

	$scope.listData = [];
	$scope.loadData=function() {

		if ($scope.sortReverse == true) {
			$scope.sort_method = 'desc';
		} else {
			$scope.sort_method = 'asc';
		}

		$http({
			method: "get",
			url: "api/customer/all?sort_by="+$scope.sortType+"&sort_type="+$scope.sort_method+"&page="+$scope.page+"&search="+$scope.search,
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
		   }
		}).
		then(function(response){
			$scope.listData = response.data;
		}, function(response){
			$scope.alert=response.data.message;
		});
	}
	
	$scope.sort = function(sortType) {
		$scope.sortType = sortType;
		$scope.sortReverse = !$scope.sortReverse;
		$scope.loadData();
	}

	$scope.showPage=function(en){
		return (en>=$scope.listData.current_page && en<=$scope.listData.current_page+2) || (en<=$scope.listData.current_page && (en>=$scope.listData.current_page-2 || en>=$scope.listData.last_page-2));
	}

	$scope.isLastPage=function(){
		return $scope.listData.current_page==$scope.listData.last_page;
	}

	$scope.paging=function(page){
		if ($scope.listData.last_page>0) {
			$scope.page=page;
			$scope.loadData();
		}
	}

  $scope.show=function(id) {
		$location.path("master/customers/show/"+id);
  }

  $scope.edit=function(id) {
		$location.path("master/customers/edit/"+id);
  }

	$scope.showStatus=false;
	$scope.getEdit=function() {
		if(!$routeParams.id){
			return false;
		}

		if($routeParams.show){
			$scope.showStatus=true;
		}

		$http({
			method: "get",
			url: "api/customer/"+$routeParams.id+"/get",
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
		   }
		}).
		then(function(response){
			response.data.data.date=parseInt(response.data.data.date);
			response.data.data.year=parseInt(response.data.data.year);
			response.data.data.customer_type_id=parseInt(response.data.data.customer_type_id);
			response.data.data.agent_id=parseInt(response.data.data.agent_id);
			$scope.fd = response.data.data;
		}, function(response){
			$scope.alert=response.data.message;
		});
	}

	$scope.delete=function(id) {
		$http({
			method: "delete",
			url: 'api/customer/'+id+'/delete',
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}
		}).
		then(function(response){
			$scope.success="Data deleted!";
			$scope.loadData();
		}, function(response){
			$scope.alert=response.data.message;
		});
	}
	
	$scope.showTable = false;

	if( $routeParams.show == "show") {
		$scope.showTable = true;
		$scope.tab = 1;

	  $scope.setTab = function(newTab){
	    $scope.tab = newTab;
	  };

	  $scope.isSet = function(tabNum){
	    return $scope.tab === tabNum;
	  };

		$scope.dtOptionsRolByTrip = DTOptionsBuilder.newOptions();
		$scope.dtColumnDefsRolByTrip = [
				DTColumnDefBuilder.newColumnDef(0),
				DTColumnDefBuilder.newColumnDef(1),
				DTColumnDefBuilder.newColumnDef(2),
		];
		$scope.dtInstanceRolByTrip = {};

		$scope.listDataRolHisByTrip = [];
		$scope.listDataDebt = [];
		$scope.loadDataRolHisByTrip = function() {
			$http({
				method: "get",
				url: "api/customer/"+idRol+"/get",
				headers: {
					"Content-Type":"application/json",
					"X-Auth-Token":$scope.token
				 }
			}).
			then(function(response){
				$scope.listDataRolHisByTrip = response.data.histories;
				$scope.listDataDebt = response.data.customer_debt;
			}, function(response){
				$scope.alert=response.data.message;
			});
		}

		$scope.dtOptionsDebt = DTOptionsBuilder.newOptions();
		$scope.dtColumnDefsDebt = [
				DTColumnDefBuilder.newColumnDef(0),
				DTColumnDefBuilder.newColumnDef(1),
				DTColumnDefBuilder.newColumnDef(2),
		];
		$scope.dtInstanceDebt = {};
	}
});
