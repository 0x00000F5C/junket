main.registerCtrl("TableRollingController", function($scope,$http,$routeParams,$filter) {
	id=$routeParams.id;
	$scope.master=[];
	$scope.open=[];
	$scope.rolling=0.00;
	$scope.confirmModal=false;
	$scope.success="";
	$scope.alert="";

	$scope.openRolling=function(){
		$http({
			method: "post",
			url: "api/rolling_tables/"+id,
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			},
			data: {
				rolling: $scope.master.nn_chips,
				flags: "open",
				time_transaction: (new Date().getHours())+":"+(new Date().getMinutes())
			}
		}).
		then(function(response){
			$scope.data=response.data.data;
			$scope.confirmModal=false;
			$scope.getOpenStatus();
		},function(response){
			$scope.alert=response.data.message;
		});
	}

	$scope.getRolling=function(){
		$http({
			method: "get",
			url: "api/transaction/"+id,
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}
		}).
		then(function(response){
			$scope.master = response.data.data;
		}, function(response){
			$scope.alert=response.data.message;
		});
	}

	// $scope.detail=[];
	// $scope.getRollingDetail=function(){
	// 	$http({
	// 		method: "get",
	// 		url: "api/transaction_detail/"+id,
	// 		headers: {
	// 			"Content-Type":"application/json",
	// 			"X-Auth-Token":$scope.token
	// 		},
	// 		params: {
	// 			flags: "role"
	// 		}
	// 	}).
	// 	then(function(response){
	// 		$scope.detail = response.data.data;
	// 		initTotal=0;
	// 		for(i=0;i<$scope.detail.length;i++){
	// 			initTotal+=parseFloat($scope.detail[i].rolling);
	// 			$scope.detail[i].total=initTotal;
	// 		}
	// 	}, function(response){
	// 		$scope.alert=response.data.message;
	// 	});
	// }

	// $scope.addRolling=function(){
	// 	$scope.loading=true;
	// 	$http({
	// 		method: "post",
	// 		url: "api/transaction_detail/"+id,
	// 		headers: {
	// 			"Content-Type":"application/json",
	// 			"X-Auth-Token":$scope.token
	// 		},
	// 		data: {
	// 			rolling: $scope.rolling,
	// 			flags: "role",
	// 			time_transaction: (new Date().getHours())+":"+(new Date().getMinutes())
	// 		}
	// 	}).
	// 	then(function(response){
	// 		$scope.data=response.data.data;
	// 		$scope.rolling=0;
	// 		// $("#rolling").focus();
	// 		$scope.getRollingDetail();
	// 		$scope.loading=false;
	// 	},function(response){
	// 		$scope.alert=response.data.message;
	// 		$scope.loading=false;
	// 	});
	// }

		$scope.detail=[];
	$scope.getTableRollingDetail=function(){
		$http({
			method: "get",
			url: "api/table_rolling/"+id,
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			},
			params : {
				transid : id
			}
		}).
		then(function(response){
			$scope.detail = response.data.data.data;
			initTotal=0;
			for(i=0;i<$scope.detail.length;i++){
				initTotal+=parseFloat($scope.detail[i].rolling);
				$scope.detail[i].total=initTotal;
			}
		}, function(response){
			$scope.alert=response.data.message;
		});
	}

	$scope.addTableRolling=function(){
		$scope.loading=true;
		$scope.trade_date=$filter('date')($scope.trade_date,'yyyy-MM-dd');
		console.log($scope.trade_date);
		// console.log("trade date: "$scope.trade_date);
		$http({
			method: "post",
			url: "api/table_rolling/"+id,
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			},
			data : {
				trade_date 	: $scope.trade_date,
				cash_chip  	: $scope.cash_chip,
				nn_chip 	: $scope.nn_chips,
				terminal	: $scope.terminal,
				transaction_id : id
			}
		}).
		then(function(response){
			$scope.data=response.data;
			console.log($scope.data);
			$scope.rolling=0;
			// $("#rolling").focus();
			$scope.getTableRollingDetail();
			$scope.loading=false;
		},function(response){
			$scope.alert=response.data.message;
			$scope.loading=false;
		});
	}

	$scope.selected={};
	$scope.editForm=function(row){
		$scope.selected = angular.copy(row);
		setTimeout(function(){$("#selected_rolling").focus()},1);
	}

	$scope.editRolling=function(detail_id){
		$scope.loading=true;
		$scope.selected.trade_date=$filter('date')($scope.selected.trade_date,'yyyy-MM-dd');
		$http({
			method: "put",
			url: "api/table_rolling/"+detail_id,
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			},
			data: {
				trade_date 	: $scope.selected.trade_date,
				cash_chip  	: $scope.selected.cash_chip,
				nn_chip 	: $scope.selected.nn_chip,
				terminal	: $scope.selected.terminal
			}
		}).
		then(function(response){
			console.log($scope.selected_terminal);
			$scope.data=response.data.data.data;
			$scope.getTableRollingDetail();
			$scope.selected={};
			$scope.loading=false;
		},function(response){
			$scope.alert=response.data.message;
			$scope.loading=false;
		});
	}

	$scope.deleteRolling=function(detail_id){
		$http({
			method: "delete",
			url: 'api/table_rolling/'+detail_id,
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}
		}).
		then(function(response){
			$scope.data = response.data.data;
			$scope.getTableRollingDetail();
			$scope.selected={};
		}, function(response){
			$scope.alert=response.data.message;
		});
	}

	$scope.getTemplate = function (row) {
		if (row.id === $scope.selected.id){
		  return 'edit';
		}else{
			return 'display';
		}
	}
});