main.registerCtrl("purchasesFromCustomerController",function($filter,$scope,$location,$http,$routeParams,DTOptionsBuilder, DTColumnDefBuilder){
	$scope.success="";
	$scope.alert = "";
	$scope.fd=[];
	$scope.fd.customer=[];

	$scope.add=function(){
		$location.path("app/purchases_from_customer/create");
	}

	$scope.dateOptions = {
		changeYear: true,
		changeMonth: true,
		dateFormat: "dd/M/yy",
		yearRange: '1900:-0',
    }

	$scope.initData=function(){
		$scope.fd.date=$filter('date')(new Date(), 'yyyy-MM-dd');
		$scope.fd.value=0;
	}
	
	$scope.customerOption=[];
	$scope.getCustomer = function() {
		$http({
			method: "get",
			url: "api/customer/get_lists",
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}
		}).
		then(function(response){
			$scope.customerOption = response.data.data;
		}, function(response){
			$scope.alert=response.data.message;
		});
	}

	$scope.accountOption=[];
	$scope.getAccount = function() {
		$http({
			method: "get",
			url: "api/account",
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
		    }
		}).
		then(function(response){
			$scope.accountOption = response.data.data;
		}, function(response){
			$scope.alert=response.data.message;
		});
	}

	$scope.changeCust=function(){
		loop1: for(inde=0;inde<$scope.customerOption.length;inde++){
			if($scope.customerOption[inde].id==$scope.fd.customer_id){
				$scope.fd.customer=[];
				$scope.fd.customer.customer_type=[];
				$scope.fd.customer.customer_type_id=$scope.customerOption[inde].customer_type_id;
				$scope.fd.customer.customer_type.name=$scope.customerOption[inde].customer_type.name;
				if($scope.fd.customer.customer_type_id==1){
					$scope.fd.commission_percent=1.1;
					$scope.fd.type_chip="NN Chips";
				}else if($scope.fd.customer.customer_type_id==2){
					$scope.fd.commission_percent=1.5;
					$scope.fd.type_chip="Junket NN Chips";
				}
				$scope.calCommission();
				break loop1;
			}
		}
	}
	
	$scope.calCommission=function(){
		$scope.fd.commission=($scope.fd.value?$scope.fd.value:0)*($scope.fd.commission_percent?$scope.fd.commission_percent:0)/100;
	}
	
	$scope.save = function() {
 		$scope.fd.user_id = $scope.user.id;
 		if (!$scope.fd.id) {
 			$http({
				method: "post",
				url: 'api/purchases_from_customer',
				headers: {
					"Content-Type":"application/json",
					"X-Auth-Token":$scope.token
				},
				params: $scope.fd
			}).
			then(function(response){
				$scope.success="Data saved!";
				$location.path("app/purchases_from_customer");
			}, function(response){
				$scope.alert=response.data.message;
			});
 		}

		if($scope.fd.id){
 			$http({
				method: "put",
				url: 'api/purchases_from_customer/'+$scope.fd.id,
				headers: {
					"Content-Type":"application/json",
					"X-Auth-Token":$scope.token
				},
				params: $scope.fd
			}).
			then(function(response){
				$scope.success="Data updated!";
				$location.path("app/purchases_from_customer");
			}, function(response){
				$scope.alert=response.data.message;
			});
 		}
	}

	$scope.cancel = function() {
		$location.path("app/purchases_from_customer");
	}

    $scope.sortType     = 'date'; // set the default sort type
	$scope.sortReverse  = true;  // set the default sort order
	$scope.searchList   = '';     // set the default search/filter term
	$scope.page			= 1;

    $scope.dtOptions = DTOptionsBuilder.newOptions();
    $scope.dtColumnDefs = [
        DTColumnDefBuilder.newColumnDef(0),
        DTColumnDefBuilder.newColumnDef(1),
        DTColumnDefBuilder.newColumnDef(2),
        DTColumnDefBuilder.newColumnDef(3),
        DTColumnDefBuilder.newColumnDef(4),
        DTColumnDefBuilder.newColumnDef(5),
        DTColumnDefBuilder.newColumnDef(6).notSortable()
    ];
    $scope.dtInstance = {};

	$scope.dataTable = [];

	$scope.loadData = function() {
		if ($scope.sortReverse == true) {
			$scope.sort_method = 'desc';
		} else {
			$scope.sort_method = 'asc';
		}

		if ($scope.date) {
			$scope.date = $filter('date')($scope.date, 'yyyy-MM-dd');
		} else {
			$scope.date = '';
		}

		$http({
			method: "get",
			url: "api/purchases_from_customer?sort_by="+$scope.sortType+"&sort_type="+$scope.sort_method+"&page="+$scope.page+"&date="+$scope.date,
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}
		}).
		then(function(response){
			$scope.dataTable = response.data;
		}, function(response){
			$scope.alert=response.data.message;
		});
	}

	$scope.sort = function(sortType) {
		$scope.sortType = sortType;
		$scope.sortReverse = !$scope.sortReverse;
		$scope.loadData();
	}

	$scope.showPage=function(en){
		return (en>=$scope.dataTable.current_page && en<=$scope.dataTable.current_page+2) || (en<=$scope.dataTable.current_page && (en>=$scope.dataTable.current_page-2 || en>=$scope.dataTable.last_page-2));
	}

	$scope.isLastPage=function(){
		return $scope.dataTable.current_page==$scope.dataTable.last_page;
	}

	$scope.paging=function(page){
		if ($scope.dataTable.last_page>0) {
			$scope.page=page;
			$scope.loadData();
		}
	}

	$scope.edit=function(id){
		$location.path("app/purchases_from_customer/edit/"+id);
	}

	$scope.getEdit = function() {
		if(!$routeParams.id){
			return false;
		}
		$http({
			method: "get",
			url: "api/purchases_from_customer/"+$routeParams.id,
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
		    }
		}).
		then(function(response){
			response.data.data.customer_id = parseInt(response.data.data.customer_id);
			response.data.data.account_id = parseInt(response.data.data.account_id);
			$scope.fd = response.data.data;
		}, function(response){
			$scope.alert=response.data.message;
		});
	}

	$scope.delete = function(id) {
		console.log(id);
		$http({method: "delete", url: 'api/purchases_from_customer/'+id, headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}
		}).
		then(function(response){
			$scope.success="Data deleted!";
			$scope.loadData();
		}, function(response){
			$scope.alert=response.data.message;
		});
	}

	$scope.showAlert=function(){
			return $scope.alert!="" && $scope.alert!="success";
	}
});
