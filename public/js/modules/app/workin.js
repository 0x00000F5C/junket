main.registerCtrl("workinController",function($scope,$location,$http,$filter){
	var d = new Date();
	var d2 = new Date();

	dateNow=new Date(d2.setHours(d.getHours() - 11));
	$scope.dataShift={};
	$scope.getData=function(){
		$http({
			method: "get",
			url: "api/get_workin?date="+$filter('date')(dateNow, 'yyyy-MM-dd'),
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}
		}).
		then(function(response){
			$scope.dataShift = response.data.data;
		}, function(response){
			$scope.alert=response.data.message;
		});
	}

	$scope.showData=function(){
		return $scope.dataShift.id !== "undefined" && $scope.dataShift.id;
	}

	$scope.workin=function(id){
		$http({
			method: "put",
			url: 'api/shift_history/'+id+'/accepted',	
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			},
			params: {
				menu_name : "shift_transfer"
			}
		}).
		then(function(response){
			$scope.status = response.data.status;
			$scope.alert = "Shift transfer accepted!";
			$scope.getData();
			$scope.loadData();
			// if($scope.status=="success"){
				// $location.path("app/shift_transfer");
			// }
		}, function(response){
			$scope.alert=response.data.message;
		});
	}
	
    $scope.sortType     = 'date'; // set the default sort type
	$scope.sortReverse  = true;  // set the default sort order
	$scope.searchList   = '';     // set the default search/filter term
	$scope.page			= 1;
	
	$scope.listData = [];
	$scope.loadData = function() {
		if ($scope.sortReverse == true) {
			$scope.sort_method = 'desc';
		} else {
			$scope.sort_method = 'asc';
		}

		if ($scope.date) {
			$scope.date = $filter('date')($scope.date, 'yyyy-MM-dd');
		} else {
			$scope.date = '';
		}

		$http({
			method: "get", 
			url: "api/shift_history/all?menu_name=shift_histories&sort_by="+$scope.sortType+"&sort_type="+$scope.sort_method+"&page="+$scope.page, 
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
		   }
		}).
		then(function(response){
			$scope.listData = response.data;
		}, function(response){
			$scope.alert=response.data.message;
		});
	}

	$scope.sort = function(sortType) {
		$scope.sortType = sortType;
		$scope.sortReverse = !$scope.sortReverse;
		$scope.loadData();
	}

	$scope.showPage=function(en){
		return (en>=$scope.listData.current_page && en<=$scope.listData.current_page+2) || (en<=$scope.listData.current_page && (en>=$scope.listData.current_page-2 || en>=$scope.listData.last_page-2));
	}

	$scope.isLastPage=function(){
		return $scope.listData.current_page==$scope.listData.last_page;
	}

	$scope.paging=function(page){
		$scope.page=page;
		$scope.loadData();
	}
});
