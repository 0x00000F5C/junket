main.registerCtrl("rollingController", function($scope,$http,$routeParams) {
	id=$routeParams.id;
	$scope.master=[];
	$scope.open=[];
	$scope.rolling=0.00;
	$scope.confirmModal=false;
	$scope.success="";
	$scope.alert="";
	$scope.type= ($routeParams.type ? $routeParams.type : 0);
	console.log($scope.type);
	$scope.totalOpen=0;
	$scope.totalOpenid = "";
	$scope.totalOpendate = "";

	$scope.getOpenStatus=function(){
		$http({
			method: "get",
			url: "api/transaction_detail/"+id,
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			},
			params: {
				flags: "1credit"
			}
		}).
		then(function(response){
			$scope.credit = response.data.data;
			sumCredit=0;
			if($scope.credit.length==0){
				sumCredit=$scope.master.nn_chips;

				$http({
					method: "post",
					url: "api/transaction_detail/"+id,
					headers: {
						"Content-Type":"application/json",
						"X-Auth-Token":$scope.token
					},
					data: {
						rolling: $scope.master.nn_chips,
						flags: "1credit",
						time_transaction: ($scope.master.date.split(" "))[1],
						type: 0
					}
				}).
				then(function(response){
					$scope.data=response.data.data;
				},function(response){
					$scope.alert=response.data.message;
				});
			}else{
				for(inde=0;inde<$scope.credit.length;inde++){
					sumCredit+=$scope.credit[inde].rolling;
				}
			}

			$http({
				method: "get",
				url: "api/transaction_detail/"+id,
				headers: {
					"Content-Type":"application/json",
					"X-Auth-Token":$scope.token
				},
				params: {
					flags: "open"
				}
			}).
			then(function(response){
				$scope.open = response.data.data;
				if($scope.open.length==0){

				}else{
					for(inde=0;inde<$scope.open.length;inde++){
						if($scope.open[inde].type == $scope.type){
							$scope.totalOpen += parseFloat($scope.open[inde].rolling);
							$scope.totalOpenid = $scope.open[inde].id;
							$scope.totalOpendate = $scope.open[inde].time_transaction;
							console.log('asa');
						}else{
							$scope.totalOpen = 0;
							$scope.totalOpenid = "";
							$scope.totalOpendate = "";
						}
					}
				}
				$scope.getRollingDetail();
			}, function(response){
				$scope.alert=response.data.message;
			});
		}, function(response){
			$scope.alert=response.data.message;
		});
	}

	$scope.openStatus=function(){
		return $scope.open.length>0;
	}

	$scope.openRolling=function(){
		console.log($scope.master);
		$http({
			method: "post",
			url: "api/transaction_detail/"+id,
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			},
			data: {
				rolling: $scope.master.nn_chips,
				flags: "open",
				time_transaction: (new Date().getHours())+":"+(new Date().getMinutes()),
				type: 1
			}
		}).
		then(function(response){
			$scope.data=response.data.data;
			$scope.confirmModal=false;
			$scope.getOpenStatus();
		},function(response){
			$scope.alert=response.data.message;
		});
	}

	$scope.getRolling=function(){
		$http({
			method: "get",
			url: "api/transaction/"+id,
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}
		}).
		then(function(response){
			$scope.master = response.data.data;
		}, function(response){
			$scope.alert=response.data.message;
		});
	}

	$scope.detail=[];
	$scope.getRollingDetail=function(){
		$http({
			method: "get",
			url: "api/transaction_detail/"+id+"/"+($scope.type?$scope.type:0),
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			},
			params: {
				flags: "role"
			}
		}).
		then(function(response){
			$scope.detail = response.data.data;
			initTotal=0;
			if($scope.totalOpen > 0){
				initTotal = $scope.totalOpen;
			}
			for(i=0;i<$scope.detail.length;i++){
				initTotal+=parseFloat($scope.detail[i].rolling);
				$scope.detail[i].total=initTotal;
			}
		}, function(response){
			$scope.alert=response.data.message;
		});
	}

	$scope.addRolling=function(){
		$scope.loading=true;
		$http({
			method: "post",
			url: "api/transaction_detail/"+id,
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			},
			data: {
				rolling: $scope.rolling,
				flags: "role",
				time_transaction: (new Date().getHours())+":"+(new Date().getMinutes()),
				type: ($scope.type?$scope.type:0)
			}
		}).
		then(function(response){
			$scope.data=response.data.data;
			$scope.rolling=0;
			// $("#rolling").focus();
			$scope.getRollingDetail();
			$scope.loading=false;
		},function(response){
			$scope.alert=response.data.message;
			$scope.loading=false;
		});
	}

	$scope.selected={};
	$scope.editForm=function(row){
		$scope.selected = angular.copy(row);
		setTimeout(function(){$("#selected_rolling").focus()},1);
	}

	$scope.editRolling=function(detail_id){
		$scope.loading=true;
		$http({
			method: "put",
			url: "api/transaction_detail/"+detail_id+"/"+id,
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			},
			data: {
				rolling: $scope.selected.rolling,
				flags: $scope.selected.flags,
				time_transaction: $scope.selected.time_transaction,
				type: ($scope.type?$scope.type:0)
			}
		}).
		then(function(response){
			$scope.data=response.data.data;
			$scope.getRollingDetail();
			$scope.selected={};
			$scope.loading=false;
		},function(response){
			$scope.alert=response.data.message;
			$scope.loading=false;
		});
	}

	$scope.editRolling=function(detail_id){
		$scope.loading=true;
		$http({
			method: "put",
			url: "api/transaction_detail/"+detail_id+"/"+id,
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			},
			data: {
				rolling: $scope.selected.rolling,
				flags: $scope.selected.flags,
				time_transaction: $scope.selected.time_transaction,
				type: ($scope.type?$scope.type:0)
			}
		}).
		then(function(response){
			$scope.data=response.data.data;
			$scope.getRollingDetail();
			$scope.selected={};
			$scope.loading=false;
		},function(response){
			$scope.alert=response.data.message;
			$scope.loading=false;
		});
	}

	$scope.editRolling=function(detail_id){
		$scope.loading=true;
		$http({
			method: "put",
			url: "api/transaction_detail/"+detail_id+"/"+id,
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			},
			data: {
				rolling: $scope.selected.rolling,
				flags: $scope.selected.flags,
				time_transaction: $scope.selected.time_transaction,
				type: ($scope.type?$scope.type:0)
			}
		}).
		then(function(response){
			$scope.data=response.data.data;
			$scope.getRollingDetail();
			$scope.selected={};
			$scope.loading=false;
		},function(response){
			$scope.alert=response.data.message;
			$scope.loading=false;
		});
	}

	$scope.updateRolling = function(detail_id){
		$scope.loading = true;
		$http({
			method: "put",
			url: "api/transaction_detail/"+detail_id+"/"+id,
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			},
			data: {
				rolling: $scope.totalOpen,
				flags: 'open',
				time_transaction: $scope.totalOpendate,
				type: ($scope.type && ($scope.type == 1) ? 0:1)
			}
		}).
		then(function(response){
			$scope.data=response.data.data;
			$scope.getOpenStatus();
			$scope.getRollingDetail();
			$scope.selected={};
			$scope.loading=false;
		},function(response){
			$scope.alert=response.data.message;
			$scope.loading=false;
		});
	}

	$scope.deleteRolling=function(detail_id){
		$http({
			method: "delete",
			url: 'api/transaction/'+detail_id+'/'+id+'/delete',
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}
		}).
		then(function(response){
			$scope.data = response.data.data;
			$scope.getRollingDetail();
			$scope.selected={};
		}, function(response){
			$scope.alert=response.data.message;
		});
	}

	$scope.getTemplate = function (row) {
		if (row.id === $scope.selected.id){
		  return 'edit';
		}else{
			return 'display';
		}
	}
});