main.registerCtrl("vendorController", function($scope, $location, $http, $filter, $log, $routeParams, DTOptionsBuilder, DTColumnDefBuilder){
	$scope.success = "";
	$scope.alert = "";
	$scope.formData = [];
	$scope.date = new Date();

	$scope.add = function() {
		$location.path("app/vendor/create");
	};

	$scope.show = function(id) {
		$location.path("app/vendor/show/" + id);
	};

	$scope.edit = function(id) {
		$location.path("app/vendor/edit/" + id);
	};

	$scope.save = function() {
		if (!$scope.formData.id) {
			$http({
				method: "post",
				url: "api/vendor",
				headers: {
					"Content-Type": "application/json",
					"X-Auth-Token": $scope.token
				},
				params: {
					"name": $scope.formData.name,
					"chip": $scope.formData.chip,
					"commision": $scope.formData.commision
				}
			}).then(function(response){
				$scope.success="Data saved!";
				$location.path("app/vendor");
			}, function(response){
				$scope.alert = response.data.message;
			});
		} else {
			$http({
				method: "put",
				url: "api/vendor/" + $scope.formData.id,
				headers: {
					"Content-Type": "application/json",
					"X-Auth-Token": $scope.token
				},
				params: $scope.formData
			}).then(function(response){
				$scope.success = "Data updated!";
				$location.path("app/vendor");
			}, function(response){
				$scope.alert = response.data.message;
			});
		}
		return false;
	};

	$scope.delete = function(id) {
		$http({
			method: "delete", 
			url: "api/vendor/" + id, 
			headers: {
				"Content-Type": "application/json",
				"X-Auth-Token": $scope.token
			}
		}).then(function(response) {
			if(response.data.status) {
				$scope.success = "Data deleted!";
				$scope.loadData();
			} else {
				$scope.alert = " ";
			}
		}, function(response) {
			$scope.alert = response.data.message;
		});
	};

	$scope.cancel = function() {
		$location.path("app/vendor");
	};

	$scope.showAlert = function(){
		return $scope.alert != "" && $scope.alert != "success";
	};

	$scope.getEdit = function() {
		if(!$routeParams.id){
			return false;
		} else {
			$http({
				method: "get",
				url: "api/vendor/" + $routeParams.id,
				headers: {
					"Content-Type": "application/json",
					"X-Auth-Token": $scope.token
				}
			}).then(function(response) {
				$scope.formData = {
					id: parseInt(response.data.data.id),
					name: response.data.data.name,
					chip: response.data.data.chip,
					commision: parseFloat(response.data.data.commision)
				};
			}, function(response){
				$scope.alert = response.data.message;
			});
		}
	};

	$scope.dtInstance = {};
	$scope.sortType = "name";
	$scope.sortReverse = true;
	$scope.page = 1;
	$scope.dataTable = [];
	$scope.params = [];

	$scope.loadTable=function(){
		$scope.params.page = $scope.page;
		$scope.params.sort_by = $scope.sortType;
		$scope.params.sort_type = $scope.sortReverse ? "desc" : "asc";
		$scope.params.filter = $scope.filter ? $scope.filter : "";

		$http({
			method: "get",
			url: "api/vendor",
			headers: {
				"Content-Type": "application/json",
				"X-Auth-Token": $scope.token
			},
			params: $scope.params
		}).then(function(response){
			$scope.dataTable = response.data;
		}, function(response){
			$scope.alert = response.data.message;
		});
	};

	$scope.showPage = function(en) {
		return (en >= $scope.dataTable.current_page && en <= $scope.dataTable.current_page + 2) || (en <= $scope.dataTable.current_page && (en >= $scope.dataTable.current_page - 2 || en >= $scope.dataTable.last_page - 2));
	};

	$scope.isLastPage = function() {
		return $scope.dataTable.current_page == $scope.dataTable.last_page;
	};

	$scope.paging = function(page) {
		$scope.page = page;
		$scope.loadTable();
	};
});
