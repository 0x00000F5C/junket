main.registerCtrl("redeemCommissionController", function($scope, $location, $http, $filter, $log, $routeParams, DTOptionsBuilder, DTColumnDefBuilder){
	$scope.success = "";
	$scope.alert = "";
	$scope.formData = [];
	$scope.fd={};
	$scope.fd.redeem_date = $filter('date')(new Date(), 'yyyy-MM-dd');

	$scope.add = function() {
		$location.path("app/redeem_commision/create");
	};

	$scope.show = function(id) {
		$location.path("app/redeem_commision/show/" + id);
	};

	$scope.edit = function(id) {
		$location.path("app/redeem_commision/edit/" + id);
	};

	$scope.save = function() {};

	$scope.delete = function(id) {};

	$scope.cancel = function() {
		$location.path("app/redeem_commision");
	};

	$scope.showAlert = function(){
		return $scope.alert != "" && $scope.alert != "success";
	};

	$scope.getEdit = function() {};

	$scope.dtInstance = {};
	$scope.sortType = "transaction_no";
	$scope.sortReverse = true;
	$scope.page = 1;
	$scope.dataTable = [];
	$scope.params = [];

	$scope.loadTable=function(){
		$scope.params.page = $scope.page;
		$scope.params.sort_by = $scope.sortType;
		$scope.params.sort_type = $scope.sortReverse ? "desc" : "asc";
		$scope.params.filter = $scope.filter ? $scope.filter : "";

		$http({
			method: "get",
			url: "api/redeem_commision",
			headers: {
				"Content-Type": "application/json",
				"X-Auth-Token": $scope.token
			},
			params: $scope.params
		}).then(function(response){
			$scope.dataTable = response.data;
		}, function(response){
			$scope.alert = response.data.message;
		});
	};

	$scope.showPage = function(en) {
		return (en >= $scope.dataTable.current_page && en <= $scope.dataTable.current_page + 2) || (en <= $scope.dataTable.current_page && (en >= $scope.dataTable.current_page - 2 || en >= $scope.dataTable.last_page - 2));
	};

	$scope.isLastPage = function() {
		return $scope.dataTable.current_page == $scope.dataTable.last_page;
	};

	$scope.paging = function(page) {
		$scope.page = page;
		$scope.loadTable();
	};
	
	$scope.onredeem=0;
	
	$scope.accountOption=[];
	$scope.getAccount=function(){
		$http({
			method: "get",
			url: "api/account",
			headers: {
				"Content-Type": "application/json",
				"X-Auth-Token": $scope.token
			},
			params: $scope.params
		}).then(function(response){
			$scope.accountOption = response.data.data;
		}, function(response){
			$scope.alert = response.data.message;
		});
	}
	
	$scope.onredeeming=function(id){
		$scope.onredeem=parseInt(id);
	}
	
	$scope.getTemplate=function(id){
		console.log($scope.onredeem+" "+id);
		return $scope.onredeem!=parseInt(id)?'display':'edit';
	}
	
	$scope.redeem=function(){
		$scope.fd.redeem_date = $filter('date')($scope.fd.redeem_date, 'yyyy-MM-dd');
		$http({
			method: "post",
			url: "api/redeem_commision/closing/"+$scope.onredeem,
			headers: {
				"Content-Type": "application/json",
				"X-Auth-Token": $scope.token
			},
			params: $scope.fd
		}).then(function(response){
			$scope.success = "Commission Redeemed!";
			$scope.onredeem=0;
			$scope.fd.account_id=null;
			$scope.loadTable();
		}, function(response){
			$scope.alert = response.data.message;
			$scope.onredeem=0;
			$scope.fd.account_id=null;
			$scope.loadTable();
		});
	}
});
