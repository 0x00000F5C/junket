angular.module("main").controller('testController', function ($scope,$http,$timeout, $q, $log) {
	$scope.title="Test";
	$scope.queryOptions = {
        query: function (query) {
            var data = {
                results: [
                    { id: "1", text: "A" },
                    { id: "2", text: "B" }
                ]
            };

            query.callback(data);
        }
    };
	$scope.array=[];
	$scope.customerList=[];
	$scope.getCustomer=function(){
		$http({
			method: "get",
			url: "api/customer/get_lists",
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}
		}).
		then(function(response){
			$scope.customerList = response.data.data;
			$scope.array=[];
			for(inde=0;inde<$scope.customerList.length;inde++){
				$scope.array.push({display:$scope.customerList[inde].first_name+" "+$scope.customerList[inde].last_name, label:$scope.customerList[inde].first_name+" "+$scope.customerList[inde].last_name, value:$scope.customerList[inde].id});
			}
		}, function(response){
			$scope.alert=response.data.message;
		});
	}
	$scope.labelDeviceAutoComplete = {
		source: function (request, response) {
			
			// array.push({ label: 'sss', value: 1 });

			response($scope.array);
		},
		minLength: 1,
		select: function (event, ui) {
			this.value = ui.item.label;

			return false;
		}
	};

    // list of `state` value/display objects
    $scope.querySearch   = querySearch;
    $scope.selectedItemChange = selectedItemChange;
    $scope.searchTextChange   = searchTextChange;

    $scope.newState = newState;

    function newState(state) {
      alert("Sorry! You'll need to create a Constitution for " + state + " first!");
    }

    // ******************************
    // Internal methods
    // ******************************

    /**
     * Search for states... use $timeout to simulate
     * remote dataservice call.
     */
    function querySearch (query) {
      var results = query ? $scope.array.filter( createFilterFor(query) ) : $scope.array,
      // var results = query ? $scope.states.filter( createFilterFor(query) ) : $scope.states,
          deferred;
        return results;
    }

    function searchTextChange(text) {
      $log.info('Text changed to ' + text);
    }

    function selectedItemChange(item) {
	  
      $log.info('Item changed to ' + JSON.stringify(item));
    }
	
    /**
     * Create filter function for a query string
     */
    function createFilterFor(query) {
      var lowercaseQuery = angular.lowercase(query);

      return function filterFn(state) {
        // return (state.value.indexOf(lowercaseQuery) === 0);
        return (angular.lowercase(state.display).indexOf(lowercaseQuery) === 0);
      };

    }
});
