main.registerCtrl("rolesController",function($scope,$location,$http,DTOptionsBuilder,DTColumnDefBuilder){
    $scope.tab = 1;

    $scope.setTab = function(newTab){
      $scope.tab = newTab;
    };

    $scope.isSet = function(tabNum){
      return $scope.tab === tabNum;
    };

	$scope.dtOptions = DTOptionsBuilder.newOptions().withOption('order', [0, 'asc']);
	$scope.dtColumnDefs = [
        DTColumnDefBuilder.newColumnDef(0),
        DTColumnDefBuilder.newColumnDef(1),
        DTColumnDefBuilder.newColumnDef(2)
    ];
    $scope.dtInstance = {};

	$scope.role_option = {};

	$roleList=[];
	$scope.getRole=function(){
		$http({
			method: "GET",
			url: "api/role/get_roles",
			headers:{
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}}).
			then(function(response){
				$scope.roleList=response.data.data;
				$scope.role_option=response.data.data[0];
				$scope.getMenuRole();
			}, function(response){
				$scope.alert=response.data.message;
		});
	}

	$scope.menuList=[];
	$scope.getMenu=function(){
		$scope.loading=true;
		$http({
			method: "GET",
			url: "api/role/show_menus",
			headers:{
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}}).
			then(function(response){
				$scope.menuList=response.data.menus;
				$scope.loading=false;
			}, function(response){
				$scope.alert=response.data.message;
				$scope.loading=false;
		});
	}

	$scope.menuRoleList=[];
	$scope.getMenuRole=function(){
		$scope.loading=true;
		$http({
			method: "GET",
			url: "api/role/"+$scope.role_option.id+"/get_role",
			headers:{
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}}).
			then(function(response){
				$scope.menuRoleList=response.data.permissions;
				$scope.menuList=activeMenuRole($scope.menuList,$scope.menuRoleList);
				console.log($scope.menuList);
				$scope.loading=false;
			}, function(response){
				$scope.alert=response.data.message;
				$scope.loading=false;
		});
	}

	activeMenuRole=function(menu,menuRole){
		for(key in menu){
			menu2=menu[key].menu;
			for(key2 in menu[key].menu){
				actions=menu[key].menu[key2].actions;
				menu[key].menu[key2].access={};
				if(typeof menu2[key2].actions === "undefined"){
					menu2[key2].actions=[];
				}
				for(inde=0;inde<menu2[key2].actions.length;inde++){
					actionMenu=actions[inde]+" "+key2;
					actionDetail=actions[inde];

					if(typeof menu[key].menu[key2].access["read"] === "undefined"){
						menu[key].menu[key2].access["read"]=false;
					}

					if(typeof menu[key].menu[key2].access[actionDetail] === "undefined"){
						menu[key].menu[key2].access[actionDetail]=false;
					}
					for(inde2=0;inde2<menuRole.length;inde2++){
						if("read "+key2==menuRole[inde2]){
							menu[key].menu[key2].access["read"]=true;
						}else{
							if(menu[key].menu[key2].access["read"]!=true){
								menu[key].menu[key2].access["read"]=false;
							}
						}
						if(actionMenu==menuRole[inde2]){
							menu[key].menu[key2].access[actionDetail]=true;
						}else{
							if(menu[key].menu[key2].access[actionDetail]!=true){
								menu[key].menu[key2].access[actionDetail]=false;
							}
						}
					}
				}
			}
		}
		return menu;
	}

	$scope.loading=false;
	$scope.checkRole=function(keyInput,keyChildInput,actionInput,event){
		$scope.loading=true;
		action2=actionInput+" "+keyChildInput;
		elem=$(event.target);
		if(elem.is(":checked")){
			console.log("check");
			$scope.menuRoleList.push(action2);
			$scope.menuList[keyInput].menu[keyChildInput].access[actionInput]=true;
			if(actionInput=="read"){
				for(key in $scope.menuList[keyInput].menu[keyChildInput].access){
					$scope.menuRoleList.push(key+" "+keyChildInput);
					$scope.menuList[keyInput].menu[keyChildInput].access[key]=true;
				}
			}
		}else{
			console.log("uncheck");
			loop1: for(inde3=0;inde3<$scope.menuRoleList.length;inde3++){
				splitRoleList=$scope.menuRoleList[inde3].split(" ");
				if(actionInput=="read" && splitRoleList[1]==keyChildInput){
					$scope.menuRoleList.splice(inde3,1);
					inde3--;
					$scope.menuList[keyInput].menu[keyChildInput].access[splitRoleList[0]]=false;
				}else if($scope.menuRoleList[inde3]==action2){
					$scope.menuRoleList.splice(inde3,1);
					$scope.menuList[keyInput].menu[keyChildInput].access[actionInput]=false;
					break loop1;
				}
			}
		}
		$scope.save();
	}

	bundlingFormData=function(dataSource){
		bundleData=[];
		for(inde4=0;inde4<dataSource.length;inde4++){
			splitData=(dataSource[inde4].split(" "));
			if(typeof bundleData[splitData[1]] === "undefined"){
				bundleData[splitData[1]]=[];
			}
			if(splitData[0]!=="read"){
				bundleData[splitData[1]].push(splitData[0]);
			}
		}

		bundleFormData=$scope.role_option;
		bundleFormData.permissions=[];
		for(keyBundle in bundleData){
			bundleRow={name:keyBundle,action:bundleData[keyBundle]};
			bundleFormData.permissions.push(bundleRow);
		}
		return bundleFormData;
	}

	$scope.save=function(){
		$scope.fd=bundlingFormData($scope.menuRoleList);
		// console.log($scope.fd);console.log(JSON.stringify($scope.fd));return false;
		$http({
			method: "PUT",
			url: "api/role/update",
			data: $scope.fd,
			headers:{
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}}).
			then(function(response){
			  $scope.status = response.status;
			  $scope.data = response.data;
			  $scope.loading=false;
			}, function(response){
			  $scope.alert=response.data.message;
			  $scope.loading=false;
		});
	}
});