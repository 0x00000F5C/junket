main.registerCtrl("userActivitiesController",function($scope,$http){
	$scope.sortType     = 'created_at'; // set the default sort type
	$scope.sortReverse  = true;  // set the default sort order
	$scope.searchList   = '';     // set the default search/filter term
	$scope.page			= 1;

	$scope.userActivitiesList=[];
	$scope.loadTable=function(){
		$http({
			method: "GET",
			url: "api/user_activities",
			headers:{
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			},
			params:{
				page: $scope.page
			}}).
			then(function(response){
				$scope.userActivitiesList=response.data;
			}, function(response){
				$scope.alert=response.data.message;
		});
	}

	$scope.showPage=function(en){
		return (en>=$scope.userActivitiesList.current_page && en<=$scope.userActivitiesList.current_page+2) || (en<=$scope.userActivitiesList.current_page && (en>=$scope.userActivitiesList.current_page-2 || en>=$scope.userActivitiesList.last_page-2));
	}

	$scope.isLastPage=function(){
		return $scope.userActivitiesList.current_page==$scope.userActivitiesList.last_page;
	}

	$scope.paging=function(page){
		$scope.page=page;
		$scope.loadTable();
	};
});