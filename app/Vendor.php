<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Vendor extends Model
{
	use SoftDeletes;
    protected $fillable = [
		"name",
		"chip",
		"commision"
	];

    protected $hidden = ['created_at', 'updated_at'];

    public function purchases() {
        return $this->hasMany(Purchase::class);
    }
	
	public function genting2Purchases() {
		return $this->hasMany(Purchase::class)->where("vendor_id", Vendor::whereRaw("`name` REGEXP '^Genting ?2$'")->first()->id);
	}
}
