<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Expense extends Model
{
    use SoftDeletes;
    
    protected $fillable = [
        "expense_no",
        "user_id",
        "bank_id",
        "manager_id",
        "date",
        "annotation",
        "amount",
		"transaction_id",
		"shift",
        "image_name",
        "image_size",
        "image_ext",
        "image_path"
    ];

    protected $hidden = ["created_at", "updated_at", "deleted_at"];

    protected $with = ["user","bank","manager","transaction"];

    public function user(){
        return $this->belongsTo(User::class)->withTrashed();
    }

    public function manager(){
        return $this->belongsTo(User::class,'manager_id')->withTrashed();
    }
	
    public function bank(){
        return $this->belongsTo(Bank::class)->withTrashed();
    }
	
	public function transaction(){
		return $this->belongsTo(Transaction::class)->withTrashed();
	}
}
