<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class JournalAccount extends Model
{
    use SoftDeletes,Sortable;

    protected $fillable = [
        'date',
        'user_id',
        'entity',
        'entity_id',
        'activity',
        'activity_id',
        'remark',
        'je_num',
        'ref_no',
		'company_id'
    ];

    protected $hidden = ['created_at', 'updated_at','deleted_at'];

    protected $dates = ['created_at','updated_at','deleted_at'];
	
	protected $with = ['detail','user'];
	
	public function detail(){
		return $this->hasMany(JournalAccountDetail::class,'journal_id');
	}
	
	public function user(){
		return $this->belongsTo(User::class);
	}
	
    protected $sortable = [
        'date',
        'user_id',
        'entity',
        'entity_id',
        'activity',
        'activity_id',
        'remark',
        'je_num',
        'ref_no'
    ];

}
