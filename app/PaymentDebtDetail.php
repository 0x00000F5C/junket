<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class PaymentDebtDetail extends Model
{
    protected $fillable = [
        "payment_debt_id", "customer_debt_id", "amount"
    ];

    protected $hidden = [];
	
	public $timestamps = false;

    protected $with = ["customer_debt"];

    public function payment_debt(){
        return $this->belongsTo(PaymentDebt::class);
    }
	
    public function customer_debt(){
        return $this->belongsTo(CustomerDebt::class);
    }
	
	public static function get_debt($id){
		return DB::table(DB::raw('(select * from (
			select cd.id as customer_debt_id,cd.date,cd.amount_due as amount_original,
				cd.amount_due-sum(ifnull(pdd.amount,0))as amount_due, 0 as amount 
			from customer_debts cd
			left join payment_debt_details pdd on pdd.customer_debt_id=cd.id
			left join payment_debts pd on pd.id=pdd.payment_debt_id 
			where cd.deleted_at is null and cd.customer_id='.$id.' group by cd.id)as sub where amount_due<>0)as sub order by date,customer_debt_id'))->get();
	}
	
	public static function get_edit($id){
		return DB::table(DB::raw('(select pdd.customer_debt_id,cd.date,cd.amount_due as amount_original,
				cd.amount_due-sum(ifnull(pdd2.amount,0))as amount_due, ifnull(pdd.amount,0) as amount 
			from payment_debts pd
			join payment_debt_details pdd on pdd.payment_debt_id=pd.id
			join customer_debts cd on cd.id=pdd.customer_debt_id
			left join payment_debt_details pdd2 on pdd2.customer_debt_id=cd.id and pdd2.id!=pdd.id
			where pd.id='.$id.' group by cd.id)as sub'))->get();
	}
}
