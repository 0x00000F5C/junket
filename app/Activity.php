<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    protected $fillable = [
        "user_id",
        "title",
        "start",
        "end",
        "place",
        "notes"
    ];

    protected $hidden = ["created_at", "updated_at"];

    public function user(){
        return $this->belongsTo(User::class)->withTrashed();
    }
}
