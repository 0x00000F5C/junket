<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PurchasesFromCustomer extends Model
{
    use SoftDeletes;

    protected $fillable =[
        "date",
        "user_id",
        "customer_id",
		"account_id",
		"type_chip",
        "commission_percent",
        "commission",
        "value"
    ];

    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];

    protected $with = ['user','customer','account'];

    public function user(){
        return $this->belongsTo(User::class)->withTrashed();
    }
	
    public function customer(){
        return $this->belongsTo(Customer::class)->withTrashed();
    }
	
    public function account(){
        return $this->belongsTo(Account::class)->withTrashed();
    }

}
