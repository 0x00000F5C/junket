<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Route::auth();

Route::group(['prefix' => 'api', 'middleware' => 'cors'], function (){
    Route::post('locked', 'API\LockedVisitorController@store');
    Route::get('/locked', 'API\LockedVisitorController@index');
    Route::get('/locked/{id}', 'API\LockedVisitorController@show');
    Route::post('/locked/clear', 'API\LockedVisitorController@clear');
    Route::delete('/locked/{id}', 'API\LockedVisitorController@destroy');

    Route::group(['prefix' => 'users'], function (){
        Route::post('/login', 'API\AuthController@user_login');
        Route::post('/logout', 'API\AuthController@user_logout');

        Route::group(['middleware' => 'api.auth'], function () {
            Route::get('/checkPermission', 'API\UserController@checkPermission');
            Route::get('/get_users', 'API\UserController@lists');
            Route::get('{user}/get_user', 'API\UserController@show');
            Route::get('{role}/find_by_role', 'API\UserController@find_by_role');
            Route::get('/employee', 'API\UserController@employee_role');
            Route::post('/save', 'API\UserController@store');
            Route::put('/update', 'API\UserController@update');
            Route::delete('{user}/delete', 'API\UserController@destroy');
            Route::put('/reset_password', 'API\UserController@resetPassword');
            Route::put('/change_password/{user}', 'API\UserController@changePassword');
			
			Route::post('/import','API\UserController@import');
        });
    });

    Route::group(['prefix' => 'role'], function (){
        Route::get('/show_menus', 'API\RoleController@show_menus');

        Route::group(['middleware' => 'api.auth'], function () {
            Route::get('/get_roles', 'API\RoleController@lists');
            Route::get('/{role}/get_role','API\RoleController@show');
            Route::post('/save', 'API\RoleController@store');
            Route::put('/update', 'API\RoleController@update');
            Route::delete('/{role}/delete','API\RoleController@destroy');
        });
    });

    Route::group(['prefix' => 'customer'], function (){
        Route::get('/get_customer_types', 'API\CustomerTypeController@lists');
        Route::get('/customers_has_debt', 'API\CustomerController@getCustomerHasDebt');

        Route::group(['middleware' => 'api.auth'], function () {
            Route::get('/type/{customer_type}/get_type', 'API\CustomerTypeController@show');
            Route::post('/type/save', 'API\CustomerTypeController@store');
            Route::put('/type/update', 'API\CustomerTypeController@update');
            Route::delete('/type/{customer_type}/delete', 'API\CustomerTypeController@destroy');

            Route::get('/all', 'API\CustomerController@index');
            Route::get('/get_lists', 'API\CustomerController@lists');
            Route::get('/{customer}/get', 'API\CustomerController@show');
            Route::post('/save', 'API\CustomerController@store');
            Route::put('/update', 'API\CustomerController@update');
            Route::delete('{customer}/delete', 'API\CustomerController@destroy');
        });
    });

    Route::group(['prefix' => 'shift_history'], function (){
        Route::group(['middleware' => 'api.auth'], function () {
            Route::get('/all', 'API\HistoryShiftController@index');
            Route::get('/current', 'API\HistoryShiftController@currentChip');
            Route::get('/{history}/get', 'API\HistoryShiftController@show');
            Route::post('/save', 'API\HistoryShiftController@store');
            Route::put('/{history}/update', 'API\HistoryShiftController@update');
            Route::delete('{history}/delete', 'API\HistoryShiftController@destroy');
            Route::get('{user}/from_user','API\HistoryShiftController@showFromUser');
            Route::get('{user}/to_user','API\HistoryShiftController@showToUser');
            Route::put('/{history}/accepted', 'API\HistoryShiftController@accepted');
        });
    });


    Route::group(['middleware' => 'api.auth'], function (){
        Route::get('/user_activities', 'API\UserActivityController@index');

        Route::get('/transactions', 'API\TransactionController@index');
        Route::get('/transaction/{transaction}', 'API\TransactionController@show');
        Route::post('/transaction', 'API\TransactionController@store');
        Route::put('/transaction/{transaction}', 'API\TransactionController@update');
        Route::delete('/transaction/{transaction}/delete', 'API\TransactionController@destroy');
        Route::post('/open_trip/{transaction}', 'API\TransactionController@openTrip');
        Route::put('/closing_trip/{transaction}', 'API\TransactionController@closingTrip');
        Route::put('/check_in_commision/{transaction}', 'API\TransactionController@checkInCommision');
        Route::put('/transaction_assign/{transaction}', 'API\TransactionController@assigning');
        Route::put('/update_rolling2/', 'API\TransactionController@updateRolling2');

        Route::get('/transaction_detail/{transaction}/{type?}', 'API\TransactionController@showDetailByTransactionId');
        Route::get('/get_detail/{detail}', 'API\TransactionController@showDetailById');
        Route::post('/transaction_detail/{transaction}', 'API\TransactionController@storeDetail');
        Route::put('/transaction_detail/{detail}/{transaction}', 'API\TransactionController@updateDetail');
        Route::delete('/transaction/{detail}/{transaction}/delete', 'API\TransactionController@destroyDetail');

        Route::get('/transaction_current', 'API\TransactionController@currentTrip');
		
        Route::group(['prefix' => 'rollings'], function (){
            Route::post('/{transaction}', 'API\TransactionController@storeRolling');
            Route::get('/{rolling}', 'API\TransactionController@showRollingById');
            Route::put('/{rolling}/{transaction}', 'API\TransactionController@updateRolling');
            Route::delete('/{rolling}/{transaction}', 'API\TransactionController@destroyRolling');
            Route::get('/by_transaction/{transaction}', 'API\TransactionController@showRollingByTransactionId');
        });

        // Route::resource('/rolling_tables', 'API\RollingTableController', ["except" => ["create", "edit"]]);
        Route::get('/table_rolling/{transaction}', 'API\RollingTableController@index');
        Route::post('/table_rolling/{transaction}', 'API\RollingTableController@store');
        Route::post('/table_rolling/{transaction}', 'API\RollingTableController@show');
        Route::put('/table_rolling/{transaction}', 'API\RollingTableController@update');
        Route::delete('/table_rolling/{transaction}', 'API\RollingTableController@destroy');
        // --
        Route::get('/vendors', 'API\VendorController@lists');
        Route::get('/vendor/{vendor}', 'API\VendorController@show');
        Route::resource('/vendor', 'API\VendorController', ["except" => ["create", "edit", "show"]]);

        Route::get('/pay_methods', 'API\PaymentMethodController@lists');
        Route::get('/pay_method/{method}', 'API\PaymentMethodController@show');

        Route::resource('purchases', 'API\PurchaseController', ["except" => ["create", "edit"]]);
		Route::get('claim_commission/master','API\ClaimCommissionController@listHead');
		Route::get('claim_commission/detail','API\ClaimCommissionController@listDetail');
		Route::get('claim_commission/all','API\ClaimCommissionController@listAll');
        Route::resource('claim_commission', 'API\ClaimCommissionController', ["except" => ["create", "edit"]]);
        Route::resource('purchases_from_customer', 'API\PurchasesFromCustomerController', ["except" => ["create", "edit"]]);
        Route::resource('return_chip', 'API\ReturnChipController', ["except" => ["create", "edit"]]);
        Route::get('companies/info', 'API\CompanyController@info');
        Route::resource('companies', 'API\CompanyController', ["except" => ["create", "edit"]]);
        Route::resource('activities', 'API\ActivityController', ["except" => ["create", "edit"]]);
        Route::post('expenses/{id}', 'API\ExpenseController@update');
        Route::get('expenses_transaction', 'API\ExpenseController@getByTransaction');
        Route::resource('expenses', 'API\ExpenseController', ["except" => ["update","create", "edit"]]);
        Route::resource('trans_bank', 'API\TransactionBankController', ["except" => ["create", "edit"]]);
        Route::resource('sell_chips', 'API\SellChipController', ["only" => ["index"]]);

        Route::get('sell_chips/{date}/{from_user}/{shift}', 'API\SellChipController@getByGroup');

        Route::get('get_workin', 'API\HistoryShiftController@workIn');
        Route::get('get_shift_transfer', 'API\HistoryShiftController@shiftTransfer');

        Route::resource('banks', 'API\BankController', ["except" => ["create", "edit"]]);
        Route::resource('customer_debts', 'API\CustomerDebtController', ["except" => ["create", "edit"]]);
        Route::resource('tips', 'API\TipsManagementController', ["except" => ["create", "edit"]]);
        Route::get('checkTips', 'API\TipsManagementController@checkTips');
        Route::resource('wg2', 'API\WithdrawalGenting2Controller', ["except" => ["create", "edit"]]);

		Route::get('payment_debts/number','API\PaymentDebtController@number');
		Route::get('payment_debts/debt_source','API\PaymentDebtController@getSources');
		Route::get('payment_debts/{id}/outstanding_debts','API\PaymentDebtController@getDebts');
        Route::resource('payment_debts', 'API\PaymentDebtController', ["except" => ["create", "edit"]]);
		Route::get('payment_debt_details/{master}','API\PaymentDebtController@showDetail');
		Route::post('payment_debt_details/{id}','API\PaymentDebtController@storeDetail');
		Route::put('payment_debt_details/{id}','API\PaymentDebtController@updateDetail');
		Route::delete('payment_debt_details/{id}','API\PaymentDebtController@destroyDetail');
		Route::delete('payment_debt_details/{master}/delete','API\PaymentDebtController@destroyDetailPerMaster');

        Route::resource('open_balances', 'API\OpenBalanceController', ["except" => ["create", "edit"]]);
		
		Route::get('agent/all','API\AgentController@all');
		Route::resource('agent','API\AgentController', ["except" => ["create", "edit"]]);
		
		Route::get('agent_company/all','API\AgentCompanyController@all');
		Route::resource('agent_company','API\AgentCompanyController', ["except" => ["create", "edit"]]);

        Route::resource('agent_share', 'API\AgentShareController', ["except" => ["create", "edit"]]);
        Route::resource('boss_share', 'API\BossShareController', ["except" => ["create", "edit"]]);
		
        Route::resource('open_balance_agent', 'API\OpenBalanceAgentController', ["except" => ["create", "edit"]]);
        Route::resource('open_balance_boss', 'API\OpenBalanceBossController', ["except" => ["create", "edit"]]);
		
		Route::get('commission_genting2/all','API\GentingtwoCommissionController@all');
		Route::resource('commission_genting2', 'API\GentingtwoCommissionController', ["except" => ["create", "edit"]]);
		
		Route::resource('deposit_genting2', 'API\GentingtwoDepositController', ["except" => ["create", "edit"]]);
		
		Route::get('account/all','API\AccountController@all');
		Route::get('account/type','API\AccountController@accountType');
		Route::get('account/parent','API\AccountController@parenting');
		Route::get('account/child/{parentid}','API\AccountController@children');
		Route::get('account/bank','API\AccountController@bank');
		Route::get('account/not_bank','API\AccountController@notBank');
		Route::get('account/number','API\AccountController@number');
		Route::get('account/genting','API\AccountController@getGentingTwo');
		Route::get('account/genting_balance','API\AccountController@getGentingTwoBalance');
        Route::resource('account', 'API\AccountController', ["except" => ["create", "edit"]]);
		
		Route::get('journal/number','API\JournalAccountController@number');
        Route::resource('journal', 'API\JournalAccountController', ["except" => ["create", "edit"]]);
		Route::post('journal_detail/{id}','API\JournalAccountController@storeDetail');
		Route::get('journal_detail_mst/{id}','API\JournalAccountController@showDetailByMaster');
		Route::get('journal_detail/{id}','API\JournalAccountController@showDetail');
		Route::put('journal_detail/{id}','API\JournalAccountController@updateDetail');
		Route::delete('journal_detail_mst/{id}','API\JournalAccountController@destroyDetailByMaster');
		Route::delete('journal_detail/{id}','API\JournalAccountController@destroyDetail');
		
        // Route::resource('bank_genting2', 'API\GentingtwoBankController', ["except" => ["create", "edit"]]);
		
		Route::get('balance_genting2','API\ReportController@balanceGenting2');
		
        Route::resource('account_config', 'API\AccountConfigController', ["except" => ["create", "edit"]]);
		
        Route::resource('transaction_expense', 'API\TransactionExpenseController', ["except" => ["create", "edit"]]);
		
        Route::resource('user_has_accounts', 'API\UserHasAccountController', ["except" => ["create", "edit"]]);
		
		Route::post("redeem_commision/closing/{id}","API\RedeemCommisionController@closing");
		Route::resource("redeem_commision", "API\RedeemCommisionController", ["except" => ["create", "edit"]]);
    });
	
	//report
	Route::get('asset_on_hand','API\ReportController@assetOnHand');
	Route::get('manager_balance','API\ReportController@managerBalance');
});

Route::group(['prefix' => 'report'], function (){
	Route::get('/rolling/{id}','API\ReportController@showRollingPerTransaction');
	Route::get('/rolling2/{id}','API\ReportController@showRolling2PerTransaction');
	Route::get('/trip/{id}','API\ReportController@showCollectionPerTransaction');
	Route::get('/open_trip/{id}','API\ReportController@showRunnerPerShift');
	Route::get('/get_collection','API\ReportController@showCollectionPerCustomer');
	Route::get('/collection/{id}','API\ReportController@reportCollectionPerCustomer');
	Route::get('/final','API\ReportController@printFinalReport');
	Route::get('/final_show','API\ReportController@showFinalReport');
	Route::get('/get_daily_manager','API\ReportController@showDailyManager');
	Route::get('/daily_manager','API\ReportController@reportDailyManager');
	Route::get('/dashboard_balance', 'API\ReportController@dashboardBalance');
	Route::get('/dashboard_daily', 'API\ReportController@dashboardDaily');
	Route::get('/dashboard_weekly', 'API\ReportController@dashboardWeekly');
	Route::get('/dashboard_monthly', 'API\ReportController@dashboardMonthly');
	Route::get('/get_bank_statement','API\ReportController@showBankStatement');
	Route::get('/bank_statement','API\ReportController@reportBankStatement');
	Route::get('/dashboard_balance_report', 'API\ReportController@dashboardBalanceReport');
	Route::get('/daily','API\ReportController@dailyReport');
	Route::get('/final_customer_show','API\ReportController@finalCustomerShow');
	Route::get('/final_customer_pdf','API\ReportController@finalCustomerPdf');
	Route::get('/rolling_show','API\ReportController@rollingShow');
	Route::get('/rolling_pdf','API\ReportController@rollingPdf');
	Route::get('/rolling2_show','API\ReportController@rolling2Show');
	Route::get('/rolling2_pdf','API\ReportController@rolling2Pdf');
	Route::get('/rolling3_show','API\ReportController@rolling3Show');
	Route::get('/rolling3_pdf','API\ReportController@rolling3Pdf');
	Route::get('/balance_account_show','API\ReportController@balanceAccountShow');
	Route::get('/balance_account_export','API\ReportController@balanceAccountExport');
	Route::get('/journal_show','API\ReportController@journalShow');
	Route::get('/runner_show','API\ReportController@runnerShow');
	Route::get('/runner_pdf','API\ReportController@runnerPdf');
	Route::get('/agent_insurance_show','API\ReportController@agentInsuranceShow');
	Route::get('/agent_insurance_pdf','API\ReportController@agentInsurancePdf');
	Route::get('/purchase_commission_show','API\ReportController@purchaseCommissionShow');
	Route::get('/purchase_commission_pdf','API\ReportController@purchaseCommissionPdf');
});

Route::group(['prefix'=>'sync'],function(){
	Route::get('/user/pull','Sync\UserController@pull');
});

Route::get("/test", function() {
	dd(DB::table("accounts")->where("id", 1)->get());
});