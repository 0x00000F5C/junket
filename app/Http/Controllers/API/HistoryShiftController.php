<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\ShiftHistory;
use Validator;
use App\UserActivity;
use App\JournalAccount;
use App\JournalAccountDetail;
use App\AccountConfig;
use App\UserHasAccount;
use DB;

class HistoryShiftController extends Controller
{
    /**
     * Index Shift Histories
     * GET /api/shift_history/all?menu_name=date=&sort_by=&sort_type=
     * Available sort = date, from_user, shift_from, to_user, shift_to, nn_chips, cash_chips, chas_real
     *
     * @param string $token                 The token for authentication
     * @return Response
     **/
    public function index(Request $request)
    {
        $user = User::where('token',$request->header("X-Auth-Token"))->first();
        $menu = $request->input('menu_name');
        $cek = check_auth($user,'read '.$menu);
        if($cek['result']==1){
            $date = $request->input('date');
            $search = $request->input('search');
            $sort_by = $request->input('sort_by');
            $sort_type = $request->input('sort_type');
			
			switch($sort_by){
				case 'from_user':
					$sort_by='users.username';
					break;
				case 'to_user':
					$sort_by='users2.username';
					break;
			}
			
			$select_base=['shift_histories.*'];
			$select_sell_chip=['shift_histories.id','shift_histories.date',
			 'shift_histories.from_user_id','shift_histories.shift_from',
			  DB::raw('sum(nn_chips)as nn_chips,sum(junket_chips)as junket_chips')];
			
			$histories=ShiftHistory::select($menu!='sell_chip'?$select_base:$select_sell_chip)
			 ->join('users','users.id','=','shift_histories.from_user_id')
			 ->join('users as users2','users2.id','=','shift_histories.to_user_id')
			 ->where('shift_histories.menu_name','=',$menu);
			 
			if($date){
				$histories=$histories->where('date','=',$date);
			}
			
			if($search){
				$histories=$histories->whereRaw('(users.name like "%'.$search.'%"
				 or shift_histories.shift_from like "%'.$search.'%" 
				 or date_format(shift_histories.date,"%d-%m-%Y") like "%'.$search.'%")');
			}
			
			if($menu=='sell_chip'){
				$histories=$histories->groupBy(['shift_histories.date',
				  'shift_histories.from_user_id',
				  'shift_histories.shift_from']);
			}
			
			$histories=$histories->orderBy($sort_by,$sort_type);
			
			if($menu=='shift_transfer' || $menu=='shift_transfer_runner'){
				if($user->roles[0]->id != 1){
					$histories=$histories->where('from_user_id','=',$user->id)->where('status','=',0);
				}
			}else{
				if($user->roles[0]->id != 1){
					$histories=$histories->whereRaw('(shift_histories.from_user_id='.$user->id.' 
					 or shift_histories.to_user_id='.$user->id.')');
				}
			}
			
			$histories=$histories->paginate(10);
        }else{
            return error_unauthorized();
        }

        return response()->json(transformCollection($histories), 200);
    }

    /**
     * Store Shift Histories
     * POST /api/shift_history/save
     *
     * @param string $token                 The token for authentication
     * @param string $menu_name             The name of menu to access
     * @param date $date                    The date of shift history
     * @param integer $from_user_id         The user id
     * @param string $shift_from            The name of shift from
     * @param integer $to_user_id           The user id
     * @param string $shift_to              The name of shift to
     * @param double $nn_chips              The NN Chips amount
     * @param double $cash_chips            The Cash Chips amount
     * @param double $cash_real             The Cash Real amount
     * @return Response
     **/
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'date' => 'required|date',
            'from_user_id' => 'required',
            'shift_from' => 'required',
            'to_user_id' => 'required',
            'shift_to' => 'required',
            'nn_chips' => 'required|numeric',
            'cash_chips' => 'required|numeric',
            'cash_real' => 'required|numeric',
            'menu_name' => 'required'
        ]);

        if($validator->fails()){
            return response()->json(["status"=>"error","message" => implode(' ',$validator->errors()->all())], 422);
        }else{
            $data = $request->all();

            $user = User::where('token',$request->header("X-Auth-Token"))->first();
            $cek = check_auth($user,'create '.$data['menu_name']);
            if($cek['result']==1){
                $log = ShiftHistory::create($data);
				$this->journal($log->id,$request);
                UserActivity::storeActivity(array(
                    "activity" => "add ".$data['menu_name']." for id: ".$log->id,
                    "user" => $user->id,
                    "menu" => $data['menu_name'],
                    "ipaddress" => $request->ip()
                ));
            }else{
                return error_unauthorized();
            }

            if($log){
                return response()->json([
                    "status" => "success",
                    "message" => $data['menu_name']." has been saved!",
                    "data" => $log
                ],200);
            }else{
                return response()->json([
                    "status" => "error",
                    "message" => "Failed save ".$data['menu_name'],
                    "data" => null
                ],403);
            }
        }
    }

    /**
     * Get Shit History by ID
     * GET /api/shift_history/{shift_history_id}/get
     *
     * @param string $token                 The token for authentication
     * @param string $menu_name             The name of menu to access
     * @return Response
     **/
    public function show(Request $request,$id)
    {
//        $user = User::where('token',$request->header("X-Auth-Token"))->first();
//        $cek = check_auth($user,'read '.$request->input('menu_name'));
//        if($cek['result']==1){
            $history = ShiftHistory::find($id);
            if($history!=null){
                return response()->json([
                    "message" => "success",
                    "data" => $history
                ],200);
            }else{
                return response()->json(["message" => "Data not exists!"],404);
            }
//        }else{
//            return error_unauthorized();
//        }
    }

    /**
     * Update Shift History
     * PUT /api/shift_history/{shift_history_id}/update
     *
     * @param string $token                 The token for authentication
     * @param string $menu_name             The name of menu to access
     * @param date $date                    The date of shift history
     * @param integer $from_user_id         The user id
     * @param string $shift_from            The name of shift from
     * @param integer $to_user_id           The user id
     * @param string $shift_to              The name of shift to
     * @param double $nn_chips              The NN Chips amount
     * @param double $cash_chips            The Cash Chips amount
     * @param double $cash_real             The Cash Real amount
     * @return Response
     **/
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'date' => 'required|date',
            'from_user_id' => 'required',
            'shift_from' => 'required',
            'to_user_id' => 'required',
            'shift_to' => 'required',
            'nn_chips' => 'required|numeric',
            'cash_chips' => 'required|numeric',
            'cash_real' => 'required|numeric',
            'menu_name' => 'required'
        ]);

        if($validator->fails()){
            return response()->json(["status"=>"error","message" => implode(' ',$validator->errors()->all())], 422);
        }else{
            $input = $request->all();

            $user = User::where('token',$request->header("X-Auth-Token"))->first();
            $cek = check_auth($user,'update '.$input['menu_name']);
            if($cek['result']==1){
                $history = ShiftHistory::find($id);
                if($history==null){
                    return response()->json(["message" => $input['menu_name']." not exists!"],404);
                }elseif ($history->from_user_id != $user->id && $user->roles[0]->id != 1){
                    return response()->json(["message" => "Not allowed!"],403);
                }else{
                    if ($input['menu_name'] == 'sell_chip' || $input['menu_name'] == 'chip_from_runner_or_assist') {
                        if ($history->status == 1) {
                            return response()->json([
                                "status" => "error",
                                "message" => "Transaction already accepted!"
                            ],403);
                        }
                    }

                    $data = [
                        'date' => $input['date'],
                        'from_user_id' => $input['from_user_id'],
                        'shift_from' => $input['shift_from'],
                        'to_user_id' => $input['to_user_id'],
                        'shift_to' => $input['shift_to'],
                        'nn_chips' => $input['nn_chips'],
                        'junket_chips' => $input['nn_chips'],
                        'cash_chips' => $input['cash_chips'],
                        'cash_real' => $input['cash_real'],
                        'menu_name' => $input['menu_name'],
						'status' => 0
                    ];
                    $history->update($data);
					$this->journal($id,$request);
                    UserActivity::storeActivity(array(
                        "activity" => "update ".$input['menu_name']." for id: ".$id,
                        "user" => $user->id,
                        "menu" => $input['menu_name'],
                        "ipaddress" => $request->ip()
                    ));
                }
            }else{
                return error_unauthorized();
            }

            if($history){
                return response()->json([
                    "status" => "success",
                    "message" => $input['menu_name']." has been updated!",
                    "data" => $history
                ],200);
            }else{
                return response()->json([
                    "status" => "error",
                    "message" => "Failed update ".$input['menu_name'],
                    "data" => null
                ],403);
            }
        }
    }

    /**
     * Delete Shit History
     * DELETE /api/shift_history/{shift_history_id}/delete?menu_name
     *
     * @param string $token                 The token for authentication
     * @param string $menu_name             The name of menu to access
     * @return Response
     **/
    public function destroy(Request $request,$id)
    {
        $history = ShiftHistory::find($id);

        if($history == null){
            return response()->json(["message" => $request->input('menu_name')." not exist"],404);
        }

        $user = User::where('token',$request->header("X-Auth-Token"))->first();
        $cek = check_auth($user,'delete '.$request->input('menu_name'));
        if($cek['result']==1) {
            if (ShiftHistory::destroy($id)) {
				JournalAccount::where('activity','=','transfer_chip')->where('activity_id','=',$id)->delete();
                UserActivity::storeActivity(array(
                    "activity" => "delete ".$request->input('menu_name')." for id: ".$id,
                    "user" => $user->id,
                    "menu" => $request->input('menu_name'),
                    "ipaddress" => $request->ip()
                ));
                return response()->json([
                    "status" => "success",
                    "message" => $request->input('menu_name')."has been deleted!"
                ], 200);
            } else {
                return response()->json([
                    "status" => "error",
                    "message" => "Failed delete ".$request->input('menu_name')
                ], 403);
            }
        }else{
            return error_unauthorized();
        }
    }

    /**
     * Accepted STATUS Shift History
     * PUT /api/shift_history/{shift_history_id}/accepted
     *
     * @param string $token                 The token for authentication
     * @param string $menu_name             The name of menu to access
     * @return Response
     **/
    public function accepted(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'menu_name' => 'required'
        ]);

        if($validator->fails()){
            return response()->json(["status"=>"error","message" => implode(' ',$validator->errors()->all())], 422);
        }

        $user = User::where('token',$request->header("X-Auth-Token"))->first();
        $cek = check_auth($user,'update '.$request->input('menu_name'));
        if($cek['result']==1){
            $history = ShiftHistory::where('id', $id)->first();
            if ($history == null) {
                return response()->json(["message" => "Data not found!"],404);
            } elseif($history->to_user_id != $user->id) {
                return response()->json(["message" => "Not allowed!"],403);
            }

            $history->update(['status' => 1]);
			$this->journal($id,$request);
            UserActivity::storeActivity(array(
                "activity" => "accepted ".$request->input('menu_name')." for id: ".$id,
                "user" => $user->id,
                "menu" => $request->input('menu_name'),
                "ipaddress" => $request->ip()
            ));
        }else{
            return error_unauthorized();
        }

        if($history){
            return response()->json([
                "status" => "success",
                "message" => "Successfully accepted!",
                "data" => $history
            ],200);
        }else{
            return response()->json([
                "status" => "error",
                "message" => "Failed accepted!",
                "data" => null
            ],403);
        }
    }

    /**
     * Journal History Shift.
     *
     * @param  int  $id
     * @param  object  $request from Request Class
     * @return boolean
     */
	public function journal($id,$request){
		$user = User::where('token',$request->header("X-Auth-Token"))->first();
		$data = ShiftHistory::find($id);
		
		DB::beginTransaction();
		
		try{
			$uha=UserHasAccount::where('user_id','=',$data['from_user_id'])->first();
			$account_config=AccountConfig::first();
			$journal=JournalAccount::where('activity','=','transfer_chip')
				->where('activity_id','=',$id)->where('entity_id','=',$data['from_user_id'])->first();
			$number=getAutoNumber('je_num','journal_accounts','JE-'.date('mY')."-",4);
			$data_journal=[
				'je_num'=>$number,
				'date'=>$data['updated_at'],
				'user_id'=>$user['id'],
				'entity'=>'user',
				'entity_id'=>$data['from_user_id'],
				'activity'=>'transfer_chip',
				'activity_id'=>$id,
				'remark'=>'Transfer Chip ('.$data['to_user']['name'].')'
			];
			
			if(count($journal)==0){
				$journal=JournalAccount::create($data_journal);
			}else{
				$data_journal['je_num']=$journal->je_num;
				$journal->update($data_journal);
				JournalAccountDetail::where('journal_id','=',$journal->id)->delete();
			}
			
			if($journal){
				$data['nn_chips']=isset($data['nn_chips'])?$data['nn_chips']:0;
				$data['junket_chips']=isset($data['junket_chips'])?$data['junket_chips']:0;
				$data['cash_chips']=isset($data['cash_chips'])?$data['cash_chips']:0;
				$data['cash_real']=isset($data['cash_real'])?$data['cash_real']:0;
				
				$amount=$data['nn_chips']+$data['junket_chips']+$data['cash_chips']+$data['cash_real'];
				
				JournalAccountDetail::create([
					'journal_id'=>$journal->id,
					'account_id'=>isset($uha['account_id'])?$uha['account_id']:$account_config['runner'],//opening balance asset
					'debit'=>$amount,
					'credit'=>0
				]);
				
				if($data['nn_chips']>0){
					JournalAccountDetail::create([
						'journal_id'=>$journal->id,
						'account_id'=>$account_config['nn_chips'],//nn chip
						'debit'=>0,
						'credit'=>$data['nn_chips'],
						'entity'=>'user',
						'entity_id'=>$data['from_user_id']
					]);
				}
				
				if($data['junket_chips']>0){
					JournalAccountDetail::create([
						'journal_id'=>$journal->id,
						'account_id'=>$account_config['junket_chips'],//junket chip
						'debit'=>0,
						'credit'=>$data['junket_chips'],
						'entity'=>'user',
						'entity_id'=>$data['from_user_id']
					]);
				}
				
				if($data['cash_chips']>0){
					JournalAccountDetail::create([
						'journal_id'=>$journal->id,
						'account_id'=>$account_config['cash_chips'],//cash chip
						'debit'=>0,
						'credit'=>$data['cash_chips'],
						'entity'=>'user',
						'entity_id'=>$data['from_user_id']
					]);
				}
				
				if($data['cash_real']>0){
					JournalAccountDetail::create([
						'journal_id'=>$journal->id,
						'account_id'=>$account_config['cash_real'],//cash real
						'debit'=>0,
						'credit'=>$data['cash_real'],
						'entity'=>'user',
						'entity_id'=>$data['from_user_id']
					]);
				}
			}
			
			$uha=UserHasAccount::where('user_id','=',$data['to_user_id'])->first();
			$journal=JournalAccount::where('activity','=','transfer_chip')
				->where('activity_id','=',$id)->where('entity_id','=',$data['to_user_id'])->first();
			$number=getAutoNumber('je_num','journal_accounts','JE-'.date('mY')."-",4);
			$data_journal['je_num']=$number;
			$data_journal['entity_id']=$data['to_user_id'];
			$data_journal['remark']='Transfer Chip ('.$data['to_user']['name'].')';
			
			if(count($journal)==0){
				$journal=JournalAccount::create($data_journal);
			}else{
				$data_journal['je_num']=$journal->je_num;
				$journal->update($data_journal);
				JournalAccountDetail::where('journal_id','=',$journal->id)->delete();
			}
			
			if($journal){
				$data['nn_chips']=isset($data['nn_chips'])?$data['nn_chips']:0;
				$data['junket_chips']=isset($data['junket_chips'])?$data['junket_chips']:0;
				$data['cash_chips']=isset($data['cash_chips'])?$data['cash_chips']:0;
				$data['cash_real']=isset($data['cash_real'])?$data['cash_real']:0;
				
				$amount=$data['nn_chips']+$data['junket_chips']+$data['cash_chips']+$data['cash_real'];
				
				JournalAccountDetail::create([
					'journal_id'=>$journal->id,
					'account_id'=>isset($uha['account_id'])?$uha['account_id']:$account_config['runner'],//opening balance asset
					'debit'=>0,
					'credit'=>$amount
				]);
				
				if($data['nn_chips']>0){
					JournalAccountDetail::create([
						'journal_id'=>$journal->id,
						'account_id'=>$account_config['nn_chips'],//nn chip
						'debit'=>$data['nn_chips'],
						'credit'=>0,
						'entity'=>'user',
						'entity_id'=>$data['to_user_id']
					]);
				}
				
				if($data['junket_chips']>0){
					JournalAccountDetail::create([
						'journal_id'=>$journal->id,
						'account_id'=>$account_config['junket_chips'],//junket chip
						'debit'=>$data['junket_chips'],
						'credit'=>0,
						'entity'=>'user',
						'entity_id'=>$data['to_user_id']
					]);
				}
				
				if($data['cash_chips']>0){
					JournalAccountDetail::create([
						'journal_id'=>$journal->id,
						'account_id'=>$account_config['cash_chips'],//cash chip
						'debit'=>$data['cash_chips'],
						'credit'=>0,
						'entity'=>'user',
						'entity_id'=>$data['to_user_id']
					]);
				}
				
				if($data['cash_real']>0){
					JournalAccountDetail::create([
						'journal_id'=>$journal->id,
						'account_id'=>$account_config['cash_real'],//cash real
						'debit'=>$data['cash_real'],
						'credit'=>0,
						'entity'=>'user',
						'entity_id'=>$data['to_user_id']
					]);
				}
			}
			
			DB::commit();
		}catch(\Exception $exc){
			// echo $exc;
			DB::rollback();
		}
			
		return true;
	}
	
    public function workIn(Request $request){
        $user = User::where('token',$request->header("X-Auth-Token"))->first();
        $cek = check_auth($user,'read workin');
        if($cek['result']==1){
            $date = $request->input('date');
            $histories = ShiftHistory::where('to_user_id', $user->id)
                ->where('date', $date)
                ->where('menu_name', 'shift_transfer')
                ->where('status', 0)->first();
            if($histories!=null){
                return response()->json([
                    "message" => "success",
                    "data" => $histories
                ],200);
            }else{
                return response()->json(["message" => "Data not found!"],404);
            }
        }else{
            return error_unauthorized();
        }
    }


    public function showFromUser(Request $request, $id)
    {
        $user = User::where('token',$request->header("X-Auth-Token"))->first();
        $cek = check_auth($user,'read shift_histories');
        if($cek['result']==1){
            $histories = ShiftHistory::where('from_user_id', $id)->paginate(10);
            if($histories!=null){
                return response()->json([
                    "message" => "success",
                    "data" => $histories
                ],200);
            }else{
                return response()->json(["message" => "Data not found!"],404);
            }
        }else{
            return error_unauthorized();
        }
    }

    public function showToUser(Request $request, $id)
    {
        $user = User::where('token',$request->header("X-Auth-Token"))->first();
        $cek = check_auth($user,'read shift_histories');
        if($cek['result']==1){
            $histories = ShiftHistory::where('to_user_id', $id)->paginate(10);
            if($histories!=null){
                return response()->json([
                    "message" => "success",
                    "data" => $histories
                ],200);
            }else{
                return response()->json(["message" => "Data not found!"],404);
            }
        }else{
            return error_unauthorized();
        }
    }
	
	public function currentChip(Request $request){
		$user_id=$request->input('user_id');
		$user = User::where('token',$request->header("X-Auth-Token"))->first();
        $cek = check_auth($user,'read '.$request->input('menu_name'));
        if($cek['result']==1){
			if($user->roles[0]->id != 1){
				$user_id=$user->id;
			}
			
			$date=date('Y-m-d');
			$time_now=date('H:i:s');
			$time_new_day='08:00:00';
			
			if($time_now>$time_new_day){
				// echo 'morning';
				$date=date('Y-m-d',strtotime($date));
			}else{
				// echo 'night';
				$date=date('Y-m-d',strtotime($date.' -1 days'));
			}
			
			$data=ShiftHistory::where('to_user_id','=',$user_id)
				->where('menu_name','=',$request->input('menu_name'))
				->where('status','=',1)
				->where('updated_at','>',$date.' '.$time_new_day)
				->sum('nn_chips');
			
			return response()->json([
				"message" => "success",
				"data" => $data
			],200);
		}else{
			return error_unauthorized();
		}
	}
	
	public function shiftTransfer(Request $request){
		$date=$request->input('date');
		$date=date('Y-m-d',strtotime($date));
		$user = User::where('token',$request->header("X-Auth-Token"))->first();
        $cek = check_auth($user,'read shift_histories');
        if($cek['result']!=1){
			return error_unauthorized();
		}
		
		if($user->roles[0]->id==4){
			$data=ShiftHistory::where('from_user_id','=',$user->id)
				->where('date','=',$date)
				->where('status','=',0)
				->first();
		}
		
		return response()->json([
			"message" => "success",
			"data" => $data
		],200);
		
	}
}
