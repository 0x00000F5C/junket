<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Transaction;
use App\TransactionDetail;
use App\TransactionRolling;
use App\Expense;
use App\TipsManagement;
use Validator;
use App\UserActivity;
use App\JournalAccount;
use App\RedeemCommision;
use App\CustomerDebt;
use DB;

class TransactionController extends Controller
{
    /**
     * Index Transaction List
     * GET /api/transactions?date=&sort_by=&sort_type=&status=
     * Available search : date, status[open,close]
     * Available sort : date, transaction_no, customer, user, nn_chips, shift, status, remarks, commission
     *
     * @param string $token     The token for authentication
     * @return Response
     **/
    public function index(Request $request)
    {
        $user = User::where('token',$request->header("X-Auth-Token"))->first();
        $cek = check_auth($user,'read transactions');
        if($cek['result']==1){
            $date = $request->input('date');
            $status = $request->input('status');
            $assign_from = $request->input('assign_from');
            $assign_to = $request->input('assign_to');
            $sort_by = $request->input('sort_by');
            $sort_type = $request->input('sort_type');
			$transactions = Transaction::select(array('transactions.*',DB::raw('(transactions.nn_chips+ifnull(sum(if(transaction_details.flags="credit",transaction_details.rolling,0)),0))as credit'),'tipsmanagement.amount'))
				->join('customers','customers.id','=','transactions.customer_id')
				->join('users','users.id','=','transactions.user_id')
				->join('transaction_details','transaction_details.transaction_id','=','transactions.id','left')
				->join('tipsmanagement','tipsmanagement.trans_id','=','transactions.id','left')
				->groupBy('transactions.id');
				
			if($user->roles[0]->id != 1 && $user->roles[0]->id != 4 && !$assign_from && !$assign_to){
				$transactions=$transactions->where('transactions.user_id','=',$user->id)
					->orWhere('assign_to','=',$user->id);
			}
			
			if($assign_from){
				$transactions=$transactions->whereRaw('((assign_to is null and transactions.user_id='.$assign_from.') or assign_to='.$assign_from.')');
			}
			
			if($assign_to){
				$transactions=$transactions->where('assign_to','=',$assign_to);
			}
			
			if($date){
				$transactions=$transactions->whereDate('date','=',$date);
			}
			
			if($status){
				$transactions=$transactions->where('status','=',$status);
			}
			
			if(!$sort_by){
				$sort_by='transactions.created_at';
			}
			$sort_type=$sort_type?$sort_type:'desc';
			
			switch($sort_by){
				case 'user':
					$sort_by='users.name';
					break;
				case 'customer':
					$sort_by='customers.first_name';
					break;
			}
			
			$transactions=$transactions->orderBy($sort_by,$sort_type);
			
			if(!$assign_from && !$assign_to){
				$transactions=$transactions->paginate(10);
			}else if($assign_from || $assign_to){
				$transactions=$transactions->paginate(100);
			}
        }else{
            return error_unauthorized();
        }
		
		return response()->json(transformCollection($transactions), 200);
    }

    /**
     * Store Transaction
     * POST /api/transaction
     *
     * @param string $token             The token for authentication
     * @param datetime $date            The date time of transaction
     * @param integer $customer_id      The customer id
     * @param integer $user_id          The user id
     * @param float $nn_chips           The NN Chips value
     * @param string $shift             The shift name
     * @param text  $remarks            The remarks
     * @param float $commission         The commission value
     * @return Response
     **/
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'date' => 'required|date',
            'customer_id' => 'required',
            'user_id' => 'required',
            'nn_chips' => 'required|numeric',
            'shift' => 'required',
            'commission' => 'required'
        ]);

        if($validator->fails()){
            return response()->json(["status"=>"error","message" => implode(' ',$validator->errors()->all())], 422);
        }else{
            $data = $request->all();

            $user = User::where('token',$request->header("X-Auth-Token"))->first();
            $cek = check_auth($user,'create transactions');
            if($cek['result']==1){
                $transaction_no = getAutoNumber('transaction_no','transactions','TR-'.date('my')."-",4);
                $data['transaction_no'] = $transaction_no;
                $log = Transaction::create($data);
				TransactionDetail::create(array(
					"transaction_id" => $log->id,
					"rolling" => $data['nn_chips'],
					"flags" => "1credit",
					"time_transaction" => date('H:i:s',strtotime($data['date'])),
					"type" => 0,
					"user_id" => $user->id
				));
				Transaction::journalOpenTrip($log->id,$request);
				Transaction::debtTrip($log->id);
                UserActivity::storeActivity(array(
                    "activity" => "add transactions open trip for id: ".$log->id,
                    "user" =>$user->id,
                    "menu" =>"transactions",
                    "ipaddress" => $request->ip()
                ));
            }else{
                return error_unauthorized();
            }

            if($log){
                return response()->json([
                    "status" => "success",
                    "message" => "Transaction has been saved!",
                    "data" => $log
                ],200);
            }else{
                return response()->json([
                    "status" => "error",
                    "message" => "Failed save transaction!",
                    "data" => null
                ],403);
            }
        }
    }

    /**
     * Display the specified resource.
     * GET /api/transaction/{id}
     *
     * @param string $token     The token for authentication
     * @return Response
     **/
    public function show(Request $request, $id)
    {
        $user = User::where('token',$request->header("X-Auth-Token"))->first();
        $cek = check_auth($user,'read transactions');
        if($cek['result']==1){
            $transaction = Transaction::find($id);
            if($transaction!=null){
                return response()->json([
                    "message" => "success",
                    "data" => $transaction
                ],200);
            }else{
                return response()->json(["message" => "Transaction not exists!"],404);
            }
        }else{
            return error_unauthorized();
        }
    }

    /**
     * Update Transaction
     * PUT /api/transaction/{id}
     *
     * @param string $token             The token for authentication
     * @param datetime $date            The date time of transaction
     * @param integer $customer_id      The customer id
     * @param integer $user_id          The user id
     * @param float $nn_chips           The NN Chips value
     * @param string $shift             The shift name
     * @param text  $remarks            The remarks
     * @param float $commission         The commission value
     * @return Response
     **/
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'date' => 'required|date',
            'customer_id' => 'required',
            'user_id' => 'required',
            'nn_chips' => 'required|numeric',
            'shift' => 'required',
            'commission' => 'required'
        ]);

        if($validator->fails()){
            return response()->json(["status"=>"error","message" => implode(' ',$validator->errors()->all())], 422);
        }else{
            $data = $request->all();
            $data['status'] = 1;
            $user = User::where('token',$request->header("X-Auth-Token"))->first();
            $cek = check_auth($user,'update transactions');
            if($cek['result']==1){
                UserActivity::storeActivity(array(
                    "activity" => "update transaction trip for id: ".$id,
                    "user" => $user->id,
                    "menu" => "transactions",
                    "ipaddress" => $request->ip()
                ));
                $transaction = Transaction::find($id);
				if($user->roles[0]->id!=1 && $user->id!=$transaction->user_id){
					return response()->json(['status'=>'error','message'=>'Not allowed update trip another user!']);
				}
                if($transaction==null){
                    return response()->json(["message" => "Transaction not exists!"],404);
                }else{
                    $transaction->update($data);
					$transaction_detail=TransactionDetail::where('transaction_id','=',$id)
						->where('flags','=','1credit')
						->first();
					
					$transaction_detail->update(array(
						"rolling" => $data['nn_chips'],
						"time_transaction" => date('H:i:s',strtotime($data['date']))
					));
					
					$transaction_detail_credit=TransactionDetail::where('transaction_id','=',$id)
						->whereRaw('(flags="1credit" and flags="credit")')
						->value('rolling');
					
					$transaction_detail_open=TransactionDetail::where('transaction_id','=',$id)
						->where('flags','=','open')
						->first();
						
					$data_open['rolling']=$transaction_detail_credit['rolling'];
					$data_open['time_transaction']=date('H:i:s',strtotime($data['date']));
					if($transaction_detail_open){
						TransactionDetail::find($transaction_detail_open->id)->update($data_open);
					}
					
					Transaction::journalOpenTrip($id,$request);
					Transaction::debtTrip($id);
                }
            }else{
                return error_unauthorized();
            }

            if($transaction){
                return response()->json([
                    "status" => "success",
                    "message" => "Transaction has been updated!",
                    "data" => $transaction
                ],200);
            }else{
                return response()->json([
                    "status" => "error",
                    "message" => "Failed update transaction!",
                    "data" => null
                ],403);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * DELETE /api/transaction/{id}/delete
     *
     * @param string $token     The token for authentication
     * @return Response
     */
    public function destroy(Request $request, $id)
    {
        $transaction = Transaction::find($id);

        if($transaction == null){
            return response()->json(["message"=>"Transaction not exist"],404);
        }

        $user = User::where('token',$request->header("X-Auth-Token"))->first();
        $cek = check_auth($user,'delete transactions');
        if($cek['result']==1) {
            UserActivity::storeActivity(array(
                "activity" => "delete transaction trip for id: ".$id,
                "user" => $user->id,
                "menu" => "transactions",
                "ipaddress" => $request->ip()
            ));
            if (Transaction::destroy($id)) {
				JournalAccount::where('activity','=','open_trip')->where('activity_id','=',$id)->delete();
				JournalAccount::where('activity','=','closing_trip')->where('activity_id','=',$id)->delete();
				JournalAccount::where('activity','=','commission_trip')->where('activity_id','=',$id)->delete();
				
				foreach($transaction['details'] as $datarolling){
					if($datarolling['flags']=='credit' || $datarolling['flags']=='1credit'){
						JournalAccount::where('activity','=','credit_trip')->where('activity_id','=',$datarolling['id'])->delete();
					}
				}
				foreach($transaction['rollings'] as $datarolling2){
					JournalAccount::where('activity','=','rolling2')->where('activity_id','=',$datarolling2['id'])->delete();
				}
				
				$expense=Expense::where('transaction_id',$id)->get();
				foreach($expense as $dataexpense){
					JournalAccount::where('activity','=','expense')->where('activity_id','=',$dataexpense['id'])->delete();
					Expense::destroy($dataexpense['id']);
				}
				
				$tips=TipsManagement::where('trans_id',$id)->get();
				foreach($tips as $datatips){
					JournalAccount::where('activity','=','tips')->where('activity_id','=',$datatips['id'])->delete();
					TipsManagement::destroy($dataexpense['id']);
				}
				
				$redeem=RedeemCommision::where('trans_id',$id)->first();
				JournalAccount::where('activity','=','redeem')->where('activity_id','=',$redeem['id'])->delete();
				RedeemCommision::destroy($redeem['id']);
				
				$debt=CustomerDebt::where('transaction_id',$id)->get();
				JournalAccount::where('activity','=','customer_debts')->where('activity_id','=',$debt['id'])->delete();
				CustomerDebt::destroy($debt['id']);
				
                return response()->json([
                    "status" => "success",
                    "message" => "Transaction has been deleted!"
                ], 200);
            } else {
                return response()->json([
                    "status" => "error",
                    "message" => "Failed delete transaction!"
                ], 403);
            }
        }else{
            return error_unauthorized();
        }
    }
	
	public function openTrip(Request $request, $id){		
		$user = User::where('token',$request->header("X-Auth-Token"))->first();
		$cek = check_auth($user,'create transactions');
		if($cek['result']!=1){
			$rolling=TransactionDetail::where('flags','=','1credit')
				->orWhere('flags','=','credit')
				->where('transaction_id','=',$id)
				->value('rolling');
				
			TransactionDetail::create(array(
				"transaction_id" => $id,
				"rolling" => $rolling,
				"flags" => "open",
				"time_transaction" => date('H:i:s'),
				"type" => 0,
				"user_id" => $user->id
			));
		}
		
		return response()->json([
			"status" => "success",
			"message" => "Transaction has been opened!",
		],200);
	}

    public function storeDetail(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'rolling' => 'required|numeric',
            'time_transaction' => 'required'
        ]);

        if($validator->fails()){
            return response()->json(["status"=>"error","message" => implode(' ',$validator->errors()->all())], 422);
        }else{

            $user = User::where('token',$request->header("X-Auth-Token"))->first();
            $cek = check_auth($user,'create transactions');
            if($cek['result']==1){
                $transaction = Transaction::find($id);
                if($transaction==null){
                    return response()->json(["message" => "Transaction not exists!"],404);
                }else{
                    $log = TransactionDetail::create(array(
                        "transaction_id" => $id,
                        "rolling" => $request->input('rolling'),
                        "flags" => $request->input('flags')?$request->input('flags'):'role',
                        "time_transaction" => $request->input('time_transaction'),
                        "type" => $request->input('type')?$request->input('type'):0,
						"user_id" => $user->id
                    ));
					if($request->input('flags')=='credit' || $request->input('flags')=='1credit'){
						Transaction::journalCredit($log->id,$request);
						Transaction::debtTrip($id);
					}
                    UserActivity::storeActivity(array(
                        "activity" => "add transactions detail for id: ".$log->id,
                        "user" => $user->id,
                        "menu" => "transactions",
                        "ipaddress" => $request->ip()
                    ));
                }
            }else{
                return error_unauthorized();
            }

            if($log){
                return response()->json([
                    "status" => "success",
                    "message" => "Transaction detail has been saved!",
                    "data" => $log
                ],200);
            }else{
                return response()->json([
                    "status" => "error",
                    "message" => "Failed save transaction detail!",
                    "data" => null
                ],403);
            }
        }
    }

    /**
     * Display detail by id.
     * GET /api/get_detail/{detail}
     *
     * @param string $token     The token for authentication
     * @return Response
     **/
    public function showDetailById(Request $request, $id)
    {
        $user = User::where('token',$request->header("X-Auth-Token"))->first();
        $cek = check_auth($user,'read transactions');
        if($cek['result']==1){
            $detail = TransactionDetail::find($id);
            if($detail!=null){
                return response()->json([
                    "message" => "success",
                    "data" => $detail
                ],200);
            }else{
                return response()->json(["message" => "Transaction not found!"],404);
            }
        }else{
            return error_unauthorized();
        }
    }

    public function updateDetail(Request $request, $id, $transaction_id)
    {
        $validator = Validator::make($request->all(), [
            'rolling' => 'required|numeric',
            'time_transaction' => 'required'
        ]);

        if($validator->fails()){
            return response()->json(["status"=>"error","message" => implode(' ',$validator->errors()->all())], 422);
        }else{
            $user = User::where('token',$request->header("X-Auth-Token"))->first();
            $cek = check_auth($user,'update transactions');
            if($cek['result']==1){
                $detail = TransactionDetail::where(['id'=>$id, 'transaction_id' => $transaction_id])->first();
                if($detail==null){
                    return response()->json(["message" => "Transaction not exists!"],404);
                }else{
                    $data = $request->all();
					$data['type']=$data['type']?$data['type']:0;
                    $log = $detail->update($data);
					if($request->input('flags')=='credit' || $request->input('flags')=='1credit'){
						Transaction::journalCredit($id,$request);
						Transaction::debtTrip($transaction_id);
					}
                    UserActivity::storeActivity(array(
                        "activity" => "update transactions detail for id: ".$detail->id,
                        "user" =>$user->id,
                        "menu" =>"transactions",
                        "ipaddress" => $request->ip()
                    ));
                }
            }else{
                return error_unauthorized();
            }

            if($log){
                return response()->json([
                    "status" => "success",
                    "message" => "Transaction detail has been update!",
                    "data" => $detail
                ],200);
            }else{
                return response()->json([
                    "status" => "error",
                    "message" => "Failed update transaction detail!",
                    "data" => null
                ],403);
            }
        }
    }

    public function destroyDetail(Request $request, $id, $transaction_id)
    {
        $detail = TransactionDetail::where(['id'=>$id, 'transaction_id' => $transaction_id])->first();

        if($detail == null){
            return response()->json(["message"=>"Transaction not exist"],404);
        }

        $user = User::where('token',$request->header("X-Auth-Token"))->first();
        $cek = check_auth($user,'delete transactions');
        if($cek['result']==1) {
            if (TransactionDetail::destroy($id)) {
				JournalAccount::where('activity','=','credit_trip')->where('activity_id','=',$id)->delete();
				Transaction::debtTrip($transaction_id);
                UserActivity::storeActivity(array(
                    "activity" => "delete detail transaction for id: ".$id,
                    "user" => $user->id,
                    "menu" => "transactions",
                    "ipaddress" => $request->ip()
                ));
                return response()->json([
                    "status" => "success",
                    "message" => "Transaction detail has been deleted!"
                ], 200);
            } else {
                return response()->json([
                    "status" => "error",
                    "message" => "Failed delete transaction detail!"
                ], 403);
            }
        }else{
            return error_unauthorized();
        }
    }

    /**
     * Display detail by transaction id.
     * GET /api/transaction_detail/{transaction}
     *
     * @param string $token     The token for authentication
     * @return Response
     **/
    public function showDetailByTransactionId(Request $request, $id, $type = 0)
    {
        $user = User::where('token',$request->header("X-Auth-Token"))->first();
        $cek = check_auth($user,'read transactions');
        if($cek['result']==1){
            $flags = $request->input('flags');
            $flags2 = $request->input('flags2');
            if($flags != null){
                $condition = array("transaction_id" => $id);
                if($flags != "open"){
                    $condition["type"]  = $type;
                }
                $details = TransactionDetail::where($condition)
                    ->where('flags', 'LIKE', $flags)
                    ->get();

            }else{
                $details = TransactionDetail::where('transaction_id', $id)->get();
            }

            if($details!=null){
                return response()->json([
                    "message" => "success",
                    "data" => $details
                ],200);
            }else{
                return response()->json(["message" => "Transaction not found!"],404);
            }
        }else{
            return error_unauthorized();
        }
    }

    public function closingTrip(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'closing_cash_chips' => 'required|numeric',
            'closing_nn_chips' => 'required|numeric',
            'closing_cash' => 'required|numeric',
        ]);

        if($validator->fails()) {
            return response()->json(["status"=>"error","message" => implode(' ',$validator->errors()->all())], 422);
        }

        $user = User::where('token',$request->header("X-Auth-Token"))->first();
        $cek = check_auth($user,'update transactions');
        if($cek['result']==1){
            $transaction = Transaction::find($id);
            if($transaction != null){
                if($transaction->user_id != $user->id && $user->roles[0]->id!=4){
                    return response()->json(["message" => "Not allowed!"],403);
                }else{
                    $data['closing_nn_chips']= $request->input('closing_nn_chips');
                    $data['closing_cash_chips']= $request->input('closing_cash_chips');
                    $data['closing_cash']= $request->input('closing_cash');
                    $data['check_in_commision']= $request->input('check_in_commision');
					$data['closing_by']=$user->id;
					$data['closing_date']= $request->input('closing_date')?$request->input('closing_date'):date('Y-m-d H:i:s');
                    $data['status'] = 0;
					
                    $transaction->update($data);
					
					$redeem=RedeemCommision::where(['trans_id'=>$id])->first();
					if(count($redeem)==0){
						RedeemCommision::create(['trans_id'=>$id,'amount'=>$data['check_in_commision']]);
					}else{
						RedeemCommision::where(['trans_id'=>$id])->update([
							'amount'=>$data['check_in_commision'],
							'redeemed'=>0,
							'account_id'=>null,
							'redeem_date'=>null
						]);
					}
					
					$debt=CustomerDebt::where(['transaction_id'=>$id])->first();
					$data_debt['transaction_id']=$id;
					$data_debt['date']=$transaction->closing_date;
					$data_debt['due_date']=$transaction->closing_date;
					$data_debt['customer_id']=$transaction->customer_id;
					$data_debt['staff_id']=$user->id;
					$data_debt['amount']=$request->input('debt');
					$data_debt['amount_due']=$request->input('debt');
					if(count($debt)==0){
						CustomerDebt::create($data_debt);
					}else{
						CustomerDebt::where('transaction_id','=',$id)->update($data_debt);
					}
					
					Transaction::journalClosing($id,$request);
					
                    UserActivity::storeActivity(array(
                        "activity" => "closing trip for transaction id: ".$id,
                        "user" => $user->id,
                        "menu" => "transactions",
                        "ipaddress" => $request->ip()
                    ));
                }
            }
        }

        if($transaction){
            return response()->json([
                "status" => "success",
                "message" => "Trip has been closing!",
                "data" => $transaction
            ],200);
        }else{
            return response()->json([
                "status" => "error",
                "message" => "Failed closing trip!",
                "data" => null
            ],403);
        }
    }
	
	public function checkInCommision(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'check_in_commision' => 'required|numeric',
        ]);

        if($validator->fails()) {
            return response()->json(["status"=>"error","message" => implode(' ',$validator->errors()->all())], 422);
        }

        $user = User::where('token',$request->header("X-Auth-Token"))->first();
        $cek = check_auth($user,'update transactions');
        if($cek['result']==1){
            $transaction = Transaction::find($id);
            if($transaction != null){
                if($transaction->user_id != $user->id){
                    return response()->json(["message" => "Not allowed!"],403);
                }else{
                    $data = $request->all();
                    $data['status'] = 0;
                    $transaction->update($data);
                    UserActivity::storeActivity(array(
                        "activity" => "check in commission for transaction id: ".$id,
                        "user" => $user->id,
                        "menu" => "transactions",
                        "ipaddress" => $request->ip()
                    ));
                }
            }
        }

        if($transaction){
            return response()->json([
                "status" => "success",
                "message" => "Commission been check in!",
                "data" => $transaction
            ],200);
        }else{
            return response()->json([
                "status" => "error",
                "message" => "Failed check in commission!",
                "data" => null
            ],403);
        }
    }

    public function storeRolling(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'rolling' => 'required|numeric',
            'insurance' => 'required|numeric',
            'amount' => 'required|numeric',
            'total' => 'required|numeric',
            'room' => 'required',
            'odds' => 'required|numeric',
            'points' => 'required|numeric'
        ]);

        if($validator->fails()){
            return response()->json(["status"=>"error","message" => implode(' ',$validator->errors()->all())], 422);
        }else{
            $user = User::where('token',$request->header("X-Auth-Token"))->first();
            $cek = check_auth($user,'create transactions');
            if($cek['result']==1){
                $transaction = Transaction::find($id);
                if($transaction==null){
                    return response()->json(["message" => "Transaction not exists!"],404);
                }else{
					$data = $request->all();
					$data['transaction_id']=$id;
                    $log = TransactionRolling::create($data);
					Transaction::journalRolling2($log->id,$request);
                    UserActivity::storeActivity(array(
                        "activity" => "add transactions rolling 2 for id: ".$log->id,
                        "user" => $user->id,
                        "menu" => "transactions",
                        "ipaddress" => $request->ip()
                    ));
                }
            }else{
                return error_unauthorized();
            }

            if($log){
                return response()->json([
                    "status" => "success",
                    "message" => "Transaction rolling 2 has been saved!",
                    "data" => $log
                ],200);
            }else{
                return response()->json([
                    "status" => "error",
                    "message" => "Failed save transaction rolling 2!",
                    "data" => null
                ],403);
            }
        }
    }

    public function showRollingById(Request $request, $id)
    {
        $user = User::where('token',$request->header("X-Auth-Token"))->first();
        $cek = check_auth($user,'read transactions');
        if($cek['result']==1){
            $detail = TransactionRolling::find($id);
            if($detail!=null){
                return response()->json([
                    "message" => "success",
                    "data" => $detail
                ],200);
            }else{
                return response()->json(["message" => "Transaction not found!"],404);
            }
        }else{
            return error_unauthorized();
        }
    }

    public function updateRolling(Request $request, $id, $transaction_id)
    {
        $validator = Validator::make($request->all(), [
            'amount' => 'required|numeric',
            'total' => 'required|numeric',
            'odds' => 'required|numeric',
            'points' => 'required|numeric'
        ]);

        if($validator->fails()){
            return response()->json(["status"=>"error","message" => implode(' ',$validator->errors()->all())], 422);
        }else{
            $user = User::where('token',$request->header("X-Auth-Token"))->first();
            $cek = check_auth($user,'update transactions');
            if($cek['result']==1){
                $detail = TransactionRolling::where(['id'=>$id, 'transaction_id' => $transaction_id])->first();
                if($detail==null){
                    return response()->json(["message" => "Transaction not exists!"],404);
                }else{
                    $data = $request->all();
                    $log = $detail->update($data);
					Transaction::journalRolling2($id,$request);
                    UserActivity::storeActivity(array(
                        "activity" => "update transactions rolling 2 for id: ".$detail->id,
                        "user" => $user->id,
                        "menu" => "transactions",
                        "ipaddress" => $request->ip()
                    ));
                }
            }else{
                return error_unauthorized();
            }

            if($log){
                return response()->json([
                    "status" => "success",
                    "message" => "Transaction rolling 2 has been update!",
                    "data" => $detail
                ],200);
            }else{
                return response()->json([
                    "status" => "error",
                    "message" => "Failed update transaction rolling 2!",
                    "data" => null
                ],403);
            }
        }
    }
    public function destroyRolling(Request $request, $id, $transaction_id)
    {
        $detail = TransactionRolling::where(['id'=>$id, 'transaction_id' => $transaction_id])->first();

        if($detail == null){
            return response()->json(["message"=>"Transaction not exist"],404);
        }

        $user = User::where('token',$request->header("X-Auth-Token"))->first();
        $cek = check_auth($user,'delete transactions');
        if($cek['result']==1) {
            if (TransactionRolling::destroy($id)) {
				JournalAccount::where('activity','=','rolling2')->where('activity_id','=',$id)->delete();
                UserActivity::storeActivity(array(
                    "activity" => "delete transaction rolling 2 for id: ".$id,
                    "user" => $user->id,
                    "menu" => "transactions",
                    "ipaddress" => $request->ip()
                ));
                return response()->json([
                    "status" => "success",
                    "message" => "Transaction rolling 2 has been deleted!"
                ], 200);
            } else {
                return response()->json([
                    "status" => "error",
                    "message" => "Failed delete transaction rolling 2!"
                ], 403);
            }
        }else{
            return error_unauthorized();
        }
    }

    public function showRollingByTransactionId(Request $request, $id)
    {
        $user = User::where('token',$request->header("X-Auth-Token"))->first();
        $cek = check_auth($user,'read transactions');
        if($cek['result']==1){
            $details = TransactionRolling::where('transaction_id', $id)->get();
			
            if($details!=null){
                return response()->json([
                    "message" => "success",
                    "data" => $details
                ],200);
            }else{
                return response()->json(["message" => "Transaction not found!"],404);
            }
        }else{
            return error_unauthorized();
        }
    }
	
	public function updateTripRolling2(Request $request){
		$data = $request->all();
		$data['status'] = 1;
		$user = User::where('token',$request->header("X-Auth-Token"))->first();
		$cek = check_auth($user,'update transactions');
		if($cek['result']==1){
			UserActivity::storeActivity(array(
				"activity" => "update transaction trip for id: ".$id,
				"user" => $user->id,
				"menu" => "transactions",
				"ipaddress" => $request->ip()
			));
			$transaction = Transaction::find($id);
			if($transaction==null){
				return response()->json(["message" => "Transaction not exists!"],404);
			}else{
				$transaction->update($data);
			}
		}else{
			return error_unauthorized();
		}

		if($transaction){
			return response()->json([
				"status" => "success",
				"message" => "Transaction has been updated!",
				"data" => $transaction
			],200);
		}else{
			return response()->json([
				"status" => "error",
				"message" => "Failed update transaction!",
				"data" => null
			],403);
		}
	}
	
	public function assigning($id,Request $request){
        $validator = Validator::make($request->all(), [
            'assign_to' => 'required'
        ]);
		
		if($validator->fails()){
            return response()->json(["status"=>"error","message" => implode(' ',$validator->errors()->all())], 422);
        }
		
		$user = User::where('token',$request->header("X-Auth-Token"))->first();
		$cek = check_auth($user,'update transactions');
		if($cek['result']==1){
			$transaction = Transaction::find($id);
			$data['assign_from_old']=$transaction['assign_from']?$transaction['assign_from']:null;
			$data['assign_from']=$request->input('assign_from')?$request->input('assign_from'):($transaction['assign_from_old']?$transaction['assign_from_old']:null);
			$data['assign_to']=$request->input('assign_to');
			$data['user_id']=$request->input('assign_to');
			if($transaction==null){
				return response()->json(["message" => "Transaction not exists!"],404);
			}else{
				$transaction->update($data);
				UserActivity::storeActivity(array(
                    "activity" => "Assigning transaction for id: ".$id,
                    "user" => $user->id,
                    "menu" => "transactions",
                    "ipaddress" => $request->ip()
                ));
			}
			
			if($transaction){
				return response()->json([
					"status" => "success",
					"message" => "Assign Transaction has been updated!",
					"data" => $transaction
				],200);
			}else{
				return response()->json([
					"status" => "error",
					"message" => "Assign Transaction update transaction!",
					"data" => null
				],403);
			}
		}else{
			return error_unauthorized();
		}
	}
	
	public function currentTrip(Request $request){
		$user_id=$request->input('user_id');
		$user = User::where('token',$request->header("X-Auth-Token"))->first();
        $cek = check_auth($user,'read transactions');
        if($cek['result']==1){
			if($user->roles[0]->id != 1){
				$user_id=$user->id;
			}
			
			$date=date('Y-m-d');
			$time_now=date('H:i:s');
			$time_new_day='08:00:00';
			
			if($time_now>$time_new_day){
				// echo 'morning';
				$date=date('Y-m-d',strtotime($date));
			}else{
				// echo 'night';
				$date=date('Y-m-d',strtotime($date.' -1 days'));
			}
			
			$transactions = Transaction::select(array('transactions.*',DB::raw('(transactions.nn_chips+ifnull(sum(if(transaction_details.flags="credit",transaction_details.rolling,0)),0))as credit')))
				->join('customers','customers.id','=','transactions.customer_id')
				->join('users','users.id','=','transactions.user_id')
				->join('transaction_details','transaction_details.transaction_id','=','transactions.id','left')
				->whereRaw('(transactions.user_id='.$user_id.' or assign_to='.$user_id.') and ((closing_by='.$user_id.' and closing_date>"'.$date.' '.$time_new_day.'")or closing_by is null)')
				->groupBy('transactions.id')
				->paginate(100);
		}else{
			return error_unauthorized();
		}
		
        return response()->json(transformCollection($transactions), 200);
	}

}
