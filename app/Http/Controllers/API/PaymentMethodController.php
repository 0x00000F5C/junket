<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\PaymentMethod;

class PaymentMethodController extends Controller
{

    /**
     * Payment Method List
     * GET /api/pay_methods
     *
     * @param string $token The token for authentication
     * @return Response
     **/
    public function lists(){
        $methods = PaymentMethod::all();
        return response()->json([
            "data" => $methods
        ],200);
    }

    /**
     * Get Payment Method by ID
     * GET /api/pay_method/{payment_method_id}
     *
     * @param string $token The token for authentication
     * @return Response
     **/
    public function show($id)
    {
        $method = PaymentMethod::find($id);

        if($method == null){
            return response()->json(["error" => "Payment method not found!"],404);
        }else{
            return response()->json(["method" => $method],200);
        }

    }
}
