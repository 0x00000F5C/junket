<?php

namespace App\Http\Controllers\API;

use App\CustomerDebt;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Customer;
use App\User;
use Validator;
use DB;
use App\UserActivity;

class CustomerController extends Controller
{
    /**
     * Customers List
     * GET /api/customer/get_lists
     *
     * @param string $token The token for authentication
     * @return Response
     **/
    public function lists(){
        $customers = Customer::orderBy('first_name','asc')->get();
        return response()->json([
            "message" => "success",
            "data" => $customers
        ],200);
    }

    /**
     * Store Customers
     * POST /api/customer/save
     *
     * @param string $token            The token for authentication
     * @param integer $customer_data   Customer data
     * @return Response
     **/
    public function store(Request $request){
        $validator = Validator::make($request->all(), [
            'customer_type_id' => 'required',
            'title' => 'required',
            'first_name' => 'required|max:50'
        ]);
		
		$validator->sometimes('email', 'email|max:255|unique:customers', function($input) {
			return !empty($input->email);
		});

        if($validator->fails()){
            return response()->json(["status"=>"error","message" => implode(' ',$validator->errors()->all())], 422);
        }else {
            $data = $request->all();
            $user = User::where('token',$request->header("X-Auth-Token"))->first();
            $cek = check_auth($user,'create customers');
            if($cek['result'] == 1){
				$data['dob']=(isset($data['year']) ? $data['year'] : '0000').'-'.(isset($data['month']) ? $data['month'] : '00').'-'.(isset($data['date']) ? $data['date'] : '00');
                $customer = Customer::create($data);
                UserActivity::storeActivity(array(
                    "activity" => "add customer for id: ".$customer->id,
                    "user" => $user->id,
                    "menu" => "customers",
                    "ipaddress" => $request->ip()
                ));
            }else{
                return error_unauthorized();
            }
        }

        if($customer){
            return response()->json([
                "status" => "success",
                "message" => "Customer has been saved!",
                "data" => $customer
            ],200);
        }else{
            return response()->json([
                "status" => "error",
                "message" => "Failed save customer!",
                "data" => null
            ],403);
        }
    }

    /**
     * Get Customer by ID
     * GET /api/customer/{customer_id}/get
     *
     * @param string $token                 The token for authentication
     * @return Response
     **/
    public function show(Request $request,$id)
    {
        $user = User::where('token',$request->header("X-Auth-Token"))->first();
        $cek = check_auth($user,'read customers');
        if($cek['result']==1){
            $customer = Customer::join(DB::raw('(select cd.customer_id,sum(cd.amount_due)-ifnull(p.amount,0)as amount_due
				from customer_debts cd
				left join (
					select pd.customer_id,sum(pdd.amount)as amount
					from payment_debts pd
					join payment_debt_details pdd on pdd.payment_debt_id=pd.id
					where pd.customer_id='.$id.' and pd.deleted_at is null
					group by pd.customer_id
				)as p on p.customer_id=cd.customer_id
				where cd.deleted_at is null and cd.customer_id='.$id.')as debt'),'debt.customer_id','=','customers.id','left')->find($id);
            if($customer!=null){
				$customer->date=date('d',strtotime($customer->dob));
				$customer->month=date('m',strtotime($customer->dob));
				$customer->year=date('Y',strtotime($customer->dob));

                $historiesTrip = Customer::historyTips($id);
                $customer_debt = Customer::debts($id);
				
                return response()->json([
                    "message" => "success",
                    "data" => $customer,
                    "histories" => $historiesTrip,
                    "customer_debt" => $customer_debt
                ],200);
            }else{
                return response()->json(["message" => "Customer not exists!"],404);
            }
        }else{
            return error_unauthorized();
        }
    }

    /**
     * Update Customer
     * PUT /api/customer/update
     *
     * @param string $token                 The token for authentication
     * @param integer $customer_id          The customer id
     * @param integer $customer_type_id     The customer type id
     * @param string $title                 The title customer
     * @param string $first_name            The customer first name
     * @param string $last_name             The customer last name
     * @param date $dob                     The customer day of birth
     * @param string $gender                The customer gender
     * @param integer $age                  The customer age
     * @param text $address                 The customer address
     * @param string $postal_code           The customer postal code
     * @param string $phone_code            The customer phone code
     * @param string $contact               The customer contact
     * @param string $company_name          The customer company name
     * @param text $background              The customer background
     * @param text $runner_remarks          The customer runner remarks
     * @param double $commissions           The customer commissions
     * @param string $debt                  The customer debt
     * @param string $credit_term           The customer credit term
     * @param string $grc_number            The customer grc number
     * @param double $check_in_commission   The customer check in commission
     * @param string $group_flag            The customer status individual/group/junket
     * @param string $agent_flag            The customer for agent/gl group
     * @param string $agent_id              The customer agent id
     * @return Response
     **/
    public function update(Request $request)
    {
        //
        $validator = Validator::make($request->all(), [
            'id' => 'required',
            'customer_type_id' => 'required',
            'title' => 'required',
            'first_name' => 'required|max:50'
        ]);
		
		$validator->sometimes('email', 'email|max:255|unique:customers,email,'.$request->input('id'), function($input) {
			return !empty($input->email);
		});

        if($validator->fails()){
            return response()->json(["status"=>"error","message" => implode(' ',$validator->errors()->all())], 422);
        }else {
            $data = $request->all();
            $user = User::where('token',$request->header("X-Auth-Token"))->first();
            $cek = check_auth($user,'update customers');
            if($cek['result']==1){
                UserActivity::storeActivity(array(
                    "activity" => "update customer for id: ".$data['id'],
                    "user" =>$user->id,
                    "menu" =>"customers",
                    "ipaddress" => $request->ip()
                ));
                $customer = Customer::find($data['id']);
                if($customer==null){
                    return response()->json(["message" => "Customer not exists!"],404);
                }else {
                    $customer->customer_type_id = $data['customer_type_id'];
                    $customer->title = $data['title'];
                    $customer->first_name = $data['first_name'];
                    $customer->last_name = isset($data['last_name']) ? $data['last_name'] : '' ;
                    // $customer->dob = isset($data['dob']) ? $data['dob'] : '';
					$customer->dob = (isset($data['year']) ? $data['year'] : '0000').'-'.(isset($data['month']) ? $data['month'] : '00').'-'.(isset($data['date']) ? $data['date'] : '00');
                    $customer->gender = isset($data['gender']) ? $data['gender'] : '';
                    $customer->age = isset($data['age']) ? $data['age'] : '';
                    $customer->address = isset($data['address']) ? $data['address'] : '';
                    $customer->contact = isset($data['contact']) ? $data['contact'] : '';
                    $customer->postal_code = isset($data['postal_code']) ? $data['postal_code'] : '';
                    $customer->phone_code = isset($data['phone_code']) ? $data['phone_code'] : '';
                    $customer->email = isset($data['email']) ? $data['email'] : '';
                    $customer->company_name = isset($data['company_name']) ? $data['company_name'] : '';
                    $customer->background = isset($data['background']) ? $data['background'] : '';
                    $customer->runner_remarks = isset($data['runner_remarks']) ? $data['runner_remarks'] : '';
                    $customer->commission = isset($data['commission']) ? $data['commission'] : '';
                    $customer->debt = isset($data['debt']) ? $data['debt'] : '';
                    $customer->credit_term = isset($data['credit_term']) ? $data['credit_term'] : '';
                    $customer->grc_number = isset($data['grc_number']) ? $data['grc_number'] : '';
                    $customer->check_in_commission = isset($data['check_in_commission']) ? $data['check_in_commission'] : '';
                    $customer->group_flag = isset($data['group_flag']) ? $data['group_flag'] : '';
                    $customer->agent_flag = isset($data['agent_flag']) ? $data['agent_flag'] : '';
                    $customer->agent_id = isset($data['agent_id']) ? $data['agent_id'] : '';
                    $customer->address2 = isset($data['address2']) ? $data['address2'] : '';
                    $customer->postal_code2 = isset($data['postal_code2']) ? $data['postal_code2'] : '';

                    $customer->save();
                }
            }else{
                return error_unauthorized();
            }
        }

        if($customer){
            return response()->json([
                "status" => "success",
                "message" => "Customer has been update!",
                "data" => $customer
            ],200);
        }else{
            return response()->json([
                "status" => "error",
                "message" => "Failed update customer!",
                "data" => null
            ],403);
        }
    }

    /**
     * Delete Customers
     * DELETE /api/customer/{customer_id}/delete
     *
     * @param string $token                 The token for authentication
     * @return Response
     **/
    public function destroy(Request $request,$id)
    {
        //
        $customer = Customer::find($id);

        if($customer == null){
            return response()->json(["message"=>"Customer not exist"],404);
        }

        $user = User::where('token',$request->header("X-Auth-Token"))->first();
        $cek = check_auth($user,'delete customers');
        if($cek['result']==1) {
            UserActivity::storeActivity(array(
                "activity" => "delete customer for id: ".$id,
                "user" =>$user->id,
                "menu" =>"customers",
                "ipaddress" => $request->ip()
            ));
            if (Customer::destroy($id)) {
                return response()->json([
                    "status" => "success",
                    "message" => "Customer has been deleted!"
                ], 200);
            } else {
                return response()->json([
                    "status" => "error",
                    "message" => "Failed delete customer!"
                ], 403);
            }
        }else{
            return error_unauthorized();
        }
    }

    /**
     * Index Customers
     * GET /api/customer/all?search=
     *
     * @param string $token                 The token for authentication
     * @return Response
     **/
    public function index(Request $request){
        $user = User::where('token',$request->header("X-Auth-Token"))->first();
        $cek = check_auth($user,'read customers');
        if($cek['result']==1){
            $search = $request->input('search');
            $sort_by = $request->input('sort_by');
            $sort_type = $request->input('sort_type');
			
			$customers = Customer::select(['customers.*','role.rolling','debt.amount_due'])
				->join('customer_types','customer_types.id','=','customers.customer_type_id')
				->join(DB::raw('(select t.customer_id,sum(td.rolling)as rolling 
					from transaction_details td 
					join transactions t on t.id=td.transaction_id 
					where t.deleted_at is null and td.flags="role"
					group by t.customer_id)as role'),'role.customer_id','=','customers.id','left')
				->join(DB::raw('(select cd.customer_id,sum(cd.amount_due)-ifnull(p.amount,0)as amount_due
					from customer_debts cd
					left join (
						select pd.customer_id,sum(pdd.amount)as amount
						from payment_debts pd
						join payment_debt_details pdd on pdd.payment_debt_id=pd.id
						where pd.deleted_at is null
						group by pd.customer_id
					)as p on p.customer_id=cd.customer_id
					where cd.deleted_at is null group by cd.customer_id)as debt'),'debt.customer_id','=','customers.id','left');
						
            if($search!=null){
                $customers = $customers->where(DB::raw("CONCAT(first_name,' ',last_name)"),'LIKE','%'.$search.'%')
					->orWhere('title','LIKE','%'.$search.'%')
					->orWhere('gender','LIKE','%'.$search.'%')
					->orWhere(DB::raw("CONCAT(phone_code,' ',contact)"),'LIKE','%'.$search.'%')
					->orWhere('customer_types.name','LIKE','%'.$search.'%')
					->orWhere('group_flag','LIKE','%'.$search.'%')
					->orWhere('grc_number','LIKE','%'.$search.'%');
            }
			
			if(!$sort_by){
				$sort_by='customers.created_at';
			}
			$sort_type=$sort_type?$sort_type:'desc';
			
			switch($sort_by){
				case 'customer_type':
					$sort_by='customer_types.name';
					break;
			}
			
			$customers = $customers->orderBy($sort_by,$sort_type)->paginate(10);
        }else{
            return error_unauthorized();
        }

        return response()->json(transformCollection($customers), 200);
    }

	public function getCustomerHasDebt() {
		$customers = CustomerDebt::groupBy("customer_id")->join("customers", "customers.id", "=", "customer_debts.customer_id")->get();
		return $customers;
	}

}
