<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Activity;
use App\User;
use Validator;

class ActivityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = User::where('token',$request->header("X-Auth-Token"))->first();
        $date = $request->input('date');
        $month = date('m', strtotime($date));
        $year = date('Y', strtotime($date));
        $activities = Activity::where('user_id', $user->id)
            ->whereMonth('start', '=', $month)
            ->whereYear('start', '=', $year)
            ->orderBy('start')->get();

        return response()->json(["data" => $activities], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = User::where('token',$request->header("X-Auth-Token"))->first();
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'start' => 'required|date',
        ]);

        if($validator->fails()){
            return response()->json(["status"=>"error","message" => implode(' ',$validator->errors()->all())], 422);
        }else{
            $data = $request->all();
            $data['user_id'] = $user->id;
            $activity = Activity::create($data);
        }

        if($activity){
            return response()->json([
                "status" => "success",
                "message" => "Activity has been saved!",
                "data" => $activity
            ],200);
        }else{
            return response()->json([
                "status" => "error",
                "message" => "Failed save activity!",
                "data" => null
            ],403);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $activity = Activity::find($id);

        return response()->json(["data" => $activity], 200);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::where('token',$request->header("X-Auth-Token"))->first();
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'start' => 'required|date',
        ]);

        if($validator->fails()){
            return response()->json(["status"=>"error","message" => implode(' ',$validator->errors()->all())], 422);
        }else{
            $data = $request->all();
            $activity = Activity::where('id', $id)
                ->where('user_id', $user->id)->first();
            if($activity == null){
                return response()->json(["message" => "Data not found"], 404);
            }else{
                $log = $activity->update($data);
            }
        }

        if($log){
            return response()->json([
                "status" => "success",
                "message" => "Activity has been update!",
                "data" => $activity
            ],200);
        }else{
            return response()->json([
                "status" => "error",
                "message" => "Failed update activity!",
                "data" => null
            ],403);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $user = User::where('token',$request->header("X-Auth-Token"))->first();
        $activity = Activity::where('id', $id)->where('user_id', $user->id)->first();
        if($activity == null){
            return response()->json(["message" => "Data not found"], 404);
        }else{
            if ($activity->delete()) {
                return response()->json([
                    "status" => "success",
                    "message" => "Activity has been deleted!"
                ], 200);
            } else {
                return response()->json([
                    "status" => "error",
                    "message" => "Failed delete activity!"
                ], 403);
            }
        }
    }
}
