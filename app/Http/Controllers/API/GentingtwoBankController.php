<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\PaymentMethod;
use App\GentingtwoBank;
use Validator;
use App\UserActivity;

class GentingtwoBankController extends Controller
{
    /**
     * Index Transaction Bank
     * GET /api/transactions?search=
     *
     * @param string $token                 The token for authentication
     * @return Response
     **/
    public function index(Request $request){
        $user = User::where('token',$request->header("X-Auth-Token"))->first();
        $cek = check_auth($user,'read bank_genting2');
        if($cek['result']==1){
            $search = $request->input('search');
            $sort_by = $request->input('sort_by');
            $sort_type = $request->input('sort_type');
            if($search!=null){
                $trasactions = GentingtwoBank::where('date', $search)
                    ->sortable([$sort_by => $sort_type])
                    ->paginate(10);
            }else{
                if($sort_by != null){
                    $trasactions = GentingtwoBank::sortable([$sort_by => $sort_type])->paginate();
                }else{
                    $trasactions = GentingtwoBank::orderBy('created_at','desc')->paginate();
                }
            }
        }else{
            return error_unauthorized();
        }

        return response()->json(transformCollection($trasactions), 200);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'date' => 'required|date',
            'credit_debit' => 'required|in:IN,OUT',
            'bank_id' => 'required|integer',
            'amount' => 'required|numeric'
        ]);

        if($validator->fails()){
            return response()->json(["status"=>"error","message" => implode(' ',$validator->errors()->all())], 422);
        }else {
            $data = $request->all();
            $user = User::where('token',$request->header("X-Auth-Token"))->first();
            $cek = check_auth($user,'create bank_genting2');
            if($cek['result'] == 1){
                $data['user_id'] = $user->id;
                $transaction = GentingtwoBank::create($data);
                UserActivity::storeActivity(array(
                    "activity" => "add transacton bank for id: ".$transaction->id,
                    "user" => $user->id,
                    "menu" => "bank_genting2",
                    "ipaddress" => $request->ip()
                ));
            }else{
                return error_unauthorized();
            }
        }

        if($transaction){
            return response()->json([
                "status" => "success",
                "message" => "Transaction has been saved!",
                "data" => $transaction
            ],200);
        }else{
            return response()->json([
                "status" => "error",
                "message" => "Failed save transaction!",
                "data" => null
            ],403);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $user = User::where('token',$request->header("X-Auth-Token"))->first();
        $cek = check_auth($user,'read bank_genting2');
        if($cek['result']==1){
            $transaction = GentingtwoBank::find($id);
            if($transaction!=null){
                return response()->json([
                    "message" => "success",
                    "data" => $transaction
                ],200);
            }else{
                return response()->json(["message" => "Transaction not exists!"],404);
            }
        }else{
            return error_unauthorized();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'date' => 'required|date',
            'credit_debit' => 'required|in:IN,OUT',
            'bank_id' => 'required|integer',
            'amount' => 'required|numeric'
        ]);

        if($validator->fails()){
            return response()->json(["status"=>"error","message" => implode(' ',$validator->errors()->all())], 422);
        }else {
            $data = $request->all();
            $user = User::where('token',$request->header("X-Auth-Token"))->first();
            $cek = check_auth($user,'update bank_genting2');
            if($cek['result'] == 1){
                $data['user_id'] = $user->id;

                $transaction = GentingtwoBank::find($id);
                if($transaction == null){
                    return response()->json(["message" => "Transaction not exists!"],404);
                }else{
                    $transaction->update($data);

                    UserActivity::storeActivity(array(
                        "activity" => "update transaction bank for id: ".$id,
                        "user" => $user->id,
                        "menu" => "bank_genting2",
                        "ipaddress" => $request->ip()
                    ));
                }
            }else{
                return error_unauthorized();
            }
        }

        if($transaction){
            return response()->json([
                "status" => "success",
                "message" => "Transaction bank has been updated!",
                "data" => $transaction
            ],200);
        }else{
            return response()->json([
                "status" => "error",
                "message" => "Failed update transaction bank!",
                "data" => null
            ],403);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $transaction = GentingtwoBank::find($id);

        if($transaction == null){
            return response()->json(["message"=>"Transaction not exist"],404);
        }

        $user = User::where('token',$request->header("X-Auth-Token"))->first();
        $cek = check_auth($user,'delete bank_genting2');
        if($cek['result']==1) {
            if (GentingtwoBank::destroy($id)) {
                UserActivity::storeActivity(array(
                    "activity" => "delete transaction bank for id: ".$id,
                    "user" => $user->id,
                    "menu" => "bank_genting2",
                    "ipaddress" => $request->ip()
                ));
                return response()->json([
                    "status" => "success",
                    "message" => "Transaction bank has been deleted!"
                ], 200);
            } else {
                return response()->json([
                    "status" => "error",
                    "message" => "Failed delete transaction bank!"
                ], 403);
            }
        }else{
            return error_unauthorized();
        }
    }
}
