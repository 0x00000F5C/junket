<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Validator;
use App\User;
use App\UserActivity;

class RoleController extends Controller
{
    public function lists(Request $request){
        $user = User::where('token',$request->header("X-Auth-Token"))->first();
        $cek = check_auth($user,'read roles');
        if($cek['result']==1){
            $roles = Role::all();
            return response()->json([
                "message" => "success",
                "data" => $roles
            ],200);
        }else{
            return error_unauthorized();
        }
    }

    public function show(Request $request,$id){
        $user = User::where('token',$request->header("X-Auth-Token"))->first();
        $cek = check_auth($user,'read roles');
        if($cek['result']==1){
            $role = Role::find($id);
            if($role!=null){
                $permissions = $role->permissions()->get()->map(function($permission){
                    return $permission->name;
                })->toArray();

                return response()->json([
                    "message" => "success",
                    "data" => $role,
                    "permissions" => $permissions
                ],200);
            }else{
                return response()->json(["message" => "Role not exists!"],404);
            }
        }else{
            return error_unauthorized();
        }
    }

    public function show_menus(){
        $menus = config('menu');

        return response()->json([
            "menus" => $menus
        ],200);
    }

    public function store(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255|unique:roles',
            'permissions' => 'required'
        ]);

        if($validator->fails()){
            return response()->json(["status"=>"error","message" => implode(' ',$validator->errors()->all())], 422);
        }else {
            $user = User::where('token',$request->header("X-Auth-Token"))->first();
            $cek = check_auth($user,'create roles');
            if($cek['result']==1){
                $menu_collection = $this->collect_data($request->input('permissions'));
                $role = Role::create(["name" => $request->input("name")]);
                UserActivity::storeActivity(array(
                    "activity" => "add role for id: ".$role->id,
                    "user" =>$user->id,
                    "menu" =>"roles",
                    "ipaddress" => $request->ip()
                ));

                foreach($menu_collection as $k => $permissions){
                    foreach($permissions as $value){
                        $permission = Permission::where("name", $value)->get();

                        if($permission->isEmpty()){
                            Permission::create(["name" => $value]);
                        }

                        $role->givePermissionTo($value);
                    }
                }
            }else{
                return error_unauthorized();
            }
        }

        if($role){
            return response()->json([
                "status" => "success",
                "message" => "Role has been saved!",
                "data" => $role
            ],200);
        }else{
            return response()->json([
                "status" => "error",
                "message" => "Failed save role!",
                "data" => null
            ],403);
        }
    }

    public function update(Request $request){
        $validator = Validator::make($request->all(), [
            'id' => 'required',
            'permissions' => 'required'
        ]);

        if($validator->fails()){
            return response()->json(["status"=>"error","message" => implode(' ',$validator->errors()->all())], 422);
        }else {
            $user = User::where('token',$request->header("X-Auth-Token"))->first();
            $cek = check_auth($user,'update roles');
            if($cek['result']==1){
                UserActivity::storeActivity(array(
                    "activity" => "update role for id: ".$request->input('id'),
                    "user" =>$user->id,
                    "menu" =>"roles",
                    "ipaddress" => $request->ip()
                ));
                $role = Role::find($request->input('id'));

                $permissions = $role->permissions()->get()->map(function($permission){
                    return $permission->name;
                })->toArray();

                foreach($permissions as $permission){
                    $role->revokePermissionTo($permission);
                }

                $menu_collection = $this->collect_data($request->input('permissions'));

                foreach($menu_collection as $k => $permissions){
                    foreach($permissions as $value){
                        $permission = Permission::where("name", $value)->get();

                        if($permission->isEmpty()){
                            Permission::create(["name" => $value]);
                        }

                        $role->givePermissionTo($value);
                    }
                }
            }else{
                return error_unauthorized();
            }
        }

        if($role){
            return response()->json([
                "status" => "success",
                "message" => "Role has been update!",
                "data" => $role
            ],200);
        }else{
            return response()->json([
                "status" => "error",
                "message" => "Failed update role!",
                "data" => null
            ],403);
        }
    }

    function collect_data(array $input){
        $tmp = array();

        foreach ($input as $key => $val){
            $tmp[$key] = array("read ".$val["name"]);

            foreach($val['action'] as $permission){
                array_push($tmp[$key], $permission." ".$val["name"]);
            }
        }

        return $tmp;
    }

    public function destroy(Request $request,$id){
        $role = Role::where("id",$id)->get();

        if($role->isEmpty()){
            return response()->json(["message"=>"Role not exist"],404);
        }

        $user = User::where('token',$request->header("X-Auth-Token"))->first();
        $cek = check_auth($user,'destroy roles');
        if($cek['result']==1) {
            UserActivity::storeActivity(array(
                "activity" => "delete role for id: ".$id,
                "user" =>$user->id,
                "menu" =>"roles",
                "ipaddress" => $request->ip()
            ));
            if (Role::destroy($id)) {
                return response()->json([
                    "status" => "success",
                    "message"=>"Role has been deleted!"
                ],200);
            } else {
                return response()->json([
                    "status" => "error",
                    "message"=>"Failed delete role!"
                ],403);
            }
        }else{
            return error_unauthorized();
        }

    }

}