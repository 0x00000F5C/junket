<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\CustomerType;
use Validator;
use App\UserActivity;

class CustomerTypeController extends Controller
{

    /**
     * Customer Types List
     * GET /api/customer/get_customer_types
     *
     * @return Response
     **/
    public function lists(){
        $types = CustomerType::all();
        return response()->json([
            "message" => "success",
            "data" => $types
        ],200);
    }

    public function show(Request $request,$id){
        $user = User::where('token',$request->header("X-Auth-Token"))->first();
        $cek = check_auth($user,'read customer_types');
        if($cek['result']==1){
            $type = CustomerType::find($id);
            if($type!=null){
                return response()->json([
                    "message" => "success",
                    "data" => $type
                ],200);
            }else{
                return response()->json(["message" => "Customer type not exists!"],404);
            }
        }else{
            return error_unauthorized();
        }

    }

    public function store(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:50|unique:customer_types'
        ]);

        if($validator->fails()){
            return response()->json(["status"=>"error","message" => implode(' ',$validator->errors()->all())], 422);
        }else {
            $user = User::where('token',$request->header("X-Auth-Token"))->first();
            $cek = check_auth($user,'create customer_types');
            if($cek['result']==1){
                $type = CustomerType::create(["name" => $request->input("name")]);
                UserActivity::storeActivity(array(
                    "activity" => "add customer type for id: ".$type->id,
                    "user" =>$user->id,
                    "menu" =>"customer_types",
                    "ipaddress" => $request->ip()
                ));
            }else{
                return error_unauthorized();
            }
        }

        if($type){
            return response()->json([
                "status" => "success",
                "message" => "Customer type has been saved!",
                "data" => $type
            ],200);
        }else{
            return response()->json([
                "status" => "error",
                "message" => "Failed save customer type!",
                "data" => null
            ],403);
        }
    }

    public function update(Request $request){
        $validator = Validator::make($request->all(), [
            'id' => 'required',
            'name' => 'required|max:50|unique:customer_types,name,'.$request->input('id')
        ]);

        if($validator->fails()){
            return response()->json(["status"=>"error","message" => implode(' ',$validator->errors()->all())], 422);
        }else {
            $user = User::where('token',$request->header("X-Auth-Token"))->first();
            $cek = check_auth($user,'update customer_types');
            if($cek['result']==1){
                $type = CustomerType::find($request->input("id"));
                $type->name = $request->input("name");
                $type->save();
                UserActivity::storeActivity(array(
                    "activity" => "update customer type for id: ".$type->id,
                    "user" =>$user->id,
                    "menu" =>"customer_types",
                    "ipaddress" => $request->ip()
                ));
            }else{
                return error_unauthorized();
            }
        }

        if($type){
            return response()->json([
                "status" => "success",
                "message" => "Customer type has been update!",
                "data" => $type
            ],200);
        }else{
            return response()->json([
                "status" => "error",
                "message" => "Failed update customer type!",
                "data" => null
            ],403);
        }
    }

    public function destroy(Request $request,$id){
        $type = CustomerType::find($id);

        if($type == null){
            return response()->json(["message"=>"Customer type not exist"],404);
        }

        $user = User::where('token',$request->header("X-Auth-Token"))->first();
        $cek = check_auth($user,'delete customer_types');
        if($cek['result']==1) {
            UserActivity::storeActivity(array(
                "activity" => "delete customer type for id: ".$id,
                "user" =>$user->id,
                "menu" =>"customer_types",
                "ipaddress" => $request->ip()
            ));
            if (CustomerType::destroy($id)) {
                return response()->json([
                    "status" => "success",
                    "message" => "Customer type has been deleted!"
                ], 200);
            } else {
                return response()->json([
                    "status" => "error",
                    "message" => "Failed delete customer type!"
                ], 403);
            }
        }else{
            return error_unauthorized();
        }

    }

}
