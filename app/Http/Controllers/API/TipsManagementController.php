<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\TipsManagement;
use App\Customer;
use Validator;
use App\UserActivity;
use App\AccountConfig;
use App\Account;
use App\User;
use App\JournalAccount;
use App\JournalAccountDetail;
use DB;

class TipsManagementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $date = $request->input('date');
        $sort_by = $request->input('sort_by');
        $sort_type = $request->input('sort_type');
        $column = "trans_date";
        if($sort_by == "name"){
            $column = "users.name";
        }else if($sort_by == "trans_date"){
            $column = "tipsmanagement.trans_date";
        }else if($sort_by == "amount"){
            $column = "tipsmanagement.amount";
        }

        $tips = TipsManagement::select('tipsmanagement.*')
            ->join('users', 'users.id', '=', 'tipsmanagement.employee_id')
			->whereDate('trans_date','=',$date)
            ->orderBy($column, $sort_type)
			->paginate(10);

        return response()->json(transformCollection($tips), 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'employee_id' => 'required',
            'trans_date' => 'required|date',
        ]);

        if($validator->fails()){
            return response()->json(["status"=>"error","message" => implode(' ',$validator->errors()->all())], 422);
        }else{
            $data = $request->all();
            $log = TipsManagement::create($data);
			$this->journal($log->id,$request);
        }

        if($log){
            return response()->json([
                "status" => "success",
                "message" => "Tips Management has been saved!",
                "data" => $log
            ],200);
        }else{
            return response()->json([
                "status" => "error",
                "message" => "Failed save Tips Management!",
                "data" => null
            ],403);
        }

    }

    /**
     * Journal Transaction.
     *
     * @param  int  $id
     * @param  object  $request from Request Class
     * @return boolean
     */
	public function journal($id,$request){
		$user = User::where('token',$request->header("X-Auth-Token"))->first();
		$data = TipsManagement::find($id);
		$account_config = AccountConfig::find(1);
		
		$remark='From Tips Management';
		if(isset($data['customer_id']) && $data['customer_id']!=0){
			$customer=Customer::find($data['customer_id']);
			$remark='Tips From '.$customer->first_name.' '.$customer->last_name;
		}
		
		DB::beginTransaction();
		
		try{
			$journal=JournalAccount::where('activity','=','tips')->where('activity_id','=',$id)->first();
			$number=getAutoNumber('je_num','journal_accounts','JE-'.date('mY')."-",4);
			$data_journal=[
				'je_num'=>$number,
				'date'=>$data['trans_date'],
				'user_id'=>$user['id'],
				'entity'=>'user',
				'entity_id'=>$data['employee_id'],
				'activity'=>'tips',
				'activity_id'=>$id,
				'remark'=>$remark
			];
			
			if(count($journal)==0){
				$journal=JournalAccount::create($data_journal);
			}else{
				$data_journal['je_num']=$journal->je_num;
				$journal->update($data_journal);
				JournalAccountDetail::where('journal_id','=',$journal->id)->delete();
			}
			
			if($journal){
				$data['amount']=isset($data['amount'])?$data['amount']:0;
				JournalAccountDetail::create([
					'journal_id'=>$journal->id,
					'account_id'=>$account_config['cash_real'],//cash
					'debit'=>$data['amount'],
					'credit'=>0,
					'entity'=>'user',
					'entity_id'=>$data['employee_id']
				]);
				
				$account_tips=Account::find($account_config['tips']);
				
				JournalAccountDetail::create([
					'journal_id'=>$journal->id,
					'account_id'=>$account_config['tips'],//tips
					'debit'=>0,
					'credit'=>$account_tips['nature_type']=='credit'?$data['amount']:-$data['amount'],
					'entity'=>'user',
					'entity_id'=>$data['employee_id']
				]);
				
				if($account_tips['nature_type']=='debit'){
					JournalAccountDetail::create([
						'journal_id'=>$journal->id,
						'account_id'=>$account_config['open_balance_asset'],//tips
						'debit'=>0,
						'credit'=>$data['amount']+$data['amount'],
						'entity'=>'user',
						'entity_id'=>$data['employee_id']
					]);
				}
			}
			
			DB::commit();
		}catch(\Exception $exc){
			// echo $exc;
			DB::rollback();
		}
			
		return true;
	}

    /**
     * Index Purchase List
     * GET /api/purchases/{id}
     *
     * @param string $token     The token for authentication
     * @return Response
     **/
    public function show(Request $request, $id)
    {
        $purchase = TipsManagement::find($id);
        if($purchase!=null){
            return response()->json([
                "message" => "success",
                "data" => $purchase
            ],200);
        }else{
            return response()->json(["message" => "Tips Employee not exists!"],404);
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'employee_id' => 'required',
            'trans_date' => 'required|date',
        ]);

        if($validator->fails()){
            return response()->json(["status"=>"error","message" => implode(' ',$validator->errors()->all())], 422);
        }else{
            $data = $request->all();
            $log = TipsManagement::find($id);
            if($log == null){
                return response()->json(["message" => "Data not found"], 404);
            }else{
                $log = $log->update($data);
				$this->journal($id,$request);
            }
        }

        if($log){
            return response()->json([
                "status" => "success",
                "message" => "Tips Management has been update!",
                "data" => $log
            ],200);
        }else{
            return response()->json([
                "status" => "error",
                "message" => "Failed update Tips Management!",
                "data" => null
            ],403);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $log = TipsManagement::find($id);
        if($log == null){
            return response()->json(["message" => "Data not found"], 404);
        }else{
            if ($log->delete()) {
				JournalAccount::where('activity','=','tips')->where('activity_id','=',$id)->delete();
                return response()->json([
                    "status" => "success",
                    "message" => "Tips Management has been deleted!"
                ], 200);
            } else {
                return response()->json([
                    "status" => "error",
                    "message" => "Failed delete Tips Management!"
                ], 403);
            }
        }
    }
	
	public function checkTips(Request $request) {
		$test = TipsManagement::where([["trans_id", $request->input("t")], ["employee_id", $request->input("e")]])->get();
		if(count($test) > 0) {
			return response()->json(["status" => false, "goTo" => $test[0]->id]);
		} else {
			return response()->json(["status" => true]);
		}
	}
}
