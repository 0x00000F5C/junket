<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Transaction;
use App\RollingTable;
use App\User;
use Validator;
use App\UserActivity;
use DB;

class RollingTableController extends Controller
{
    /**
     * Display a listing of the resource.
     * GET /api/rolling_tables/
     * @param string $token
     * The token for authentication
     *
     * @return Response
     */
    public function index(Request $request)
    {

        $transid = $request->input('transid');
        $rolltable = DB::table('rolling_tables')
                ->select(DB::raw('id, transaction_id, print_date,
                trade_date, terminal, cash_chip, nn_chip, cash_chip+nn_chip as roll_chip_turnover,0.012*cash_chip as cash_chip_rebate, 0.012*nn_chip as nn_chip_rebate,(cash_chip+nn_chip)*0.012 as roll_chip_rebate'))
                ->where('transaction_id','=',$transid)
                ->orderBy('id', 'desc')
                ->paginate(10);

        return response()->json(['data' => $rolltable], 200);
    }

    public function store(Request $request , $id)
    {
        $validator = Validator::make($request->all(), [
            'cash_chip' => 'required|numeric',
            'nn_chip' => 'required|numeric',
            'terminal' => 'required',
            'transaction_id' => 'required|numeric'
            ]);

        if($validator->fails()){
            return response()->json(["status"=>"error","message" => implode(' ',$validator->errors()->all())], 422);
        }else{
            $user = User::where('token',$request->header("X-Auth-Token"))->first();
            $cek = check_auth($user,'create transactions');
            if($cek['result']==1){
                $transaction = Transaction::find($id);
                if($transaction==null){
                    return response()->json(["message" => "Transaction not exists!"],404);
                }else{
                    $data = $request->all();
                    $log = RollingTable::create($data);
                    UserActivity::storeActivity(array(
                        "activity" => "add transactions rolling 2 for id: ".$log->id,
                        "user" => $user->id,
                        "menu" => "transactions",
                        "ipaddress" => $request->ip()
                    ));
                }
            }else{
                return error_unauthorized();
            }

            if($log){
                return response()->json([
                    "status" => "success",
                    "message" => "Transaction rolling 2 has been saved!",
                    "data" => $log
                ],200);
            }else{
                return response()->json([
                    "status" => "error",
                    "message" => "Failed save transaction rolling 2!",
                    "data" => null
                ],403);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
 /*       $user = User::where('token',$request->header("X-Auth-Token"))->first();
        $cek = check_auth($user,'read table_rolling');

        if($cek['result']==1){*/
        $rolltable = DB::table('rolling_tables')
            ->select(DB::raw('id, transaction_id, print_date,
                trade_date, terminal, cash_chip, nn_chip, cash_chip+nn_chip as roll_chip_turnover,0.012*cash_chip as cash_chip_rebate, 0.012*nn_chip as nn_chip_rebate,(cash_chip+nn_chip)*0.012 as roll_chip_rebate'))
                ->where('id','=',$id)
                ->first();
            if($rolltable){
                return response()->json(['data' => $rolltable], 200);
            }else{
                return response()->json(['data' => null], 404);
            }
        /*}else{
            return error_unauthorized();
        };*/
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'trade_date' => 'required',
            'terminal' => 'required',
            'cash_chip' => 'required',
            'nn_chip' => 'required'
        ]);

        if($validator->fails()){
            return response()->json(["status"=>"error","message" => implode(' ',$validator->errors()->all())], 422);
        }

        $data = $request->all();
        $method = RollingTable::find($id);

        if($method){
            $method->update($data);
            $user = User::where('token',$request->header("X-Auth-Token"))->first();
            UserActivity::storeActivity(array(
                "activity" => "Modified table rolling for customer:".$method->customer_name,
                "user" => $user->id,
                "menu" => "table rolling",
                "ipaddress" => $request->ip()
            ));

            return response()->json([
                "status" => "success",
                "message" => " table rolling has been updated!",
                "data" => $method
            ], 200);
        }else{
            return response()->json(['message' => 'table rolling Type not found'], 404);
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $rolltable = RollingTable::find($id);

        if($rolltable == null){
            return response()->json(["message"=>"rolling table not exist"],404);
        }

        $user = User::where('token',$request->header("X-Auth-Token"))->first();
        /*        $cek = check_auth($user,'delete table_rolling');
                if($cek['result']==1) {*/
            UserActivity::storeActivity(array(
                "activity" => "delete table rolling for customer: ".$rolltable->customer_name,
                "user" =>$user->id,
                "menu" =>"table rolling",
                "ipaddress" => $request->ip()
            ));
            if (RollingTable::destroy($id)) {
                return response()->json([
                    "status" => "success",
                    "message" => "Table rolling has been deleted!"
                ], 200);
            } else {
                return response()->json([
                    "status" => "error",
                    "message" => "Failed delete Table rolling!"
                ], 403);
            }
/*        }else{
            return error_unauthorized();
        }*/
    }
}
