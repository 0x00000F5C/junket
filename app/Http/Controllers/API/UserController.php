<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use Spatie\Permission\Models\Role;
use Validator;
use App\UserActivity;
use DB;

class UserController extends Controller
{
    public function lists(Request $request){
        $users = User::all();
        return response()->json([
            "message" => "success",
            "data" => $users
        ],200);
    }

    public function show(Request $request,$id){
        $user = User::where('token',$request->header("X-Auth-Token"))->first();
        $cek = check_auth($user,'read users');
        if($cek['result']==1){
            UserActivity::storeActivity(array(
                "activity" => "show user",
                "user" =>$user->id,
                "menu" =>"users",
                "ipaddress" => $request->ip()
            ));
            $user = User::find($id);
            if($user!=null){
                return response()->json([
                    "message" => "success",
                    "data" => [
                        "id" => $user->id,
                        "username" => $user->username,
                        "name" => $user->name,
                        "email" => $user->email,
                        "roles" => $user->roles()->first()->name
                    ]
                ],200);
            }else{
                return response()->json(["message" => "User not exists!"],404);
            }
        }else{
            return error_unauthorized();
        }
    }

    public function find_by_role($id){
        $user_by_role = Role::find($id);
        return response()->json([
            "message" => "success",
            "data" => $user_by_role->users
        ],200);
    }

    public function employee_role(Request $request){
        $user_by_role = Role::find([4, 5, 6]);
		$usrs = $request->input("id") ? User::where("id", $request->input("id"))->get() : User::all();
        foreach ($usrs as $value) {
            if ($value->hasAnyRole($user_by_role)) {
                $users[] = $value;
            }
        }
        return response()->json([
            "message" => "success",
            "data" => @$users
        ],200);
    }
    
    public function store(Request $request){
        $validator = Validator::make($request->all(), [
            'username' => 'required|max:50|unique:users',
            'name' => 'required|max:255',
            'email' => 'email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
            'role' => 'required'
        ]);

        if($validator->fails()){
            return response()->json(["status"=>"error","message" => implode(' ',$validator->errors()->all())], 422);
        }else{
            $cek_roles = Role::where('name',$request->input('role'))->first();
            if($cek_roles==null){
                return response()->json(["errors" => "role not exists!"], 404);
            }

            $data = $request->all();
            $user = User::where('token',$request->header("X-Auth-Token"))->first();
            $cek = check_auth($user,'create users');
            if($cek['result']==1){
                $log = User::create([
                    'username' => $data['username'],
                    'name' => $data['name'],
                    'email' => $data['email'],
                    'password' => bcrypt($data['password']),
                ]);
                UserActivity::storeActivity(array(
                    "activity" => "add user for id: ".$log->id,
                    "user" => $user->id,
                    "menu" => "users",
                    "ipaddress" => $request->ip()
                ));
                $log->assignRole($data['role']);
            }else{
                return error_unauthorized();
            }
        }

        if($log){
            return response()->json([
                "status" => "success",
                "message" => "User has been saved!",
                "data" => $log
            ],200);
        }else{
            return response()->json([
                "status" => "error",
                "message" => "Failed save role!",
                "data" => null
            ],403);
        }

    }

    public function update(Request $request){
        $validator = Validator::make($request->all(), [
            'id' => 'required',
            'name' => 'required|max:255',
            'password' => 'min:6|confirmed',
            'email' => 'email|max:255|unique:users,email,'.$request->input('id'),
            'role' => 'required'
        ]);

        if($validator->fails()){
            return response()->json(["status"=>"error","message" => implode(' ',$validator->errors()->all())], 422);
        }else{
            $cek_roles = Role::where('name',$request->input('role'))->first();
            if($cek_roles==null){
                return response()->json(["errors" => "role not exists!"], 404);
            }
            
            $data = $request->all();
            $user_login = User::where('token',$request->header("X-Auth-Token"))->first();
			if($data['id']!=$user_login->id && $user_login->roles[0]->name!='Administrator'){
				return response()->json(["status"=>"error","message" => "Unauthorized!"], 422);
			}
			
            $cek = check_auth($user_login,'update users');
			
			$user = User::find($data['id']);
            if($cek['result']==1){
                UserActivity::storeActivity(array(
                    "activity" => "update user for id: ".$data['id'],
                    "user" => $user_login->id,
                    "menu" => "users",
                    "ipaddress" => $request->ip()
                ));
				if(isset($data['password'])){
					$data['password']=bcrypt($data['password']);
				}
                if($user!=null){
                    $user->update($data);

                    $role = $user->roles()->first()->name;
                }else{
                    return response()->json([
                        "status" => "error",
                        "message" => "User not exists!"
                    ],404);
                }
                if($role!=$data['role']){
                    $user->removeRole($role);
                    $user->assignRole($data['role']);
                }

            }else{
                return error_unauthorized();
            }

            return response()->json([
                "status" => "success",
                "message" => "User has been updated!",
                "data" => $user
            ],200);
        }
    }

    public function destroy(Request $request,$id){
        $user = User::find($id);

        if($user == null){
            return response()->json(["message"=>"User not exist"],404);
        }
        $sender = User::where('token',$request->header("X-Auth-Token"))->first();
        $cek = check_auth($sender,'delete users');
        if($cek['result']==1){
            UserActivity::storeActivity(array(
                "activity" => "delete user",
                "user" =>$user->id,
                "menu" =>"users",
                "ipaddress" => $request->ip()
            ));

            if(User::destroy($id)){
                return response()->json([
                    "status" => "success",
                    "message"=>"User has been deleted!"
                ],200);
            }else{
                return response()->json([
                    "status" => "error",
                    "message"=>"Failed delete user!"
                ],403);
            }
        }else{
            return error_unauthorized();
        }

    }

    public function resetPassword(Request $request)
    {
        $user = User::where('token',$request->header("X-Auth-Token"))->first();
        if($user->roles[0]->id != 1){
            return response()->json(['errors' => "Only Administrator can reset password!"], 403);
        }

        $validator = Validator::make($request->all(), [
            'username' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(["status"=>"error","message" => implode(' ',$validator->errors()->all())], 422);
        }

        $user = User::where('username',$request->input('username'))->first();
        if ($user == null) {
            return response()->json(["message" => "User not exist!"],404);
        }else{
            $user->update(['password' => bcrypt('123456')]);
            return response()->json([
                'status' => 'success',
                'message' => 'password for '.$request->input('username').' has been reset to 123456!'
            ]);
        }
    }

    public function changePassword(Request $request, $id)
    {
        $data = $request->all();
        $validator = Validator::make($request->all(), [
            'username' => 'required',
            'old_password' => 'required',
            'password' => 'required|min:6|confirmed',
        ]);

        if ($validator->fails()) {
            return response()->json(["status"=>"error","message" => implode(' ',$validator->errors()->all())], 422);
        }

        $user = User::where('id', $id)
            ->where('username', $data['username'])
            ->first();

        if($user == null){
            return response()->json(["message" => "User not found!"], 404);
        }else{
            if($user->token != $request->header('x-auth-token')) {
                return response()->json(["errors" => "User not match!"], 403);
            }
            if(!password_verify($data['old_password'], $user->password)){
                return response()->json([
                    "status" => "error",
                    "message" => "The username and password you entered don't match."
                ], 403);
            }else{
                $log = $user->update([
                    'password' => bcrypt($data['password']),
                    "token" => null,
                    "expired_at" => null
                ]);
            }
        }


        if($log){
            return response()->json([
                "status" => "success",
                "message" => "Password has been changed, Please re-login!",
            ],200);
        }else{
            return response()->json([
                "status" => "error",
                "message" => "Failed change password!",
                "data" => null
            ],403);
        }
    }

    public function checkPermission(Request $request){
        $action = $request->input('action');
        $menu = $request->input('menu');

        $user = User::where('token',$request->header("X-Auth-Token"))->first();
        $permission = $user->can($action." ".$menu);
        return response()->json([
            'permission' => $permission
        ], 200);
    }
	
	public function import(Request $request){
		$user = User::where('token',$request->header("X-Auth-Token"))->first();
		$cek = check_auth($user,'create users');
		if($cek['result']==0){
			return error_unauthorized();
		}
		
		if(!$_FILES['upload']){
			return response()->json([
				'status'=>'error',
				'message'=>'Please choose file!'
			],403);
		}
		
		$upload = $_FILES['upload'];
		$allowed =  array('xls','xlsx','csv');
		$filename = $_FILES['upload']['name'];
		$ext = pathinfo($filename, PATHINFO_EXTENSION);
		if(!in_array(strtolower($ext),$allowed) ) {
			return response()->json([
				'status'=>'error',
				'message'=>'Please choose file .csv, .xlsx or .xls'
			],403);
		}
		
		DB::beginTransaction();
		try{
			$row = 1;
			if (($handle = fopen($upload['tmp_name'], "r")) !== FALSE) {
				while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
					if($row==1){
						$row++;
						continue;
					}
					
					$input['name']=$data[1];
					$input['email']=$data[2];
					$input['password']=$data[3];
					$input['username']=$data[4];
					$input['role']=$data[5];
					
					$validator = Validator::make($input, [
						'username' => 'required|max:50|unique:users',
						'name' => 'required|max:255',
						'email' => 'email|max:255|unique:users',
						'password' => 'required|min:6',
						'role' => 'required'
					]);
					
					if ($validator->fails()) {
						DB::rollback();
						
						return response()->json([
							'status'=>'error',
							'message'=> implode(' ',$validator->errors()->all()).' ('.$input['username'].','.$input['email'].')'
						], 422);
					}
					
					$log = User::create([
						'username' => $input['username'],
						'name' => $input['name'],
						'email' => $input['email'],
						'password' => bcrypt($input['password'])
					]);
					$log->assignRole($input['role']);
					
					$row++;
				}
				fclose($handle);

				UserActivity::storeActivity(array(
					"activity" => "import user",
					"user" =>$user->id,
					"menu" =>"users",
					"ipaddress" => $request->ip()
				));

				DB::commit();
				
				return response()->json([
					'status'=>'success',
					'message'=>'Data Imported!'
				]);
			}
		}catch(\Exception $exc){
			DB::rollback();
			
			return response()->json([
				'status'=>'error',
				'message'=>$exc
			]);
		}
	}

}
