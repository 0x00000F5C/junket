<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Agent;
use App\User;
use Validator;
use DB;
use App\UserActivity;

class AgentController extends Controller
{
	
    /**
     * Agent List
     * GET /api/agent
     *
     * @param string $token | The token for authentication
     * @return Response
     **/
    public function Index(){
        $agents = Agent::all();
        return response()->json([
            'message' => 'success',
            'data' => $agents
        ],200);
    }
	
    /**
     * Store Agent
     * POST /api/agent
     *
     * @param string $token | The token for authentication
     * @param array $agent_data | Data input agent
     * @return Response
     **/
    public function store(Request $request){
        $validator = Validator::make($request->all(), [
            'id_no' => 'required|unique:agents,deleted_at,null',
            'name' => 'required|unique:agents,deleted_at,null',
            'phone_code' => 'required',
            'contact' => 'required'
        ]);

        if($validator->fails()){
            return response()->json([
				'status' => 'error',
				'message' => implode(' ',$validator->errors()->all())
				], 422);
        }
		
		$data = $request->all();
		$user = User::where('token',$request->header('X-Auth-Token'))->first();
		$cek = check_auth($user,'create agents');
		if($cek['result']!=1){
			return error_unauthorized();
		}
		
		$agent = Agent::create($data);
		UserActivity::storeActivity(array(
			'activity' => 'add agent for id: '.$agent->id,
			'user' => $user->id,
			'menu' => 'agents',
			'ipaddress' => $request->ip()
		));
		
        if($agent){
            return response()->json([
                'status' => 'success',
                'message' => 'Agent has been saved!',
                'data' => $agent
            ],200);
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'Failed save agent!',
                'data' => null
            ],403);
        }
    }

	/**
     * All Agent
     * GET /api/agent/all
     *
     * @param string $token | The token for authentication
     * @param string $search | Searching value
     * @param string $sort_by | Sorting field
     * @param string $sort_type | Sorting asc, desc
     * @return Response
     **/
    public function all(Request $request){
        $user = User::where('token',$request->header("X-Auth-Token"))->first();
        $cek = check_auth($user,'read agents');
        if($cek['result']!=1){
            return error_unauthorized();
		}
	
		$search = $request->input('search');
		$sort_by = $request->input('sort_by');
		$sort_type = $request->input('sort_type');
		
		$agent = Agent::select('agents.*')
			->join('agent_companies','agent_companies.id','=','agents.agent_company_id','left');
		
		if($search){
			$agent = $agent->where('id_no','LIKE','%'.$search.'%')
				->orWhere('name','LIKE','%'.$search.'%')
				->orWhere(DB::raw('CONCAT(phone_code," ",contact)'),'LIKE','%'.$search.'%')
				->orWhere('email','LIKE','%'.$search.'%')
				->orWhere('agent_companies.company_name','LIKE','%'.$search.'%');
		}
		
		if(!$sort_by){
			$sort_by='agents.created_at';
		}
		$sort_type=$sort_type?$sort_type:'desc';
		
		switch($sort_by){
			case 'company_name':
				$sort_by='agent_companies.name';
				break;
		}
			
		$agent = $agent->orderBy($sort_by,$sort_type)->paginate(10);

        return response()->json(transformCollection($agent), 200);
    }
	
    /**
     * Get Agent by ID
     * GET /api/agent/{agent_id}
     *
     * @param string $token | The token for authentication
     * @return Response
     **/
    public function show(Request $request,$id)
    {
        $user = User::where('token',$request->header("X-Auth-Token"))->first();
        $cek = check_auth($user,'read agents');
        if($cek['result']!=1){
            return error_unauthorized();
		}
		
		$agent = Agent::find($id);
            
		if(!$agent){
			return response()->json([
				'status' => 'error',
				'message' => 'Agent not exists!'
			],404);
		}
		
		return response()->json([
			'status' => 'success',
			'data' => $agent
		],200);
    }

    /**
     * Update Customer
     * PUT /api/customer/{agent_id}
     *
     * @param string $token | The token for authentication
     * @param integer $agent_id | Agent id
     * @param array $agent_data | Data update agent
     * @return Response
     **/
    public function update(Request $request,$id)
    {
        $validator = Validator::make($request->all(), [
            'id_no' => 'required|unique:agents,id_no,'.$request->input('id').',id,deleted_at,NULL',
            'name' => 'required|unique:agents,name,'.$request->input('id').',id,deleted_at,NULL',
            'phone_code' => 'required',
            'contact' => 'required'
        ]);

         if($validator->fails()){
            return response()->json([
				'status' => 'error',
				'message' => implode(' ',$validator->errors()->all())
				], 422);
        }
		
		$data = $request->all();
		$user = User::where('token',$request->header('X-Auth-Token'))->first();
		$cek = check_auth($user,'update agents');
		if($cek['result']!=1){
			return error_unauthorized();
		}
		
		$agent = Agent::find($id);
		if(!$agent){
			return response()->json([
				'status' => 'error',
				'message' => 'Agent not exists!'
			],404);
		}
		
		$agent->update($data);
		UserActivity::storeActivity(array(
			'activity' => 'update agent for id: '.$agent->id,
			'user' => $user->id,
			'menu' => 'agents',
			'ipaddress' => $request->ip()
		));
		
        if($agent){
            return response()->json([
                'status' => 'success',
                'message' => 'Agent has been updated!',
                'data' => $agent
            ],200);
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'Failed update agent!',
                'data' => null
            ],403);
        }
    }

    /**
     * Delete Agent
     * DELETE /api/agent/{agent_id}
     *
     * @param string $token | The token for authentication
     * @param integer $agent_id | Agent id
     * @return Response
     **/
    public function destroy(Request $request,$id)
    {
        $agent = Agent::find($id);

        if(!$agent){
            return response()->json([
				'status'=>'error',
				'message'=>'Agent not exist'
			],404);
        }

        $user = User::where('token',$request->header("X-Auth-Token"))->first();
        $cek = check_auth($user,'delete agents');
        if($cek['result']!=1) {
            return error_unauthorized();
        }
		
		$agent=Agent::destroy($id);
		UserActivity::storeActivity(array(
			'activity' => 'delete agent for id: '.$id,
			'user' =>$user->id,
			'menu' =>'agents',
			'ipaddress' => $request->ip()
		));
		
		if($agent) {
			return response()->json([
				'status' => 'success',
				'message' => 'Agent has been deleted!'
			], 200);
		} else {
			return response()->json([
				'status' => 'error',
				'message' => 'Agent delete agent!'
			], 403);
		}
    }

}
