<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Transaction extends Model
{
    use SoftDeletes;

    protected $fillable = [
        "date",
        "transaction_no",
        "customer_id",
        "user_id",
        "nn_chips",
        "shift",
        "remarks",
        "commission",
        "status",
        "closing_cash_chips",
        "closing_nn_chips",
        "closing_cash",
        "check_in_commision",
		"room",
		"type_chip",
		"win_only",
		"assign_from_old",
		"assign_from",
		"assign_to",
		"scenario",
		"closing_by",
		"closing_date"
    ];

    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];

    protected $dates = ['deleted_at'];
	
	protected $boolean = ['win_only'];

    protected $with = ['user','customer','details','rollings','expenses'];

    public function user(){
        return $this->belongsTo(User::class)->withTrashed();
    }

    public function customer(){
        return $this->belongsTo(Customer::class)->withTrashed();
    }

    public function details(){
        return $this->hasMany(TransactionDetail::class);
    }

    public function rollings(){
        return $this->hasMany(TransactionRolling::class);
    }

    public function expenses(){
        return $this->hasMany(TransactionExpense::class);
    }

    public function cust_debt(){
        return $this->hasOne(CustomerDebt::class);
    }

    public function rolling_table(){
        return $this->hasMany(RollingTable::class);
    }
	
	public static function journalOpenTrip($id,$request){
		$user = User::where('token',$request->header("X-Auth-Token"))->first();
		$data = Transaction::find($id);
		
		DB::beginTransaction();
		
		try{
			$uha=UserHasAccount::where('user_id','=',$data['user_id'])->first();
			$account_config=AccountConfig::first();
			$journal=JournalAccount::where('activity','=','open_trip')->where('activity_id','=',$id)->first();
			$number=getAutoNumber('je_num','journal_accounts','JE-'.date('mY')."-",4);
			$data_journal=[
				'je_num'=>$number,
				'date'=>date('Y-m-d',strtotime($data['created_at'])),
				'user_id'=>$user['id'],
				'entity'=>'customer',
				'entity_id'=>$data['customer_id'],
				'activity'=>'open_trip',
				'activity_id'=>$id,
				'remark'=>'Open Trip ('.$data['customer']['first_name'].' '.$data['customer']['last_name'].')'
			];
			
			if(count($journal)==0){
				$journal=JournalAccount::create($data_journal);
			}else{
				$data_journal['je_num']=$journal->je_num;
				$journal->update($data_journal);
				JournalAccountDetail::where('journal_id','=',$journal->id)->delete();
			}
			
			if($journal){
				$data['nn_chips']=isset($data['nn_chips'])?$data['nn_chips']:0;
				JournalAccountDetail::create([
					'journal_id'=>$journal->id,
					'account_id'=>isset($uha['account_id'])?$uha['account_id']:$account_config['receivable'],//ar
					'debit'=>$data['nn_chips'],
					'credit'=>0,
					'entity'=>'customer',
					'entity_id'=>$data['customer_id']
				]);
				
				JournalAccountDetail::create([
					'journal_id'=>$journal->id,
					'account_id'=>$account_config['nn_chips'],//nn chip
					'debit'=>0,
					'credit'=>$data['nn_chips'],
					'entity'=>'user',
					'entity_id'=>$data['user_id']
				]);
			}
			
			DB::commit();
		}catch(\Exception $exc){
			// echo $exc;
			DB::rollback();
		}
			
		return true;
	}
	
	public static function debtTrip($id){
		$transaction=DB::table(DB::raw('(select t.*,t.nn_chips+sum(ifnull(td.rolling,0))as debt
			from transactions t
			left join transaction_details td on td.transaction_id=t.id and td.flags="credit"
			where t.deleted_at is null and t.id='.$id.'
			)as sub'))->first();
		$data_debt['transaction_id']=$id;
		$data_debt['date']=$transaction->date;
		$data_debt['due_date']=$transaction->date;
		$data_debt['customer_id']=$transaction->customer_id;
		$data_debt['staff_id']=$transaction->user_id;
		$data_debt['amount']=$transaction->debt;
		$data_debt['amount_due']=$transaction->debt;
		$debt=CustomerDebt::where(['transaction_id'=>$id])->first();
		if(count($debt)==0){
			CustomerDebt::create($data_debt);
		}else{
			CustomerDebt::where('transaction_id','=',$id)->update($data_debt);
		}
		
	}
	
	public static function journalCredit($id,$request){
		$user = User::where('token',$request->header("X-Auth-Token"))->first();
		$data = TransactionDetail::select(['transaction_details.*','transactions.customer_id','customers.first_name','customers.last_name'])
			->join('transactions','transactions.id','=','transaction_details.transaction_id')
			->join('customers','customers.id','=','transactions.customer_id')
			->where('transaction_details.id','=',$id)
			->first();
		
		DB::beginTransaction();
		
		try{
			$uha=UserHasAccount::where('user_id','=',$data['user_id'])->first();
			$account_config=AccountConfig::first();
			$journal=JournalAccount::where('activity','=','credit_trip')->where('activity_id','=',$id)->first();
			$number=getAutoNumber('je_num','journal_accounts','JE-'.date('mY')."-",4);
			$data_journal=[
				'je_num'=>$number,
				'date'=>date('Y-m-d',strtotime($data['created_at'])),
				'user_id'=>$user['id'],
				'entity'=>'customer',
				'entity_id'=>$data['customer_id'],
				'activity'=>'credit_trip',
				'activity_id'=>$id,
				'remark'=>'Credit Trip ('.$data['first_name'].' '.$data['last_name'].')'
			];
			
			if(count($journal)==0){
				$journal=JournalAccount::create($data_journal);
			}else{
				$data_journal['je_num']=$journal->je_num;
				$journal->update($data_journal);
				JournalAccountDetail::where('journal_id','=',$journal->id)->delete();
			}
			
			if($journal){
				$data['rolling']=isset($data['rolling'])?$data['rolling']:0;
				JournalAccountDetail::create([
					'journal_id'=>$journal->id,
					'account_id'=>isset($uha['account_id'])?$uha['account_id']:$account_config['receivable'],//ar
					'debit'=>$data['rolling'],
					'credit'=>0,
					'entity'=>'customer',
					'entity_id'=>$data['customer_id']
				]);
				
				JournalAccountDetail::create([
					'journal_id'=>$journal->id,
					'account_id'=>$account_config['nn_chips'],//nn chip
					'debit'=>0,
					'credit'=>$data['rolling'],
					'entity'=>'user',
					'entity_id'=>$data['user_id']
				]);
			}
			
			DB::commit();
		}catch(\Exception $exc){
			// echo $exc;
			DB::rollback();
		}
			
		return true;
	}
	
	public static function journalClosing($id,$request){
		$user = User::where('token',$request->header("X-Auth-Token"))->first();
		$data = Transaction::find($id);
		
		DB::beginTransaction();
		
		try{
			$uha=UserHasAccount::where('user_id','=',$data['user_id'])->first();
			$account_config=AccountConfig::first();
			$journal=JournalAccount::where('activity','=','closing_trip')->where('activity_id','=',$id)->first();
			$number=getAutoNumber('je_num','journal_accounts','JE-'.date('mY')."-",4);
			$data_journal=[
				'je_num'=>$number,
				'date'=>date('Y-m-d',strtotime($data['closing_date'])),
				'user_id'=>$user['id'],
				'entity'=>'customer',
				'entity_id'=>$data['customer_id'],
				'activity'=>'closing_trip',
				'activity_id'=>$id,
				'remark'=>'Closing Trip ('.$data['customer']['first_name'].' '.$data['customer']['last_name'].')'
			];
			
			if(count($journal)==0){
				$journal=JournalAccount::create($data_journal);
			}else{
				$data_journal['je_num']=$journal->je_num;
				$journal->update($data_journal);
				JournalAccountDetail::where('journal_id','=',$journal->id)->delete();
			}
			
			if($journal){
				$data['closing_nn_chips']=isset($data['closing_nn_chips'])?$data['closing_nn_chips']:0;
				$data['closing_cash_chips']=isset($data['closing_cash_chips'])?$data['closing_cash_chips']:0;
				$data['closing_cash']=isset($data['closing_cash'])?$data['closing_cash']:0;
				$data['check_in_commision']=isset($data['check_in_commision'])?$data['check_in_commision']:0;
				JournalAccountDetail::create([
					'journal_id'=>$journal->id,
					'account_id'=>isset($uha['account_id'])?$uha['account_id']:$account_config['receivable'],//ar
					'debit'=>0,
					'credit'=>($data['closing_nn_chips']+$data['closing_cash_chips']+$data['closing_cash']),
					'entity'=>'customer',
					'entity_id'=>$data['customer_id']
				]);
				
				/*JournalAccountDetail::create([
					'journal_id'=>$journal->id,
					'account_id'=>$account_config['expense'],//expense
					'debit'=>$data['check_in_commision'],
					'credit'=>0,
					'entity'=>'customer',
					'entity_id'=>$data['customer_id']
				]);*/
				
				JournalAccountDetail::create([
					'journal_id'=>$journal->id,
					'account_id'=>$account_config['nn_chips'],//nn chip
					'debit'=>$data['closing_nn_chips'],
					'credit'=>0,
					'entity'=>'user',
					'entity_id'=>$data['user_id']
				]);
				
				JournalAccountDetail::create([
					'journal_id'=>$journal->id,
					'account_id'=>$account_config['cash_chips'],//cash chip
					'debit'=>$data['closing_cash_chips'],
					'credit'=>0,
					'entity'=>'user',
					'entity_id'=>$data['user_id']
				]);
				
				JournalAccountDetail::create([
					'journal_id'=>$journal->id,
					'account_id'=>$account_config['cash_real'],//cash real
					'debit'=>$data['closing_cash'],
					'credit'=>0,
					'entity'=>'user',
					'entity_id'=>$data['user_id']
				]);
				
				/*JournalAccountDetail::create([
					'journal_id'=>$journal->id,
					'account_id'=>$account_config['receivable'],//ar
					'debit'=>0,
					'credit'=>$data['check_in_commision'],
					'entity'=>'customer',
					'entity_id'=>$data['customer_id']
				]);*/
			}
			
			//commission trip
			$commission=DB::table(DB::raw('(select 
				 sum(td.rolling*ifnull(t.commission,0)/100)as customer,
				 sum(td.rolling*(if(1-t.commission>0,1-t.commission,0))/100)as gl
				from transactions t
				join transaction_details td on td.transaction_id=t.id
				join customers c on c.id=t.customer_id
				join users u on u.id=t.user_id
				where t.id='.$id.' and (td.flags="role" or td.flags="open")and td.type=0)as sub'))->first();
				
			$journal=JournalAccount::where('activity','=','commission_trip')->where('activity_id','=',$id)->first();
			$number=getAutoNumber('je_num','journal_accounts','JE-'.date('mY')."-",4);
			$data_journal=[
				'je_num'=>$number,
				'date'=>date('Y-m-d',strtotime($data['closing_date'])),
				'user_id'=>$user['id'],
				'entity'=>'customer',
				'entity_id'=>$data['customer_id'],
				'activity'=>'commission_trip',
				'activity_id'=>$id,
				'remark'=>'Commission Trip ('.$data['customer']['first_name'].' '.$data['customer']['last_name'].')'
			];
			
			if(count($journal)==0){
				$journal=JournalAccount::create($data_journal);
			}else{
				$data_journal['je_num']=$journal->je_num;
				$journal->update($data_journal);
				JournalAccountDetail::where('journal_id','=',$journal->id)->delete();
			}
			
			if($journal){
				$commission->customer=isset($commission->customer)?$commission->customer:0;
				$commission->gl=isset($commission->gl)?$commission->gl:0;
				/*JournalAccountDetail::create([
					'journal_id'=>$journal->id,
					'account_id'=>isset($uha['account_id'])?$uha['account_id']:$account_config['commission'],//ar
					'debit'=>0,
					'credit'=>$commission->gl,
					'entity'=>'user',
					'entity_id'=>$data['user_id']
				]);*/
				
				JournalAccountDetail::create([
					'journal_id'=>$journal->id,
					'account_id'=>isset($uha['account_id'])?$uha['account_id']:$account_config['commission'],//ar
					'debit'=>$commission->customer,
					'credit'=>0,
					'entity'=>'customer',
					'entity_id'=>$data['customer_id']
				]);
				
				/*JournalAccountDetail::create([
					'journal_id'=>$journal->id,
					'account_id'=>$account_config['cash_chips'],//cash chip
					'debit'=>$commission->gl,
					'credit'=>0,
					'entity'=>'user',
					'entity_id'=>$data['user_id']
				]);*/
				
				JournalAccountDetail::create([
					'journal_id'=>$journal->id,
					'account_id'=>$account_config['cash_chips'],//cash real
					'debit'=>0,
					'credit'=>$commission->customer,
					'entity'=>'customer',
					'entity_id'=>$data['customer_id']
				]);
			}
			
			DB::commit();
		}catch(\Exception $exc){
			// echo $exc;
			DB::rollback();
		}
			
		return true;
	}
	
	public static function journalRolling2($id,$request){
		$user = User::where('token',$request->header("X-Auth-Token"))->first();
		$data = TransactionRolling::select(['transaction_rollings.*','transactions.customer_id','customers.first_name','customers.last_name','customers.agent_id'])
			->join('transactions','transactions.id','=','transaction_rollings.transaction_id')
			->join('customers','customers.id','=','transactions.customer_id')
			->where('transaction_rollings.id','=',$id)
			->first();
		
		DB::beginTransaction();
		
		try{
			$uha=UserHasAccount::where('user_id','=',$data['user_id'])->first();
			$journal=JournalAccount::where('activity','=','rolling2')->where('activity_id','=',$id)->first();
			$number=getAutoNumber('je_num','journal_accounts','JE-'.date('mY')."-",4);
			$data_journal=[
				'je_num'=>$number,
				'date'=>date('Y-m-d',strtotime($data['created_at'])),
				'user_id'=>$user['id'],
				'entity'=>'customer',
				'entity_id'=>$data['customer_id'],
				'activity'=>'rolling2',
				'activity_id'=>$id,
				'remark'=>'Rolling 3 Insurance ('.$data['first_name'].' '.$data['last_name'].')',
				'company_id'=>2
			];
			
			if(count($journal)==0){
				$journal=JournalAccount::create($data_journal);
			}else{
				$data_journal['je_num']=$journal->je_num;
				$journal->update($data_journal);
				JournalAccountDetail::where('journal_id','=',$journal->id)->delete();
			}
			
			$percent=0;
			switch($data['room']){
				case 'Room 38':
					$percent=1;
				break;
				case 'Maxims':
					$percent=0.7;
				break;
				case 'Other Room':
					$percent=0.8;
				break;
				
			}
			
			$insurance=0;
			$insurance_agent=0;
			$insurance=$percent*$data['insurance'];
			if($data['agent_id']){
				$percent_agent=$data['agent_shared']?($data['agent_shared']/100):0;
				if($data['status']=='Win Only'){
					$percent_agent=0.1;
					if($data['scenario']==0){
						$percent_agent=0;
					}
				}
				$insurance_agent=$insurance*$percent_agent;
				$insurance-=$insurance_agent;
			}
			
			if($journal){
				$account_config=AccountConfig::first();
				$data['rolling']=isset($data['rolling'])?$data['rolling']:0;
				$data['insurance']=isset($data['insurance'])?$data['insurance']:0;
				JournalAccountDetail::create([
					'journal_id'=>$journal->id,
					'account_id'=>isset($uha['account_id'])?$uha['account_id']:$account_config['insurance'],//insurance
					'debit'=>0,
					'credit'=>$insurance
				]);
				
				JournalAccountDetail::create([
					'journal_id'=>$journal->id,
					'account_id'=>$account_config['cash_chips'],//cash
					'debit'=>$insurance,
					'credit'=>0
				]);
			}
			
			DB::commit();
		}catch(\Exception $exc){
			// echo $exc;
			DB::rollback();
		}
			
		return true;
	}
}
