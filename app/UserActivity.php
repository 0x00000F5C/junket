<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserActivity extends Model
{

    protected $fillable = ['activity','user_id','menu','visitor'];

    protected $hidden = ['updated_at'];

    protected $with = ['user'];

    public function user(){
        return $this->belongsTo(User::class)->withTrashed();
    }

    static function storeActivity($data){
        $activity = UserActivity::create([
            "activity" => $data['activity'],
            "user_id" => $data['user'],
            "menu" => $data['menu'],
            "visitor" => $data['ipaddress']
        ]);

        return $activity;
    }
}
