<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;
use DB;

class AccountType extends Model
{
    use SoftDeletes, Sortable;

    protected $fillable = [
        'name',
        'parent_id',
        'account_group_type'
    ];

    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];

    protected $dates = ['created_at','updated_at','deleted_at'];
	
    protected $sortable = [
        'name',
        'parent_id',
        'account_group_type'
    ];

}
