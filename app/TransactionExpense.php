<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransactionExpense extends Model
{
    protected $fillable = [
        "transaction_id",
        "date",
        "description",
        "amount",
		"user_id"
    ];

    protected $hidden = ["deleted_at"];
	
	protected $with = ['user'];
	
	public function user(){
		return $this->belongsTo(User::class);
	}
	
}
