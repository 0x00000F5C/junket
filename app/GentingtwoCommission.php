<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;
use DB;

class GentingtwoCommission extends Model
{
    use SoftDeletes, Sortable;

    protected $fillable = [
        'date',
        'buy_from',
        'type_chip',
        'amount',
        'commission',
        'commission_payout',
        'user_id',
        'entity',
        'entity_id'
    ];

    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];

    protected $dates = ['created_at','updated_at','deleted_at'];
	
    protected $sortable = [
        'date',
        'buy_from',
        'type_chip',
        'amount',
        'commission',
        'commission_payout',
        'user_id',
        'entity',
        'entity_id'
    ];

}
