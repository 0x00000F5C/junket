<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LockedVisitor extends Model
{
    protected $fillable = ["ip_address"];

    protected $hidden = ['updated_at'];
}
