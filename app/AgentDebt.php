<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AgentDebt extends Model {
    use SoftDeletes;

    protected $fillable = [
        "agent_id", "transaction_id", "date", "amount", "is_paid", "interest_type", "interest_rate", "interest_amount", "staff_id", "due_date","amount_due"
    ];

    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];

    public function customer(){
        return $this->belongsTo(Agent::class)->withTrashed();
    }

    public function transaction(){
        return $this->belongsTo(Transaction::class)->withTrashed();
    }

    public function users(){
        return $this->belongsTo(User::class,'staff_id');
    }
}
