<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransactionDetail extends Model
{
    protected $fillable = [
        "transaction_id",
        "rolling",
        "flags",
        "time_transaction",
		"type",
		"user_id"
    ];

    protected $hidden = ["created_at","updated_at"];

    public function transaction(){
        return $this->belongsTo(TransactionDetail::class);
    }
	
    public function user(){
        return $this->belongsTo(User::class);
    }
}
