<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;
use DB;

class UserHasAccount extends Model
{

    protected $fillable = [
        'user_id',
		'account_id'
    ];

    protected $hidden = ['created_at', 'updated_at'];

    protected $dates = ['created_at','updated_at'];
	
	protected $with = ['user','account'];
	
	public function user(){
		return $this->belongsTo(User::class)->withTrashed();
	}
	
	public function account(){
		return $this->belongsTo(Account::class)->withTrashed();
	}

}
