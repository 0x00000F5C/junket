<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;
use DB;

class AccountConfig extends Model
{
    use SoftDeletes, Sortable;

    protected $fillable = [
        'nn_chips',
        'junket_chips',
        'cash_chips',
        'cash_real',
        'bank',
        'open_balance_asset',
        'agent_share',
        'boss_share',
        'capital',
        'loan',
        'receivable',
        'commission',
        'insurance',
        'income',
        'expense',
        'expense_morning',
        'expense_night',
        'expense_bank',
        'king_win',
		'runner',
		'tips'
    ];

    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];

    protected $dates = ['created_at','updated_at','deleted_at'];
	
    protected $sortable = [
        'nn_chips',
        'junket_chips',
        'cash_chips',
        'cash_real',
        'bank',
        'open_balance_asset',
        'agent_share',
        'boss_share',
        'capital',
        'commission',
        'insurance',
        'income',
        'expense'
    ];

}
