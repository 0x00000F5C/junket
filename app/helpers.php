<?php

function check_auth($user,$permission){
    $result = 0;
    if($user != null){
        if($user->can($permission)){
            if((strtotime($user->expired_at) - time()) >= 0) {
                $result = 1;
            }
        }
    }

    return array("result"=>$result,"expired_at"=>strtotime($user->expired_at));
}

function get_menus($user){
    $menus = config('menu');
    $has_menu = [];

    foreach ($menus as $head => $menu){
        $has_menu[$head] = [
            "icon" => $menu['icon'],
            "menu" => []
        ];

        foreach ($menu['menu'] as $sub_menu => $value){
            if($user->can("read ".$sub_menu)){
                $has_menu[$head]['menu'][$sub_menu] = $value;
                $has_menu[$head]["menu"][$sub_menu]["actions"] = [];
                if($user->can("create ".$sub_menu)){
                    array_push($has_menu[$head]["menu"][$sub_menu]["actions"],"create");
                }
                if($user->can("update ".$sub_menu)){
                    array_push($has_menu[$head]["menu"][$sub_menu]["actions"],"update");
                }
                if($user->can("delete ".$sub_menu)){
                    array_push($has_menu[$head]["menu"][$sub_menu]["actions"],"delete");
                }
                if($user->can("show ".$sub_menu)){
                    array_push($has_menu[$head]["menu"][$sub_menu]["actions"],"show");
                }
            }
        }

        if(count($has_menu[$head]["menu"]) == 0){
            unset($has_menu[$head]);
        }
    }

    return $has_menu;
}

function error_unauthorized(){
    return response()->json([
		"status" => "error",
        "message" => "Unauthorized",
        "data" => null
    ],403);
}

function transformCollection($data){
    $dataArray = $data->toArray();
    return [
        'total' => $dataArray['total'],
        'per_page' => intval($dataArray['per_page']),
        'current_page' => $dataArray['current_page'],
        'last_page' => $dataArray['last_page'],
        'next_page_url' => $dataArray['next_page_url'],
        'prev_page_url' => $dataArray['prev_page_url'],
        'from' => $dataArray['from'],
        'to' =>$dataArray['to'],
        'data' => $dataArray['data']
    ];
}

function getAutoNumber($field, $table, $Parse, $Digit_Count){
    $NOL = "0";
    $sql = DB::table($table)->select($field)->where($field,'LIKE',$Parse.'%')->orderBy($field,'desc')->skip(0)->take(1)->get();
    $counter = 2;
    if (sizeof($sql) == 0) {
        while ($counter < $Digit_Count):
            $NOL = "0" . $NOL;
            $counter++;
        endwhile;
        return $Parse . $NOL . "1";
    }
    else {
        $R = $sql[0]->$field;
        $K = sprintf("%d", substr($R, -$Digit_Count));
        $K = $K + 1;
        $L = $K;
        while (strlen($L) != $Digit_Count) {
            $L = $NOL . $L;
        }
        return $Parse . $L;
    }
}