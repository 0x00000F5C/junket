<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;

class ShiftHistory extends Model
{
    use SoftDeletes, Sortable;

    protected $fillable = [
        'date',
        'from_user_id',
        'shift_from',
        'to_user_id',
        'shift_to',
        'balance',
        'nn_chips_out',
        'nn_chips',
        'junket_chips',
        'cash_chips',
        'cash_real',
        'remark',
        "status",
        "menu_name"
    ];

    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];

    protected $dates = ['deleted_at'];

    protected $with = ['from_user','to_user'];

    public function from_user(){
        return $this->belongsTo(User::class)->withTrashed();
    }

    public function to_user(){
        return $this->belongsTo(User::class)->withTrashed();
    }

    protected $sortable = [
        'id',
        'date',
        'shift_from',
        'shift_to',
        'nn_chips',
        'cash_hips',
        'cash_real'
    ];

    public static function collectShiftHistories($user, $date, $sort_by, $sort_type){
        if($user->roles[0]->id == 1){
            if($date == null){
                if($sort_by != null && $sort_by != 'from_user' && $sort_by != 'to_user'){
                    $histories = ShiftHistory::whereIn('menu_name', ['shift_histories', 'shift_transfer'])
                        ->sortable([$sort_by => $sort_type]);
                }elseif ($sort_by == 'from_user') {
                    $histories = ShiftHistory::select('shift_histories.*')
                        ->join('users', 'users.id', '=', 'shift_histories.from_user_id')
                        ->whereIn('shift_histories.menu_name', ['shift_histories', 'shift_transfer'])
                        ->orderBy('users.name', $sort_type);
                }elseif ($sort_by == 'to_user'){
                    $histories = ShiftHistory::select('shift_histories.*')
                        ->join('users', 'users.id', '=', 'shift_histories.to_user_id')
                        ->whereIn('shift_histories.menu_name', ['shift_histories', 'shift_transfer'])
                        ->orderBy('users.name', $sort_type);
                }else{
                    $histories = ShiftHistory::whereIn('menu_name', ['shift_histories', 'shift_transfer'])
                        ->orderBy('created_at','desc');
                }
            }else{
                if($sort_by != null && $sort_by != 'from_user' && $sort_by != 'to_user'){
                    $histories = ShiftHistory::whereIn('menu_name', ['shift_histories', 'shift_transfer'])
                        ->where('date', $date)
                        ->sortable([$sort_by => $sort_type]);
                }elseif ($sort_by == 'from_user') {
                    $histories = ShiftHistory::select('shift_histories.*')
                        ->join('users', 'users.id', '=', 'shift_histories.from_user_id')
                        ->where('shift_histories.date', $date)
                        ->whereIn('shift_histories.menu_name', ['shift_histories', 'shift_transfer'])
                        ->orderBy('users.name', $sort_type);
                }elseif ($sort_by == 'to_user'){
                    $histories = ShiftHistory::select('shift_histories.*')
                        ->join('users', 'users.id', '=', 'shift_histories.to_user_id')
                        ->whereIn('shift_histories.menu_name', ['shift_histories', 'shift_transfer'])
                        ->where('shift_histories.date', $date)
                        ->orderBy('users.name', $sort_type);
                }else{
                    $histories = ShiftHistory::whereIn('menu_name', ['shift_histories', 'shift_transfer'])
                        ->where('date', $date)
                        ->orderBy('created_at','desc');
                }
            }
        }else{
            if($date == null){
                if($sort_by != null && $sort_by != 'from_user' && $sort_by != 'to_user'){
                    $histories = ShiftHistory::where('from_user_id', $user->id)
                        ->orWhere('to_user_id', $user->id)
                        ->whereIn('menu_name', ['shift_histories', 'shift_transfer'])
                        ->sortable([$sort_by => $sort_type]);
                }elseif ($sort_by == 'from_user') {
                    $histories = ShiftHistory::select('shift_histories.*')
                        ->join('users', 'users.id', '=', 'shift_histories.from_user_id')
                        ->where('shift_histories.from_user_id', $user->id)
                        ->orWhere('shift_histories.to_user_id', $user->id)
                        ->whereIn('shift_histories.menu_name', ['shift_histories', 'shift_transfer'])
                        ->orderBy('users.name', $sort_type);
                }elseif ($sort_by == 'to_user'){
                    $histories = ShiftHistory::select('shift_histories.*')
                        ->join('users', 'users.id', '=', 'shift_histories.to_user_id')
                        ->where('shift_histories.from_user_id', $user->id)
                        ->orWhere('shift_histories.to_user_id', $user->id)
                        ->whereIn('shift_histories.menu_name', ['shift_histories', 'shift_transfer'])
                        ->orderBy('users.name', $sort_type);
                }else{
                    $histories = ShiftHistory::where('from_user_id', $user->id)
                        ->orWhere('to_user_id', $user->id)
                        ->whereIn('menu_name', ['shift_histories', 'shift_transfer'])
                        ->orderBy('created_at','desc');
                }
            }else{
                if($sort_by != null && $sort_by != 'from_user' && $sort_by != 'to_user'){
                    $histories = ShiftHistory::where('date', $date)
                        ->where('from_user_id', $user->id)
                        ->orWhere('to_user_id', $user->id)
                        ->whereIn('menu_name', ['shift_histories', 'shift_transfer'])
                        ->sortable([$sort_by => $sort_type]);
                }elseif ($sort_by == 'from_user') {
                    $histories = ShiftHistory::select('shift_histories.*')
                        ->join('users', 'users.id', '=', 'shift_histories.from_user_id')
                        ->where('shift_histories.date', $date)
                        ->where('shift_histories.from_user_id', $user->id)
                        ->orWhere('shift_histories.to_user_id', $user->id)
                        ->whereIn('shift_histories.menu_name', ['shift_histories', 'shift_transfer'])
                        ->orderBy('users.name', $sort_type);
                }elseif ($sort_by == 'to_user'){
                    $histories = ShiftHistory::select('shift_histories.*')
                        ->join('users', 'users.id', '=', 'shift_histories.to_user_id')
                        ->where('shift_histories.date', $date)
                        ->where('shift_histories.from_user_id', $user->id)
                        ->orWhere('shift_histories.to_user_id', $user->id)
                        ->whereIn('shift_histories.menu_name', ['shift_histories', 'shift_transfer'])
                        ->orderBy('users.name', $sort_type);
                }else{
                    $histories = ShiftHistory::where('date', $date)
                        ->where('from_user_id', $user->id)
                        ->orWhere('to_user_id', $user->id)
                        ->whereIn('menu_name', ['shift_histories', 'shift_transfer'])
                        ->orderBy('created_at','desc');
                }
            }
        }

        return $histories;
    }

    public static function collectByMenu($date, $sort_by, $sort_type, $menu){
        if($date == null){
            if($sort_by != null && $sort_by != 'from_user' && $sort_by != 'to_user'){
                $histories = ShiftHistory::where('menu_name', $menu)
                    ->sortable([$sort_by => $sort_type]);
            }elseif ($sort_by == 'from_user') {
                $histories = ShiftHistory::select('shift_histories.*')
                    ->join('users', 'users.id', '=', 'shift_histories.from_user_id')
                    ->where('shift_histories.menu_name', $menu)
                    ->orderBy('users.name', $sort_type);
            }elseif ($sort_by == 'to_user'){
                $histories = ShiftHistory::select('shift_histories.*')
                    ->join('users', 'users.id', '=', 'shift_histories.to_user_id')
                    ->where('shift_histories.menu_name', $menu)
                    ->orderBy('users.name', $sort_type);
            }else{
                $histories = ShiftHistory::where('menu_name', $menu)
                    ->orderBy('created_at','desc');
            }
        }else{
            if($sort_by != null && $sort_by != 'from_user' && $sort_by != 'to_user'){
                $histories = ShiftHistory::where('menu_name', $menu)
                    ->where('date', $date)
                    ->sortable([$sort_by => $sort_type]);
            }elseif ($sort_by == 'from_user') {
                $histories = ShiftHistory::select('shift_histories.*')
                    ->join('users', 'users.id', '=', 'shift_histories.from_user_id')
                    ->where('shift_histories.date', $date)
                    ->where('shift_histories.menu_name', $menu)
                    ->orderBy('users.name', $sort_type);
            }elseif ($sort_by == 'to_user'){
                $histories = ShiftHistory::select('shift_histories.*')
                    ->join('users', 'users.id', '=', 'shift_histories.to_user_id')
                    ->whereIn('menu_name', ['shift_histories', 'shift_transfer'])
                    ->where('shift_histories.date', $date)
                    ->where('shift_histories.menu_name', $menu)
                    ->orderBy('users.name', $sort_type);
            }else{
                $histories = ShiftHistory::where('menu_name', $menu)
                    ->where('date', $date)
                    ->orderBy('created_at','desc');
            }
        }

        return $histories;
    }

    public static function collectFromRunnerOrAssist($user, $date, $sort_by, $sort_type){
        if($date == null){
            if($user->roles[0]->id == 4){  //if roles as manager
                if($sort_by != null && $sort_by != 'from_user' && $sort_by != 'to_user'){
                    $data = ShiftHistory::where('to_user_id', $user->id)
                        ->where('menu_name', 'chip_from_runner_or_assist')
                        ->sortable([$sort_by => $sort_type]);
                }elseif ($sort_by == 'from_user') {
                    $data = ShiftHistory::select('shift_histories.*')
                        ->join('users', 'users.id', '=', 'shift_histories.from_user_id')
                        ->where('shift_histories.menu_name', 'chip_from_runner_or_assist')
                        ->where('shift_histories.to_user_id', $user->id)
                        ->orderBy('users.name', $sort_type);
                }elseif ($sort_by == 'to_user'){
                    $data = ShiftHistory::select('shift_histories.*')
                        ->join('users', 'users.id', '=', 'shift_histories.to_user_id')
                        ->where('shift_histories.menu_name', 'chip_from_runner_or_assist')
                        ->where('shift_histories.to_user_id', $user->id)
                        ->orderBy('users.name', $sort_type);
                }else{
                    $data = ShiftHistory::where('to_user_id', $user->id)
                        ->where('menu_name', 'chip_from_runner_or_assist')
                        ->orderBy('created_at','desc');
                }
            }else{
                if($sort_by != null && $sort_by != 'from_user' && $sort_by != 'to_user'){
                    $data = ShiftHistory::where('from_user_id', $user->id)
                        ->where('menu_name', 'chip_from_runner_or_assist')
                        ->sortable([$sort_by => $sort_type]);
                }elseif ($sort_by == 'from_user') {
                    $data = ShiftHistory::select('shift_histories.*')
                        ->join('users', 'users.id', '=', 'shift_histories.from_user_id')
                        ->where('shift_histories.menu_name', 'chip_from_runner_or_assist')
                        ->where('shift_histories.from_user_id', $user->id)
                        ->orderBy('users.name', $sort_type);
                }elseif ($sort_by == 'to_user'){
                    $data = ShiftHistory::select('shift_histories.*')
                        ->join('users', 'users.id', '=', 'shift_histories.to_user_id')
                        ->where('shift_histories.menu_name', 'chip_from_runner_or_assist')
                        ->where('shift_histories.from_user_id', $user->id)
                        ->orderBy('users.name', $sort_type);
                }else{
                    $data = ShiftHistory::where('from_user_id', $user->id)
                        ->where('menu_name', 'chip_from_runner_or_assist')
                        ->orderBy('created_at','desc');
                }
            }
        }else{
            if($user->roles[0]->id == 4){  //if roles as manager
                if($sort_by != null && $sort_by != 'from_user' && $sort_by != 'to_user'){
                    $data = ShiftHistory::where('to_user_id', $user->id)
                        ->where('menu_name', 'chip_from_runner_or_assist')
                        ->where('date', $date)
                        ->sortable([$sort_by => $sort_type]);
                }elseif ($sort_by == 'from_user') {
                    $data = ShiftHistory::select('shift_histories.*')
                        ->join('users', 'users.id', '=', 'shift_histories.from_user_id')
                        ->where('shift_histories.date', $date)
                        ->where('shift_histories.menu_name', 'chip_from_runner_or_assist')
                        ->where('shift_histories.to_user_id', $user->id)
                        ->orderBy('users.name', $sort_type);
                }elseif ($sort_by == 'to_user'){
                    $data = ShiftHistory::select('shift_histories.*')
                        ->join('users', 'users.id', '=', 'shift_histories.to_user_id')
                        ->where('shift_histories.date', $date)
                        ->where('shift_histories.menu_name', 'chip_from_runner_or_assist')
                        ->where('shift_histories.to_user_id', $user->id)
                        ->orderBy('users.name', $sort_type);
                }else{
                    $data = ShiftHistory::where('to_user_id', $user->id)
                        ->where('date', $date)
                        ->where('menu_name', 'chip_from_runner_or_assist')
                        ->orderBy('created_at','desc');
                }
            }else{
                if($sort_by != null && $sort_by != 'from_user' && $sort_by != 'to_user'){
                    $data = ShiftHistory::where('from_user_id', $user->id)
                        ->where('date', $date)
                        ->where('menu_name', 'chip_from_runner_or_assist')
                        ->sortable([$sort_by => $sort_type]);
                }elseif ($sort_by == 'from_user') {
                    $data = ShiftHistory::select('shift_histories.*')
                        ->join('users', 'users.id', '=', 'shift_histories.from_user_id')
                        ->where('shift_histories.date', $date)
                        ->where('shift_histories.menu_name', 'chip_from_runner_or_assist')
                        ->where('shift_histories.from_user_id', $user->id)
                        ->orderBy('users.name', $sort_type);
                }elseif ($sort_by == 'to_user'){
                    $data = ShiftHistory::select('shift_histories.*')
                        ->join('users', 'users.id', '=', 'shift_histories.to_user_id')
                        ->where('shift_histories.date', $date)
                        ->where('shift_histories.menu_name', 'chip_from_runner_or_assist')
                        ->where('shift_histories.from_user_id', $user->id)
                        ->orderBy('users.name', $sort_type);
                }else{
                    $data = ShiftHistory::where('from_user_id', $user->id)
                        ->where('date', $date)
                        ->where('menu_name', 'chip_from_runner_or_assist')
                        ->orderBy('created_at','desc');
                }
            }
        }

        return $data;
    }

}
