<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnToJournalAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('journal_accounts',function(Blueprint $table){
			$table->string('je_num',50)->default('');
			$table->string('ref_no',50)->default('');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('journal_accounts',function(Blueprint $table){
			$table->dropColumn(['je_num','ref_no']);
		});
    }
}
