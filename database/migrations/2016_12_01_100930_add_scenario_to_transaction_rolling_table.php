<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddScenarioToTransactionRollingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transaction_rollings', function (Blueprint $table) {
            $table->enum('scenario', ['0', '1'])->comment('0 = lose, 1 = win')->default(0);
            $table->double("agent_shared")->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transaction_rollings', function (Blueprint $table) {
            $table->dropColumn(['scenario','agent_shared']);
        });
    }
}
