<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('accounts',function(Blueprint $table){
			$table->integer('nn_chips_id')->nullable()->unsigned();
			$table->double('nn_chips')->default(0);
			$table->integer('junket_chips_id')->nullable()->unsigned();
			$table->double('junket_chips')->default(0);
			$table->integer('cash_chips_id')->nullable()->unsigned();
			$table->double('cash_chips')->default(0);
			$table->integer('cash_real_id')->nullable()->unsigned();
			$table->double('cash_real')->default(0);
			$table->integer('balance_id')->nullable()->unsigned();
			$table->double('balance')->default(0);
			$table->date('balance_date')->default('0000-00-00');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('accounts',function(Blueprint $table){
			$table->dropColumn(['nn_chips_id','nn_chips','junket_chips_id',
				'junket_chips','cash_chips_id','cash_chips','cash_real_id',
				'cash_real','balance_id','balance','balance_date']);
		});
    }
}
