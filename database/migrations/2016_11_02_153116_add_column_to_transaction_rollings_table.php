<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnToTransactionRollingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transaction_rollings',function(Blueprint $table){
			$table->double('rolling')->default(0);
			$table->double('insurance')->default(0);
			$table->string('status')->default('0');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transaction_rollings',function(Blueprint $table){
			$table->dropColumn(['rolling','insurance','status']);
		});
    }
}
