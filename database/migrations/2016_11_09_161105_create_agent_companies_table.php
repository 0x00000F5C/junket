<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgentCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agent_companies',function(Blueprint $table){
			$table->increments('id');
			$table->string('company_name')->nullable();
			$table->string('pic')->nullable();
			$table->text('address')->nullable();
			$table->string('postal_code')->nullable();
			$table->string('company_contact')->nullable();
			$table->string('company_email')->nullable();
            $table->timestamps();
			
			$table->softDeletes();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('agent_companies');
    }
}
