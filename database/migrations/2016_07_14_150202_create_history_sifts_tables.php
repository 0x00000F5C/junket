<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistorySiftsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shift_histories', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date');
            $table->integer('from_user_id')->unsigned();
            $table->string('shift_from');
            $table->integer('to_user_id')->unsigned();
            $table->string('shift_to');
            $table->double('nn_chips')->default(0);
            $table->double('cash_chips')->default(0);
            $table->double('cash_real')->default(0);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('from_user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->foreign('to_user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('shift_histories');
    }
}
