<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountConfigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('account_configs',function(Blueprint $table){
			$table->increments('id');
			$table->integer('nn_chips')->nullable()->unsigned();
			$table->integer('junket_chips')->nullable()->unsigned();
			$table->integer('cash_chips')->nullable()->unsigned();
			$table->integer('cash_real')->nullable()->unsigned();
			$table->integer('bank')->nullable()->unsigned();
			$table->integer('open_balance_asset')->nullable()->unsigned();
			$table->integer('commission')->nullable()->unsigned();
			$table->integer('insurance')->nullable()->unsigned();
			$table->integer('receivable')->nullable()->unsigned();
			$table->integer('income')->nullable()->unsigned();
			$table->integer('expense')->nullable()->unsigned();
			$table->timestamps();
			$table->softDeletes();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('account_configs');
    }
}
