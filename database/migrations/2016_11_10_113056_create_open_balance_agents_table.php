<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOpenBalanceAgentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('open_balance_agents', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('agent_id')->unsigned();
            $table->double('junket_chips')->default(0);
            $table->double('nn_chips')->default(0);
            $table->double('cash_chips')->default(0);
            $table->double('cash_real')->default(0);
            $table->timestamps();

            $table->foreign('agent_id')
                ->references('id')->on('agents')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('open_balance_agents');
    }
}
