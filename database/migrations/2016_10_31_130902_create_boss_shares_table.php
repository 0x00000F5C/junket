<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBossSharesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('boss_shares', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('boss_id')->unsigned();
            $table->double('amount')->default(0);
            $table->timestamps();

            $table->foreign('boss_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('boss_shares');
    }
}
