<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddManagerToExpensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('expenses',function(Blueprint $table){
			$table->integer('manager_id')->unsigned()->nullable();
			$table->foreign('manager_id')->on('users')->references('id')->onDelete('cascade');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('expenses',function(Blueprint $table){
			$table->dropForeign(['manager_id']);
			$table->dropColumn('manager_id');
		});
    }
}
