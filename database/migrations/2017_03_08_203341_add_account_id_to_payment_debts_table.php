<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAccountIdToPaymentDebtsTable extends Migration {
    public function up() {
		Schema::table("payment_debts", function(Blueprint $table) {
			$table->integer("customer_id")->nullable()->change();
			$table->integer("agent_id")->nullable()->change();
			$table->integer("vendor_id")->nullable()->change();
			$table->integer("company_id")->nullable()->change();
		});
    }

    public function down() {
		//
    }
}
