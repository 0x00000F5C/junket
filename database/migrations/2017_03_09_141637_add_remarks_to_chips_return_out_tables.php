<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRemarksToChipsReturnOutTables extends Migration {
    public function up() {
		Schema::table("shift_histories", function(Blueprint $table) {
			$table->text("remark")->nullable()->after("cash_real");
		});
    }

    public function down() {
		Schema::table("shift_histories", function(Blueprint $table) {
			$table->dropColumn("remark");
		});
    }
}
