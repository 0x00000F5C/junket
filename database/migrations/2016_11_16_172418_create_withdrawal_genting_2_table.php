<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWithdrawalGenting2Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('withdrawal_genting_2', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date');
            $table->string('agent_boss');
            $table->integer('agent_id')->nullable()->references('id')->on('agents');
            $table->integer('boss_id')->nullable()->references('id')->on('users');
            $table->double('amount')->default(0);
            $table->integer('user_id')->references('id')->on('users');
            $table->integer('type');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('withdrawal_genting_2');
    }
}
