<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserToTransactionDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transaction_details',function(Blueprint $table){
			$table->integer('user_id')->unsigned()->nullable();
			
			$table->foreign('user_id')->on('users')->references('id')->onDelete('cascade');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transaction_details',function(Blueprint $table){
			$table->dropForeign(['user_id']);
			$table->dropColumn('user_id');
		});
    }
}
