<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBankAccountToPaymentDebtsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('payment_debts',function(Blueprint $table){
			$table->integer('bank_account_id')->unsigned()->nullable();
			$table->foreign('bank_account_id')->on('accounts')->references('id')->onDelete('cascade');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payment_debts',function(Blueprint $table){
			$table->dropForeign(['bank_account_id']);
			$table->dropColumn('bank_account_id');
		});
    }
}
