<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customer_type_id')->unsigned();
            $table->string('title',10);
            $table->string('first_name',50);
            $table->string('last_name',50)->nullable();
            $table->date('dob')->nullable();
            $table->string('gender',1)->nullable();
            $table->integer('age')->nullable();
            $table->text('address')->nullable();
            $table->string('contact')->nullable();
            $table->string('company_name')->nullable();
            $table->text('background')->nullable();
            $table->text('runner_remarks')->nullable();
            $table->double('commission')->default(0);
            $table->string('debt')->nullable();
            $table->string('credit_term')->nullable();
            $table->timestamps();

            $table->foreign('customer_type_id')
                ->references('id')
                ->on('customer_types')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('customers');
    }
}
