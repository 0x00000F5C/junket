<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableRollingTables extends Migration
{
    /**
     * Run the migrations.
     *Table rolling
    Date, Trip No, Trading Date, Trip Date, Terminal, GRC No, Name,
     *Rolling Chip Turn over
    Cash Chip Rolling : C, NN Chip Rolling: D, Rolling Chip Turnover: C+D

    Cash Chip Rolling Rebate: A (1.2%*C)
    NN Chip Rolling Rebate: B (1.2%*C)
    Rolling Chip Runt over Rebate : A+B

     * @return void
     */
    public function up()
    {
        Schema::create('rolling_tables', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('transaction_id')->unsigned();
            $table->date('print_date');
            $table->date('trade_date')->nullable();
            $table->string('terminal')->nullable();
            $table->double('cash_chip')->default(0);
            $table->double('nn_chip')->default(0);
            $table->timestamps();

            $table->foreign('transaction_id')
                ->references('id')
                ->on('transactions')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('rolling_tables');
    }
}
