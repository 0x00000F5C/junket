<?php

use Illuminate\Database\Seeder;
use App\PaymentMethod;

class PaymentMethodSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PaymentMethod::create(['name' => 'NN Chips']);
        PaymentMethod::create(['name' => 'Cash Chips']);
        PaymentMethod::create(['name' => 'Cash Real']);
        PaymentMethod::create(['name' => 'Bank']);
        PaymentMethod::create(['name' => 'Bank Cash']);
        PaymentMethod::create(['name' => 'Cash at Hand Manager']);
        PaymentMethod::create(['name' => 'Credit (CCF) from Genting']);
    }
}
