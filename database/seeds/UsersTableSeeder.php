<?php

use Illuminate\Database\Seeder;
use App\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = Role::create(["name" => "Administrator"]);
        $management = Role::create(["name" => "Management"]);
        $finance = Role::create(["name" => "Finance Personnel"]);
        $manager = Role::create(["name" => "Manager"]);
        $assistant = Role::create(["name" => "Assistant Manager"]);
        $runner =  Role::create(["name" => "Runner"]);

        $menus = config('menu');

        foreach($menus as $head => $menu){
            foreach($menu["menu"] as $title => $value){

                Permission::create([
                    "name" => "read ".$title
                ]);
                $admin->givePermissionTo("read ".$title);
//                if($title == "customers" || $title == "purchases" || $title == "shift_histories" || $title == "transactions"){
//                    $manager->givePermissionTo("read ".$title);
//                }
//                if($title == "customers" || $title == "transactions"){
//                    $runner->givePermissionTo("read ".$title);
//                }

                foreach($value["actions"] as $action){
                    Permission::create([
                        "name" => $action." ".$title
                    ]);
                    $admin->givePermissionTo($action." ".$title);
//                    if($title == "customers" || $title == "purchases" || $title == "shift_histories" || $title == "transactions"){
//                        $manager->givePermissionTo($action." ".$title);
//                    }
//                    if($title == "customers" || $title == "transactions"){
//                        $runner->givePermissionTo($title." ".$title);
//                    }
                }
            }
        }

        $user = User::create([
            "username" => "admin",
            "email" => "admin@admin.com",
            "name" => "Administrator",
            "password" => bcrypt("admin1234")
        ]);

        $user->assignRole("Administrator");
    }
}
